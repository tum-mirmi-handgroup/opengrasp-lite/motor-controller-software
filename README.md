# OPENGRASP-LITE Motordriver Firmware

This project contains the firmware for the motor controller of the OPENGRASP-LITE hand. 
It handles the low level control of the motors with real time requirements, e.g. the position controller of each fingers, and the contact detection allowing the [main controller](https://gitlab.com/tum-mirmi-handgroup/opengrasp-lite/main-controller-software) to handle more high-level tasks like the coordination of different fingers into a grasp, or communication with external devices. 

[[_TOC_]]

### Available Control Schemes

The motor provides two main control schemes, an open loop voltage control and a closed loop cascaded position control, with a PI current controller at 10 kHz, a PID velocity controller at 1 kHz and a P position controller.

### Startup Behavior

At startup the motor controller awaits a command by the main controller to start the calibration procedure. As soon as this command is issued, it will first search for the absolute movement range and then make to full open-close passes at different voltages to calibrate the detection model. 

# Build System

The Software is structured as a Microchip Studio Solution, which can be downloaded [here](https://www.microchip.com/en-us/tools-resources/develop/microchip-studio#Downloads).
The Microchip Studio Solution contains multiple projects to build the software for different hardware paltforms, most notably are the following two platforms.
 - `Platform_SAMD51J19A` works on the Motorboards v2.0.0 which is used within the OPENGRASP-LITE Hand. To flash the board an external Arm Serial Wire Debugger (SWD), that is compatible with the Microchip Studio, like the Atmel ICE is necessary.
 - `Platform_SAME54XPro` works on a SAME54 Xplained PRO Evaluation Kit to allow for an easy 

# Repository structure
 - `firmware/` contains the source code and the Microchip Studio Solution file
 - `firmware/Platform_<NAME>` contains the source code only related to the specific platform, with the `hardware_defines.h` file being the most notably file. As this file contains most of the macros that are used to configure the common source code for a specific platform.
 - `firmware/common` contains source code that is shared between different hardware platforms
 - `firmware/libs` different library style modules that for a specific use case:
    - `DSP` an external library used for math functions.
    - `motor_test` Specialized code to tune PID controller, it can be activated with the `DO_MOTOR_TEST` define.
    - `sam5_control` Contains code for a bilinear transformed PID controller with back-calculation and a D-Filter.
    - `sam5_dc_motor_control` This contains the main logic of the software, handling the calibration and running state machines.
    - `sam5_i2c_slave` A software module to communicate with the I2C Master via a custom protocol which runs on the main controller.
    - `sam5_sw_crc8` Software CRC8 used in I2C communication.
    - `state_machine_pattern_c` A simple state machine pattern.

