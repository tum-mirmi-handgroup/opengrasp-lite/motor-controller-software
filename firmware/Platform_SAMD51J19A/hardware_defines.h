#ifndef HARDWARE_DEFINES_H_
#define HARDWARE_DEFINES_H_

#include "interrupt_levels.h"


#define DO_MOTOR_TEST	0

//*******************************************************************************
// Status LED setup PIN_PA12 is on the side of the big capacitor and the connectors, PIN_PA13 is on the side with the micro controllers
#define STATUS_LED_PIN_0						PIN_PA12				
#define STATUS_LED_PIN_1						PIN_PA13

/*******************************************************************************/
#define I2C_SLAVE_SERCOM_UNIT			0								///< Selected SERCOM unit

#define I2C_SLAVE_SCL_PIN               GPIO(GPIO_PORTA, 9)				///< GPIO pin to which the SCL signal is connected
#define I2C_SLAVE_SCL_PIN_FUNCTION      GPIO_PIN_FUNCTION_C				///< Function value that connects the I2C_SLAVE_SCL_PIN to PAD1 of the selected SERCOM unit
#define I2C_SLAVE_SDA_PIN               GPIO(GPIO_PORTA, 8)				///< GPIO pin to which the SDA signal is connected
#define I2C_SLAVE_SDA_PIN_FUNCTION      GPIO_PIN_FUNCTION_C				///< Function value that connects the I2C_SLAVE_SDA_PIN to PAD0 of the selected SERCOM unit

#define I2C_SLAVE_GLCK_CORE_GEN			GCLK_PCHCTRL_GEN_GCLK0_Val
#define I2C_SLAVE_GLCK_SLOW_GEN			GCLK_PCHCTRL_GEN_GCLK2_Val


/*******************************************************************************/
#define M1_PRESENT
#define M2_PRESENT
#define M3_PRESENT


/*******************************************************************************/
#ifdef M1_PRESENT
// Output
#define M1_EN_PIN					GPIO(GPIO_PORTB, 3)				///< Pin to send ENABLE signal to Motor 1 (M1)

#define M1_PWM_TCCn					4								///< TCC instance used for M1 PWMs (Forward and reverse)
#define M1_PWM_OTMX					0x0								///< TCC Output Matrix Value used

#define M1_PWM_FORWARD_PIN			GPIO(GPIO_PORTB, 31)			///< Pin where M1 FORWARD PWM signal is connected to
#define M1_PWM_FORWARD_CCn			1								///< CC channel used for M1 PWM FORWARD
#define M1_PWM_FORWARD_FUNCTION		GPIO_PIN_FUNCTION_F

#define M1_PWM_REVERSE_PIN			GPIO(GPIO_PORTB, 30)			///< Pin where M1 REVERSE PWM signal is connected to
#define M1_PWM_REVERSE_CCn			0								///< CC channel used for M1 PWM REVERSE
#define M1_PWM_REVERSE_FUNCTION		GPIO_PIN_FUNCTION_F

// Input
#define M1_ENDSTOP_PIN				GPIO(GPIO_PORTB, 22)				///< Pin where ENDSTOP hall sensor of M1 is connected to
#define M1_ENDSTOP_EICn				6								///< Interrupt number of M1 ENDSTOP input

#define M1_OCM_PIN					GPIO(GPIO_PORTA, 7)				///< Pin of the ADC input channel for M1 CURRENT MEASURING
#define M1_OCM_ADCn					0								///< Analog to digital conversion (ADC) unit used for M1 CURRENT MEASURING
#define M1_OCM_ADC_AINn				7								///< ADC channel used for M1 CURRENT MEASURING
#define M1_OCM_R_SENSE				510.f							///< [Ohm] Resistor value with that the open drain Motor driver is connected to ground

#define M1_DIAG_PIN					GPIO(GPIO_PORTA, 17)			///< Pin where M1 DIAGNOSTIC output is connected to
#define M1_DIAG_EICn				1								///< External interrupt number of M1 DIAGNOSTIC

#define M1_ENCODER_A_PIN			GPIO(GPIO_PORTA, 27)
#define M1_ENCODER_A_EICn			11

#define M1_ENCODER_B_PIN			GPIO(GPIO_PORTB, 23)
#define M1_ENCODER_B_EICn			7

#define M1_ENCODER_EVSYS_CHANNEL	1
#endif // M1_PRESENT


/*******************************************************************************/
#ifdef M2_PRESENT
// Output
#define M2_EN_PIN					GPIO(GPIO_PORTB, 2)				///< Pin to send ENABLE signal to Motor 2 (M2)

#define M2_PWM_TCCn					1								///< TCC instance used for M2 PWMs (Forward and reverse)
#define M2_PWM_OTMX					0x0								///< TCC Output Matrix Value used

#define M2_PWM_FORWARD_PIN			GPIO(GPIO_PORTA, 23)			///< Pin where M2 FORWARD PWM signal is connected to
#define M2_PWM_FORWARD_CCn			3								///< CC channel used for M1 PWM FORWARD
#define M2_PWM_FORWARD_FUNCTION		GPIO_PIN_FUNCTION_F

#define M2_PWM_REVERSE_PIN			GPIO(GPIO_PORTA, 22)			///< Pin where M2 REVERSE PWM signal is connected to
#define M2_PWM_REVERSE_CCn			2								///< CC channel used for M1 PWM REVERSE
#define M2_PWM_REVERSE_FUNCTION		GPIO_PIN_FUNCTION_F

// Input
#define M2_ENDSTOP_PIN				GPIO(GPIO_PORTA, 21)			///< Pin where ENDSTOP hall sensor of M2 is connected to
#define M2_ENDSTOP_EICn				5								///< Interrupt number of M2 ENDSTOP input

#define M2_OCM_PIN					GPIO(GPIO_PORTA, 5)				///< Pin of the ADC input channel for M2 CURRENT MEASURING
#define M2_OCM_ADCn					0								///< Analog to digital conversion (ADC) unit used for M2 CURRENT MEASURING
#define M2_OCM_ADC_AINn				5								///< ADC channel used for M2 CURRENT MEASURING
#define M2_OCM_R_SENSE				510.f							///< Resistor value with that the open drain Motor driver is connected to ground

#define M2_DIAG_PIN					GPIO(GPIO_PORTA, 16)			///< Pin where M2 DIAGNOSTIC output is connected to
#define M2_DIAG_EICn				0								///< External interrupt number of M2 DIAGNOSTIC

#define M2_ENCODER_A_PIN			GPIO(GPIO_PORTA, 25)
#define M2_ENCODER_A_EICn			9

#define M2_ENCODER_B_PIN			GPIO(GPIO_PORTA, 24)
#define M2_ENCODER_B_EICn			8

#define M2_ENCODER_EVSYS_CHANNEL	2
#endif // M2_PRESENT


/*******************************************************************************/
#ifdef M3_PRESENT
// Output
#define M3_EN_PIN					GPIO(GPIO_PORTB, 1)				///< Pin to send ENABLE signal to Motor 3 (M3)

#define M3_PWM_TCCn					3								///< TCC instance used for M3 PWMs (Forward and reverse)
#define M3_PWM_OTMX					0x0								///< TCC Output Matrix Value used

#define M3_PWM_FORWARD_PIN			GPIO(GPIO_PORTB, 17)			///< Pin where M3 FORWARD PWM signal is connected to
#define M3_PWM_FORWARD_CCn			1								///< CC channel used for M3 PWM FORWARD
#define M3_PWM_FORWARD_FUNCTION		GPIO_PIN_FUNCTION_F

#define M3_PWM_REVERSE_PIN			GPIO(GPIO_PORTB, 16)			///< Pin where M3 REVERSE PWM signal is connected to
#define M3_PWM_REVERSE_CCn			0								///< CC channel used for M3 PWM REVERSE
#define M3_PWM_REVERSE_FUNCTION		GPIO_PIN_FUNCTION_F

// Input
#define M3_ENDSTOP_PIN				GPIO(GPIO_PORTA, 18)			///< Pin where ENDSTOP hall sensor of M3 is connected to
#define M3_ENDSTOP_EICn				2								///< Interrupt number of M3 ENDSTOP input

#define M3_OCM_PIN					GPIO(GPIO_PORTB, 9)				///< Pin of the ADC input channel for M3 CURRENT MEASURING
#define M3_OCM_ADCn					0								///< Analog to digital conversion (ADC) unit used for M3 CURRENT MEASURING
#define M3_OCM_ADC_AINn				3								///< ADC channel used for M3 CURRENT MEASURING
#define M3_OCM_R_SENSE				510.f							///< Resistor value with that the open drain Motor driver is connected to ground

#define M3_DIAG_PIN					GPIO(GPIO_PORTA, 15)			///< Pin where M3 DIAGNOSTIC output is connected to
#define M3_DIAG_EICn				15								///< External interrupt number of M3 DIAGNOSTIC

#define M3_ENCODER_A_PIN			GPIO(GPIO_PORTA, 20)
#define M3_ENCODER_A_EICn			4

#define M3_ENCODER_B_PIN			GPIO(GPIO_PORTA, 19)
#define M3_ENCODER_B_EICn			3

#define M3_ENCODER_EVSYS_CHANNEL	3

#endif // M3_PRESENT
#endif //HARDWARE_DEFINES_H_