#ifndef _HARDWARE_DEFINES_H_
#define _HARDWARE_DEFINES_H_

#include "interrupt_levels.h"

//*******************************************************************************
// Status LED setup
#define STATUS_LED_PIN_0						PIN_PC18

/*******************************************************************************/
#define I2C_SLAVE_SERCOM_UNIT			6								///< Selected SERCOM unit

#define I2C_SLAVE_SCL_PIN               GPIO(GPIO_PORTD, 8)				///< GPIO pin to which the SCL signal is connected
#define I2C_SLAVE_SCL_PIN_FUNCTION      PINMUX_PD08D_SERCOM6_PAD1		///< Function value that connects the I2C_SLAVE_SCL_PIN to PAD1 of the selected SERCOM unit
#define I2C_SLAVE_SDA_PIN               GPIO(GPIO_PORTD, 9)				///< GPIO pin to which the SDA signal is connected
#define I2C_SLAVE_SDA_PIN_FUNCTION      PINMUX_PD09D_SERCOM6_PAD0		///< Function value that connects the I2C_SLAVE_SDA_PIN to PAD0 of the selected SERCOM unit

#define I2C_SLAVE_GLCK_CORE_GEN			GCLK_PCHCTRL_GEN_GCLK0_Val
#define I2C_SLAVE_GLCK_SLOW_GEN			GCLK_PCHCTRL_GEN_GCLK3_Val


/*******************************************************************************
EXT 1 Header of XPro Board

     Motor1   Motor2	                      
   -------------------                      -------------------
 1 | -      | -      |  2                 1 | ID     | GND    |  2
   |--------|--------|                      |--------|--------|
 3 | ENC A  | ENC A  |  4                 3 | PB04   | PB05   |  4
   |--------|--------|                      |--------|--------|
 5 | ENC B  | ENC B  |  6                 5 | PA06   | PA07   |  6
   |--------|--------|                      |--------|--------|
 7 | END    | END    |  8                 7 | PB08   | PB09   |  8
   |--------|--------|                      |--------|--------|
 9 | EN     | EN     | 10                 9 | PB07   | PA27   | 10
   |--------|--------|                      |--------|--------|
11 | -      | -      | 12                11 | -      | -      | 12
   |--------|--------|                      |--------|--------|
13 | OCM    | OCM    | 14                13 | PA05   | PA04   | 14
   |--------|--------|                      |--------|--------|
15 | DIAG   | DIAG   | 16                15 | PB28   | PB27   | 16
   |--------|--------|                      |--------|--------|
17 | -      | -      | 18                17 | -      | -      | 18
   |--------|--------|                      |--------|--------|
19 | -      | -      | 20                19 | GND    | VCC    | 20
   -------------------                      -------------------
   
Motor 1 PWM:
	Forward: PB14 (EXT2)
	Reverse: PB15 (EXT2)
     
Motor 2 PWM:
	Forward: PD10 (EXT3)
	Reverse: PD11 (EXT3)
*/

/*******************************************************************************/
#define M1_PRESENT
#define M2_PRESENT


/*******************************************************************************/
#ifdef M1_PRESENT
// Output
#define M1_EN_PIN					GPIO(GPIO_PORTB, 7)				///< Pin to send ENABLE signal to Motor 1 (M1)

#define M1_PWM_TCCn					4								///< TCC instance used for M1 PWMs (Forward and reverse)					
#define M1_PWM_OTMX					0x0								///< TCC Output Matrix Value used

#define M1_PWM_FORWARD_PIN			GPIO(GPIO_PORTB, 14)			///< Pin where M1 FORWARD PWM signal is connected to
#define M1_PWM_FORWARD_CCn			0								///< CC channel used for M1 PWM FORWARD
#define M1_PWM_FORWARD_FUNCTION		GPIO_PIN_FUNCTION_F

#define M1_PWM_REVERSE_PIN			GPIO(GPIO_PORTB, 15)			///< Pin where M1 REVERSE PWM signal is connected to
#define M1_PWM_REVERSE_CCn			1								///< CC channel used for M1 PWM REVERSE
#define M1_PWM_REVERSE_FUNCTION		GPIO_PIN_FUNCTION_F

// Input
#define M1_ENDSTOP_PIN				GPIO(GPIO_PORTB, 8)				///< Pin where ENDSTOP hall sensor of M1 is connected to
#define M1_ENDSTOP_EICn				8								///< Interrupt number of M1 ENDSTOP input

#define M1_OCM_PIN					GPIO(GPIO_PORTA, 5)				///< Pin of the ADC input channel for M1 CURRENT MEASURING
#define M1_OCM_ADCn					0								///< Analog to digital conversion (ADC) unit used for M1 CURRENT MEASURING
#define M1_OCM_ADC_AINn				5								///< ADC channel used for M1 CURRENT MEASURING
#define M1_OCM_R_SENSE				220.f							///< [Ohm] Resistor value with that the open drain Motor driver is connected to ground

#define M1_DIAG_PIN					GPIO(GPIO_PORTB, 28)			///< Pin where M1 DIAGNOSTIC output is connected to
#define M1_DIAG_EICn				14								///< External interrupt number of M1 DIAGNOSTIC

#define M1_ENCODER_A_PIN			GPIO(GPIO_PORTB, 4)
#define M1_ENCODER_A_EICn			4

#define M1_ENCODER_B_PIN			GPIO(GPIO_PORTA, 6)
#define M1_ENCODER_B_EICn			6

#define M1_ENCODER_EVSYS_CHANNEL	1

#define M1_GEAR_RATIO				M_GEAR_RATIO_POLOLU_100			///< This value contains the gear ration of a 100:1 reduction gear this will contain 100
#endif // M1_PRESENT


/*******************************************************************************/
#ifdef M2_PRESENT
// Output
#define M2_EN_PIN					GPIO(GPIO_PORTA, 27)			///< Pin to send ENABLE signal to Motor 2 (M2)

#define M2_PWM_TCCn					0								///< TCC instance used for M2 PWMs (Forward and reverse)
#define M2_PWM_OTMX					0x0								///< TCC Output Matrix Value used

#define M2_PWM_FORWARD_PIN			GPIO(GPIO_PORTD, 10)			///< Pin where M2 FORWARD PWM signal is connected to
#define M2_PWM_FORWARD_CCn			3								///< CC channel used for M1 PWM FORWARD
#define M2_PWM_FORWARD_FUNCTION		GPIO_PIN_FUNCTION_F

#define M2_PWM_REVERSE_PIN			GPIO(GPIO_PORTD, 11)			///< Pin where M2 REVERSE PWM signal is connected to
#define M2_PWM_REVERSE_CCn			4								///< CC channel used for M1 PWM REVERSE
#define M2_PWM_REVERSE_FUNCTION		GPIO_PIN_FUNCTION_F

// Input
#define M2_ENDSTOP_PIN				GPIO(GPIO_PORTB, 9)				///< Pin where ENDSTOP hall sensor of M2 is connected to
#define M2_ENDSTOP_EICn				9								///< Interrupt number of M2 ENDSTOP input

#define M2_OCM_PIN					GPIO(GPIO_PORTA, 4)				///< Pin of the ADC input channel for M2 CURRENT MEASURING
#define M2_OCM_ADCn					0								///< Analog to digital conversion (ADC) unit used for M2 CURRENT MEASURING
#define M2_OCM_ADC_AINn				4								///< ADC channel used for M2 CURRENT MEASURING
#define M2_OCM_R_SENSE				220.f							///< Resistor value with that the open drain Motor driver is connected to ground

#define M2_DIAG_PIN					GPIO(GPIO_PORTB, 27)			///< Pin where M2 DIAGNOSTIC output is connected to
#define M2_DIAG_EICn				13								///< External interrupt number of M2 DIAGNOSTIC

#define M2_ENCODER_A_PIN			GPIO(GPIO_PORTB, 5)
#define M2_ENCODER_A_EICn			5

#define M2_ENCODER_B_PIN			GPIO(GPIO_PORTA, 7)
#define M2_ENCODER_B_EICn			7

#define M2_ENCODER_EVSYS_CHANNEL	2

#define M2_GEAR_RATIO				M_GEAR_RATIO_POLOLU_100			///< This value contains the gear ration of a 100:1 reduction gear this will contain 100
#endif // M2_PRESENT


#endif //_HARDWARE_DEFINES_H_