#ifndef CONTROL_FLAGS_H_
#define CONTROL_FLAGS_H_

typedef struct {
	uint8_t run_motor_controllers : 1;
	uint8_t heartbeat : 1;
	uint8_t spare : 6;
} control_flags_t;

extern volatile control_flags_t control_flags;

#endif //CONTROL_FLAGS_H_