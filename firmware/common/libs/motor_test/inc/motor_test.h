#ifndef MOTOR_TEST_H_
#define MOTOR_TEST_H_

#include "hardware_defines.h"
#include "sam5_dc_motor_controller.h"

void motor_test_init(dc_motor_controller_t* controller);
void motor_test_run(dc_motor_controller_t* controller);
void motor_test_record_measurement(dc_motor_controller_t* controller);

#endif // MOTOR_TEST_H_