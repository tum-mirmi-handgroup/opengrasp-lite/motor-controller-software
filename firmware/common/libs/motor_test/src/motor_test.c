#include "motor_test.h"
#include "control_flags.h"

#include <string.h>

#if DO_MOTOR_TEST

#define BUFFER_SIZE 4000
#define BUFFER_PRESCALER 10
#define LOOP_COUNT (BUFFER_SIZE * BUFFER_PRESCALER)

static uint32_t start_count;
static uint8_t toggle;
static struct io_descriptor *io;

static float32_t voltage[BUFFER_SIZE];
static float32_t current[BUFFER_SIZE];
static float32_t velocity[BUFFER_SIZE];
static float32_t velocity_smoothed[BUFFER_SIZE];
static float32_t position[BUFFER_SIZE];
static float32_t acceleration[BUFFER_SIZE];
static uint32_t p_buffer_pos;
static uint8_t recording_flag;
static uint8_t first_run;


static dc_motor_controller_input_flags_t flags;

void motor_test_init(dc_motor_controller_t* controller){
	usart_sync_get_io_descriptor(&USART_0, &io);
	usart_sync_enable(&USART_0);
	
	
	supply_voltage = 16;
	flags.control_mode = MOTOR_CM_POSITION;
	flags.continue_at_end = 0;
	flags.stop_at_contact = 1;
	flags.use_aggressive_contact_detection = 0;
	controller->inputs_com.flags.control_mode = MOTOR_CM_CALIB_PREP_POS;
	controller->inputs_com.flags.continue_at_end = 1;
	controller->inputs_com.flags.stop_at_contact = 0;
	controller->inputs_com.current = 0.4;
	controller->inputs_com.velocity = 250;
	controller->inputs_com.angle = 50000;
	controller->inputs_com.voltage = -1.5;
	
	control_flags.run_motor_controllers = 1;
	start_count = 0;
	toggle = 0;
	p_buffer_pos = 0;
	recording_flag = 0;
	first_run = 1;
}

void motor_test_run(dc_motor_controller_t* controller){
	if(controller->state.flags_b.calib_stage == CALIB_STAGE_WAITING2){
		controller->inputs_com.flags.control_mode = MOTOR_CM_CALIBRATION;
	}
	else if(controller->internals.loop_iteration_count > start_count && controller->state.flags_b.calib_stage == CALIB_STAGE_FINISHED){
		while(recording_flag == 1){}
		controller->inputs_com.flags.control_mode = MOTOR_CM_STOPPED;
		if(first_run){
			io_write(io, (const uint8_t*) "Ca+\r\n", 5);
			io_write(io, (const uint8_t*) &controller->internals.contact_detection.data_cur_pos, sizeof(controller->internals.contact_detection.data_cur_pos));
			io_write(io, (const uint8_t*) "\r\n", 2);
			io_write(io, (const uint8_t*) "Ca-\r\n", 5);
			io_write(io, (const uint8_t*) &controller->internals.contact_detection.data_cur_neg, sizeof(controller->internals.contact_detection.data_cur_neg));
			io_write(io, (const uint8_t*) "\r\n", 2);
		}
		else{
			//io_write(io, (const uint8_t*) "Vol\r\n", 5);
			//io_write(io, (const uint8_t*) &voltage[0], sizeof(float32_t) * BUFFER_SIZE);
			//io_write(io, (const uint8_t*) "\r\n", 2);
			//io_write(io, (const uint8_t*) "Cur\r\n", 5);
			//io_write(io, (const uint8_t*) &current[0], sizeof(float32_t) * BUFFER_SIZE);
			//io_write(io, (const uint8_t*) "\r\n", 2);
			//io_write(io, (const uint8_t*) "Vel\r\n", 5);
			//io_write(io, (const uint8_t*) &velocity[0], sizeof(float32_t) * BUFFER_SIZE);
			//io_write(io, (const uint8_t*) "\r\n", 2);
			//io_write(io, (const uint8_t*) "Ve2\r\n", 5);
			//io_write(io, (const uint8_t*) &velocity_smoothed[0], sizeof(float32_t) * BUFFER_SIZE);
			//io_write(io, (const uint8_t*) "\r\n", 2);
			//io_write(io, (const uint8_t*) "Pos\r\n", 5);
			//io_write(io, (const uint8_t*) &position[0], sizeof(float32_t) * BUFFER_SIZE);
			//io_write(io, (const uint8_t*) "\r\n", 2);
			//io_write(io, (const uint8_t*) "Acc\r\n", 5);
			//io_write(io, (const uint8_t*) &acceleration[0], sizeof(float32_t) * BUFFER_SIZE);
			//io_write(io, (const uint8_t*) "\r\n", 2);
		}
		first_run = 0;
		recording_flag = 1;
		delay_ms(10);
		
		CRITICAL_SECTION_ENTER()
		controller->inputs_com.flags.control_mode = flags.control_mode;
		controller->inputs_com.flags.stop_at_contact = flags.stop_at_contact;
		controller->inputs_com.flags.continue_at_end = flags.continue_at_end;
		const float32_t angle_range = controller->limits.angle_max - controller->limits.angle_min;
		controller->inputs_com.angle = controller->limits.angle_min + (toggle ? 0.9f : 0.1f) * angle_range;
		toggle = !toggle;
		start_count = controller->internals.loop_iteration_count + LOOP_COUNT + 50;
		CRITICAL_SECTION_LEAVE()
	}
}

void motor_test_record_measurement(dc_motor_controller_t* controller){
	if(recording_flag != 0){
		if((controller->internals.loop_iteration_count % BUFFER_PRESCALER) == 0){
			voltage[p_buffer_pos] = controller->state.voltage;
			current[p_buffer_pos] = controller->state.current;
			velocity[p_buffer_pos] = controller->state.velocity;
			position[p_buffer_pos] = controller->control.position_controller.buffer.error[0];
			velocity_smoothed[p_buffer_pos] = controller->internals.sensors.ema_filtered_velocity;
			acceleration[p_buffer_pos] = controller->internals.sensors.ema_filtered_acceleration;
			++p_buffer_pos;
			if(p_buffer_pos >= BUFFER_SIZE){
				p_buffer_pos = 0;
				recording_flag = 0;
			}
			
		}
	}
}

#else

void motor_test_init(dc_motor_controller_t* controller){}
void motor_test_run(dc_motor_controller_t* controller){}
void motor_test_record_measurement(dc_motor_controller_t* controller){}

#endif