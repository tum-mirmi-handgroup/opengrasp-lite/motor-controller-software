# SAM5-SW-CRC8-ECC

A repository to calculate CRC8 of input data in software, as the hardware implementation of SAM5 chips only support CRC32 and this is too much transmission overhead.

It also provides 1-bit Erroro Correction for messages with a maximum of 14 bytes length.

## Repository Structure

The repo consists of 4 directories:
	- `src` The source files
	- `inc` The header files
	- `test` GTest based unittests (build with CMake)
	- `generation` Python scripts that can be used to generate and validate the CRC and ECC tables used in the implementation
