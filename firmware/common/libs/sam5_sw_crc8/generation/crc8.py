from dataclasses import dataclass, field
from typing import Dict, Optional
from representation import Representation


@dataclass(frozen=True)
class CRC8:
    table: Dict[int, int] = field(init=False, default_factory=dict)
    poly: Representation
    init: int
    xor_out: int
    refin: bool
    refout: bool
    check: Optional[int]

    def __post_init__(self):
        self.create_table()
        assert len(self.table) == 256
        if self.check is not None:
            assert self.check == self.calc(b"123456789"), f"{self.check} != {self.calc(b'123456789')}"

    def calc(self, data: bytes) -> int:
        crc = self.init
        for byte in data:
            byte = self._reverse_bits(byte) if self.refin else byte
            crc = self.table[byte ^ crc]
        if self.refout:
            crc = self._reverse_bits(crc)
        return crc ^ self.xor_out

    @staticmethod
    def _reverse_bits(data: int) -> int:
        data = bin(data)[2:]
        data = "0" * (8 - len(data)) + data
        assert len(data) == 8
        return int(data[::-1], 2)

    def create_table(self):
        self.table.clear()
        for i in range(256):
            crc = i
            for _ in range(8):
                if (crc & 0x80) != 0:
                    crc <<= 1
                    crc %= 256
                    crc ^= self.poly
                else:
                    crc <<= 1
            self.table[i] = crc

    def print_table(self, bytes_per_line):
        print("const uint8_t sam5_sw_crc8_CrcTable[256] = {\r\n\t", end="")
        for i, (key, value) in enumerate(self.table.items()):
            assert i == key
            if i > 0 and i % bytes_per_line == 0:
                print("\r\n\t", end="")
            print(f"0x{value:02X}", end="")
            if i != 255:
                print(", ", end="")
        print("\r\n};")


algorithms = {
    "AUTOSAR": CRC8(
        poly=Representation.from_normal(0x2f),
        init=0xFF,
        xor_out=0xFF,
        refin=False,
        refout=False,
        check=0xdf,
    ),
    "BLUETOOTH": CRC8(
        poly=Representation.from_normal(0xa7),
        init=0x00,
        refin=True,
        refout=True,
        xor_out=0x00,
        check=0x26,
    ),
    "CDMA2000": CRC8(
        poly=Representation.from_normal(0x9b),
        init=0xff,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0xda,
    ),
    "DARC": CRC8(
        poly=Representation.from_normal(0x39),
        init=0x00,
        refin=True,
        refout=True,
        xor_out=0x00,
        check=0x15,
    ),
    "DVB-S2": CRC8(
        poly=Representation.from_normal(0xd5),
        init=0x00,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0xbc,
    ),
    "GSM-A": CRC8(
        poly=Representation.from_normal(0x1d),
        init=0x00,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0x37,
    ),
    "GSM-B": CRC8(
        poly=Representation.from_normal(0x49),
        init=0x00,
        refin=False,
        refout=False,
        xor_out=0xff,
        check=0x94,
    ),
    "HITAG": CRC8(
        poly=Representation.from_normal(0x1d),
        init=0xff,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0xb4,
    ),
    "I-432-1": CRC8(
        poly=Representation.from_normal(0x07),
        init=0x00,
        refin=False,
        refout=False,
        xor_out=0x55,
        check=0xa1,
    ),
    "I-CODE": CRC8(
        poly=Representation.from_normal(0x1d),
        init=0xfd,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0x7e,
    ),
    "LTE": CRC8(
        poly=Representation.from_normal(0x9b),
        init=0x00,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0xea,
    ),
    "MAXIM-DOW": CRC8(
        poly=Representation.from_normal(0x31),
        init=0x00,
        refin=True,
        refout=True,
        xor_out=0x00,
        check=0xa1,
    ),
    "MIFARE-MAD": CRC8(
        poly=Representation.from_normal(0x1d),
        init=0xc7,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0x99,
    ),
    "NRSC-5": CRC8(
        poly=Representation.from_normal(0x31),
        init=0xff,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0xf7,
    ),
    "OPENSAFETY": CRC8(
        poly=Representation.from_normal(0x2f),
        init=0x00,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0x3e,
    ),
    "ROHC": CRC8(
        poly=Representation.from_normal(0x07),
        init=0xff,
        refin=True,
        refout=True,
        xor_out=0x00,
        check=0xd0,
    ),
    "SAE-J1850": CRC8(
        poly=Representation.from_normal(0x1d),
        init=0xff,
        refin=False,
        refout=False,
        xor_out=0xff,
        check=0x4b,
    ),
    "SMBUS": CRC8(
        poly=Representation.from_normal(0x07),
        init=0x00,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0xf4,
    ),
    "TECH-3250": CRC8(
        poly=Representation.from_normal(0x1d),
        init=0xff,
        refin=True,
        refout=True,
        xor_out=0x00,
        check=0x97,
    ),
    "WCDMA": CRC8(
        poly=Representation.from_normal(0x9b),
        init=0x00,
        refin=True,
        refout=True,
        xor_out=0x00,
        check=0x25,
    ),
    "CUSTOM_HD4": CRC8(
        poly=Representation.from_normal(0x07),
        init=0xff,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0xfb
    ),
    "CUSTOM_HD3": CRC8(
        poly=Representation.from_normal(0x4d),
        init=0xff,
        refin=False,
        refout=False,
        xor_out=0x00,
        check=0x06
    ),
}

if __name__ == '__main__':
    pass
