#ifndef SAM5_CONTROL_H_
#define SAM5_CONTROL_H_

#include <arm_neon.h>

typedef struct
{	
	float32_t k_p;		///< The overall gain for the PID controller		u(t) = K_p*[e(t) + k_i*e(t)*1/s + K_d*e(t)*s]
	float32_t k_i;		///< The integral gain for the PID controller		K_i = 1/T_i
	float32_t k_d;		///< The differential gain for the PID controller	K_d = T_d
	
	float32_t k_t;		///< The gain for the back calculation k_t = 1/T_t, a rule of thumb is T_t = T_i if no D part available, else T_t = sqrt(T_i*T_d)
	float32_t n;		///< Filter coefficient, Usual values are 8 to 20. The bigger N get's the higher the cutoff frequency of the low pass will be, as tau = k_d/N = T_d/N. Set N to zero to deactivate the filter
	float32_t hyst;		///< Hysteresis to avoid oscillating if we are close enough to the target
} sam5_control_settings_t;

typedef struct
{
	float32_t out[3];		///<     out[0] = u(t)		//     out[1] = u(t-1)		//     out[2] = u(t-2)
	float32_t error[3];		///<   error[0] = e(t)		//   error[1] = e(t-1)		//   error[2] = e(t-2)
	float32_t error_s[3];	///< error_s[0] = e_s(t)	// error_s[1] = e_s(t-1)	// error_s[2] = e_s(t-2)
	float32_t filter[3];	///<  filter[0] = f(t)		//  filter[1] = f(t-1)		//  filter[2] = f(t-2)
} sam5_control_buffer_t;

typedef struct
{
	// Double buffered settings so we can track if a parameter was updated via I2C
	sam5_control_settings_t settings;
	sam5_control_settings_t settings_com;
	
	sam5_control_buffer_t buffer;
	float32_t tau;							///< The current derivative filter tau
	float32_t out_max;						///< The current applied limit
	
	float32_t *const p_setpoint;			///< Pointer to the reference value
	float32_t *const p_process_value;		///< Pointer to the feedback value

	float32_t *const p_out_max_1;			///< Pointer to the first absolute saturation limit for the controller output - Used saturation limit is the lower one
	float32_t *const p_out_max_2;			///< Pointer to the second absolute saturation limit for the controller output - Used saturation limit is the lower one
	float32_t *const p_out;					///< Pointer to the controller output
} sam5_controller_t;

void sam5_control_reset_controller(sam5_controller_t* c);

// If you want to be flexible and able to switch between different controllers like a p and pi, use this function. 
// If you have tested enough and chosen the controller you want, you can use another function to dial the decision it in and squeeze out the last bit of performance
void sam5_control_run(sam5_controller_t *c, float32_t delta_t);

void sam5_control_p(sam5_controller_t *c, float32_t delta_t);
void sam5_control_pi(sam5_controller_t *c, float32_t delta_t);
void sam5_control_pd(sam5_controller_t *c, float32_t delta_t);
void sam5_control_pid(sam5_controller_t *c, float32_t delta_t);

#endif //SAM5_CONTROL_H_