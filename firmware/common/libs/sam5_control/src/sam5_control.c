#include "sam5_control.h"

#include <string.h>
#include <atmel_start.h>
#include <math.h>
#include <utils.h>
#include <arm_math.h>

#define u(index) c->buffer.out[index]
#define e(index) c->buffer.error[index]
#define e_s(index) c->buffer.error_s[index]
#define f(index) c->buffer.filter[index]
#define u_lim c->out_max
#define Hyst c->settings.hyst
#define K_p c->settings.k_p
#define K_i c->settings.k_i
#define K_d c->settings.k_d
#define K_t c->settings.k_t
#define N c->settings.n
#define Tau c->tau
#define SP *c->p_setpoint
#define PV *c->p_process_value

static inline float32_t limit_output_and_calc_error_s(sam5_controller_t* c){
	if(u(0) < -u_lim){
		e_s(0) = -u_lim - u(0);
		return -u_lim;
	}
	else if(u(0) > u_lim){
		e_s(0) = u_lim - u(0);
		return u_lim;
	}
	else{
		e_s(0) = 0;
		return u(0);
	}
}

static inline float32_t get_saturation_limit(const sam5_controller_t* c){
	const float32_t lim1 = c->p_out_max_1 == NULL ? INFINITY : (*c->p_out_max_1 > 0 ? *c->p_out_max_1 : -*c->p_out_max_1);
	const float32_t lim2 = c->p_out_max_2 == NULL ? INFINITY : (*c->p_out_max_2 > 0 ? *c->p_out_max_2 : -*c->p_out_max_2);
	return min(lim1, lim2); 
}

static inline void check_for_changed_parameters(sam5_controller_t* c){
	const float32_t new_u_lim = get_saturation_limit(c);
	if(K_i != c->settings_com.k_i || K_p != c->settings_com.k_p || K_t != c->settings_com.k_t || N != c->settings_com.n || Hyst != c->settings_com.hyst || (new_u_lim < u_lim && u(0) > new_u_lim)){
		sam5_control_reset_controller(c);
	}
	u_lim = new_u_lim;
}

#define HYSTERESIS_EXTRA_SLOPE	1000

static inline float32_t calc_error(const sam5_controller_t* c){
	if(Hyst == 0){
		return SP - PV;
	}
	
	// Apply a hysteresis to the error function (You can see the absolute error depicted below)
	//\_                                                       _/
	//   \_                                                 _/
	//      \_   |                                   |   _/
	//         \_|                                   |_/
	//		     |\                                 /|
	//		     | \                               / |
	//		     |  \                             /  |
	//		     |   \                           /   |
	//		     |    \                         /    |
	//		     |     \                       /     |
	//		     |      \ |        |        | /      |
	//___________|_______\|________0________|/_______|___________
	//           |        |  hyst  |  hyst  |        |
	//           |     2*hyst      |     2*hyst      |
	const float32_t error = SP - PV;
	if(error < 0){
		if(error > -Hyst){
			return 0;
		}
		else if(error > -(1 + 1.f / HYSTERESIS_EXTRA_SLOPE)*Hyst){
			return (error + Hyst) * (1 + HYSTERESIS_EXTRA_SLOPE);
		}
	}
	else{
		if(error < Hyst){
			return 0;
		}
		else if(error < (1 + 1.f / HYSTERESIS_EXTRA_SLOPE)*Hyst){
			return (error - Hyst) * (1 + HYSTERESIS_EXTRA_SLOPE);
		}
	}
	return error;
}

static inline void apply_output(sam5_controller_t* c){
	u(2) = u(1);
	u(1) = u(0);
	
	e(2) = e(1);
	e(1) = e(0);
	
	e_s(2) = e_s(1);
	e_s(1) = e_s(0);
	
	f(2) = f(1);
	f(1) = f(0);
	
	*c->p_out = limit_output_and_calc_error_s(c);
}

void sam5_control_reset_controller(sam5_controller_t* c){
	c->settings.k_p = c->settings_com.k_p > 0 ? c->settings_com.k_p : 0;
	c->settings.k_i = c->settings_com.k_i > 0 ? c->settings_com.k_i : 0;
	c->settings.k_d = c->settings_com.k_d > 0 ? c->settings_com.k_d : 0;
	c->settings.k_t = c->settings_com.k_t > 0 ? c->settings_com.k_t : 0;
	c->settings.n = c->settings_com.n > 0 ? c->settings_com.n : 0;
	c->settings.hyst = c->settings_com.hyst > 0 ? c->settings_com.hyst : 0;
	
	// Copy clamped settings back into the com struct so we only reset the controller if the com struct was really update from externally. 
	// If we do not do this and one of the settings is smaller than 0 in the com struct, the controller will be reset permanently
	memcpy(&c->settings_com, &c->settings, sizeof(sam5_control_settings_t));
	memset(&c->buffer, 0, sizeof(sam5_control_buffer_t));
	u_lim = get_saturation_limit(c);
	Tau = K_d/N;
}

void sam5_control_run(sam5_controller_t *c, float32_t delta_t){
	if(K_p == 0){
		return;
	}
	if(K_i == 0){
		if(K_d == 0){
			sam5_control_p(c, delta_t);
		}
		else{
			sam5_control_pd(c, delta_t);
		}
	}
	else{
		if(K_d == 0){
			sam5_control_pi(c, delta_t);
		}
		else{
			sam5_control_pid(c, delta_t);
		}
	}
}
//######################################################################################################################
//**********************************************************************************************************************
// The calculations on how to derive the discretized controllers using bilinear discretization are in the attached PDF
//**********************************************************************************************************************

static inline float32_t calc_filtered_error(const sam5_controller_t* c, float32_t delta_t_half){
	if(N == 0){
		return e(0);
	}
	else{
		return ((Tau - delta_t_half)*f(1) + delta_t_half*(e(0) + e(1))) / (Tau + delta_t_half);
	}
}

void sam5_control_p(sam5_controller_t* c, float32_t delta_t){
	check_for_changed_parameters(c);
	e(0) = calc_error(c);
	u(0) = K_p * e(0);
	apply_output(c);
}

void sam5_control_pi(sam5_controller_t* c, float32_t delta_t){
	check_for_changed_parameters(c);
	e(0) = calc_error(c);
	const float32_t delta_t_half = 0.5f * delta_t;
	u(0) = u(1) + K_p * (e(0) - e(1) + K_i * delta_t_half * (e(0) + e(1))) + K_t * delta_t_half * (e_s(0) + e_s(1));
	//                  |----------|   |---------------------------------|   |------------------------------------|
	//                  Proportional               Integral                            Back calculation
	apply_output(c);
}

void sam5_control_pd(sam5_controller_t* c, float32_t delta_t){
	check_for_changed_parameters(c);
	const float32_t delta_t_half = 0.5f * delta_t;
	e(0) = calc_error(c);
	f(0) = calc_filtered_error(c, delta_t_half);
	u(0) = -u(1) + K_p * (e(0) + e(1) + K_d * (f(0) + f(1)) / delta_t_half);
	//                   |----------|   |----------------------------------|
	//                   Proportional     Differential with filtered e(t)
	apply_output(c);
}

void sam5_control_pid(sam5_controller_t* c, float32_t delta_t){
	check_for_changed_parameters(c);
	const float32_t delta_t_half = 0.5f * delta_t;
	e(0) = calc_error(c);
	f(0) = calc_filtered_error(c, delta_t_half);
	u(0) = u(2) + K_p * (e(0) - e(2) + K_i * delta_t_half * (e(0) + 2 * e(1) + e(2)) + K_d * (f(0)-2*f(1)+f(2)) / delta_t_half) + K_t * delta_t_half * (e_s(0) + 2 * e_s(1) + e_s(2));
	//                  |----------|   |-------------------------------------------|   |--------------------------------------|   |-------------------------------------------------|
	//                  Proportional                 Integral                             Differential with filtered e(t)                          Back calculation
	apply_output(c);
}