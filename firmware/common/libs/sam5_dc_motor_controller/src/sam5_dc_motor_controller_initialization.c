#include <utils.h>
#include "sam5_dc_motor_controller_impl.h"
#include "sam5_dc_motor_controller_macros.h"

#define NUMBER_CALIBRATION_MEASUREMENTS 10000

typedef struct {
	void (*mclk_set_func)(const void *const hw);
	uint8_t tc_gclk_id;
} TC_MCLK_Config_t;

const TC_MCLK_Config_t tc_mclk_configs[] = {
	{hri_mclk_set_APBAMASK_TC0_bit, TC0_GCLK_ID},
	{hri_mclk_set_APBAMASK_TC1_bit, TC1_GCLK_ID},
	{hri_mclk_set_APBBMASK_TC2_bit, TC2_GCLK_ID},
	{hri_mclk_set_APBBMASK_TC3_bit, TC3_GCLK_ID},
	#ifdef TC4_GCLK_ID
	{hri_mclk_set_APBCMASK_TC4_bit, TC4_GCLK_ID},
	#endif
	#ifdef TC5_GCLK_ID
	{hri_mclk_set_APBCMASK_TC5_bit, TC5_GCLK_ID},
	#endif
	#ifdef TC6_GCLK_ID
	{hri_mclk_set_APBDMASK_TC6_bit, TC6_GCLK_ID},
	#endif
	#ifdef TC7_GCLK_ID
	{hri_mclk_set_APBDMASK_TC7_bit, TC7_GCLK_ID},
	#endif
};

uint8_t initialize_motor_controller_peripherals(dc_motor_controller_t *controller) {

	/// Initialize PINs
	initialize_motor_controller_pins(controller);

	/// Initialize PWM timer/counter
	initialize_motor_controller_pwm(controller);

	/// Initialize external event channels
	initialize_motor_controller_eic(controller);

	/// Initialize Voltage Reference Systems
	initialize_motor_controller_vref(controller);

	/// Initialize speed timer
	initialize_motor_controller_tc(controller);

	/// Initialize event system
	initialize_motor_controller_evsys(controller);

	return 0;
}

void set_input_pin_with_function(uint32_t pin, uint32_t function) {
	gpio_set_pin_direction(pin, GPIO_DIRECTION_IN);
	gpio_set_pin_pull_mode(pin, GPIO_PULL_OFF);
	gpio_set_pin_function(pin, function);
}

void set_output_pin_with_function(uint32_t pin, uint32_t function) {
	gpio_set_pin_level(pin, false);
	gpio_set_pin_direction(pin, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(pin, function);
}

uint8_t initialize_motor_controller_pins(dc_motor_controller_t *controller) {
	dc_controller_hardware_param_t hw = controller->hardware;

	// Output
	set_output_pin_with_function(hw.en_pin, GPIO_PIN_FUNCTION_OFF);     // no special function
	set_output_pin_with_function(hw.pwm_forward.pin, hw.pwm_forward.funct);  // function F: TCC
	set_output_pin_with_function(hw.pwm_reverse.pin, GPIO_PIN_FUNCTION_F);  // function F: TCC

	// Input
	set_input_pin_with_function(hw.endstop_pin, GPIO_PIN_FUNCTION_A);   // function A: EIC/EXTINT
	set_input_pin_with_function(hw.ocm_pin, GPIO_PIN_FUNCTION_B);       // function B: ADC
	set_input_pin_with_function(hw.diag_pin, GPIO_PIN_FUNCTION_A);      // function A: EIC/EXTINT
	set_input_pin_with_function(hw.encoder_a.pin, GPIO_PIN_FUNCTION_A);     // function A: EIC/EXTINT
	set_input_pin_with_function(hw.encoder_b.pin, GPIO_PIN_FUNCTION_A);     // function A: EIC/EXTINT

	return 0;
}

uint8_t initialize_motor_controller_pwm(dc_motor_controller_t *controller) {
	/// Initialize TC for motor voltage control
	/// Initialize clock
	uint8_t tcc_gclk_id;
	switch (controller->hardware.pwm_tccn) {
		case 0:
		hri_mclk_set_APBBMASK_TCC0_bit(MCLK);
		tcc_gclk_id = TCC0_GCLK_ID;
		break;
		case 1:
		hri_mclk_set_APBBMASK_TCC1_bit(MCLK);
		tcc_gclk_id = TCC1_GCLK_ID;
		break;
		case 2:
		hri_mclk_set_APBCMASK_TCC2_bit(MCLK);
		tcc_gclk_id = TCC2_GCLK_ID;
		break;
		case 3:
		hri_mclk_set_APBCMASK_TCC3_bit(MCLK);
		tcc_gclk_id = TCC3_GCLK_ID;
		break;
		case 4:
		hri_mclk_set_APBDMASK_TCC4_bit(MCLK);
		tcc_gclk_id = TCC4_GCLK_ID;
		break;
		default:
		return -1;
	}

	hri_gclk_write_PCHCTRL_reg(GCLK, tcc_gclk_id, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));

	/// Check if no software reset of TCC is in progress
	if (!hri_tcc_is_syncing(controller->hardware.p_pwm_tcc, TCC_SYNCBUSY_SWRST)) {
		/// If TCC is already enabled
		if (hri_tcc_get_CTRLA_reg(controller->hardware.p_pwm_tcc, TCC_CTRLA_ENABLE)) {
			///* Disable TCC
			hri_tcc_clear_CTRLA_ENABLE_bit(controller->hardware.p_pwm_tcc);
			///* Wait until TCC is disabled
			hri_tcc_wait_for_sync(controller->hardware.p_pwm_tcc, TCC_SYNCBUSY_ENABLE);
		}
		/// Perform a software reset of TCC
		hri_tcc_write_CTRLA_reg(controller->hardware.p_pwm_tcc, TCC_CTRLA_SWRST);
	}
	/// Wait until the software reset is finished
	hri_tcc_wait_for_sync(controller->hardware.p_pwm_tcc, TCC_SYNCBUSY_SWRST);

	/**
	* Clear Control Register A:
	* - Disable capture on all channels
	* - Disable DMA One-shot trigger Mode
	* - Disable Host/Slave coupling of multiple TCCs
	* - Disable Auto Lock
	* - Reload or reset counter on next GLCK
	* - Halt TCC in standby
	* - Set prescaler to 1
	* - Disable dithering
	*/
	hri_tcc_clear_CTRLA_reg(controller->hardware.p_pwm_tcc, ~(hri_tcc_ctrla_reg_t)0);

	/// Clear Control Register B
	hri_tcc_clear_CTRLB_reg(controller->hardware.p_pwm_tcc, ~(hri_tcc_ctrlbset_reg_t)0);
	
	/// Disable dead time insertion on all outputs
	hri_tcc_clear_WEXCTRL_reg(controller->hardware.p_pwm_tcc, ~(hri_tcc_wexctrl_reg_t)0);
	
	/// Apply selected output matrix
	hri_tcc_write_WEXCTRL_OTMX_bf(controller->hardware.p_pwm_tcc, controller->hardware.pwm_otmx);
	
	/// Enable normal operation during debug (To avoid voltage spikes for the motor at break points)
	hri_tcc_set_DBGCTRL_DBGRUN_bit(controller->hardware.p_pwm_tcc);
	
	/// Clear WAVE register:
	/// - Disable swapped dead time insertion on all outputs
	/// - Set default polarity for all channels
	/// - Disable circular buffers on all channels
	hri_tcc_clear_WAVE_reg(controller->hardware.p_pwm_tcc, ~(hri_tcc_wave_reg_t)0);
	
	/// Use Normal PWM (Period is 2^16 and toggle on CCn
	hri_tcc_write_WAVE_WAVEGEN_bf(controller->hardware.p_pwm_tcc, 0x2);
	
	//// Write PERBUF register
	hri_tcc_write_PER_PER_bf(controller->hardware.p_pwm_tcc, CONF_CPU_FREQUENCY / M_PWM_FREQ);
	return 0;
}

void activate_eic_channel_both_edge_detection(uint8_t diag_eic_number) {
	// Calculate the index of the CONFIG register and the bit position within the register
	uint8_t configIndex = diag_eic_number / 8;
	uint8_t bitPosition = (diag_eic_number % 8) * 4;

	// Calculate the bit mask for enabling the filter
	uint32_t filterMask = 1 << (bitPosition + 3);

	// Calculate the bit mask for both edges edge detection
	uint32_t edgeMask = 0x3U << bitPosition;

	// Set edges to both edge detection and enable the filter for the EIC channel
	hri_eic_set_CONFIG_reg(EIC, configIndex, filterMask | edgeMask);
}

uint8_t initialize_motor_controller_eic(dc_motor_controller_t *controller) {

	/// Initialize EIC for Hall pattern change and fault detection
	/// Initialize clock
	hri_gclk_write_PCHCTRL_reg(GCLK, EIC_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_mclk_set_APBAMASK_EIC_bit(MCLK);
	/// Check if software reset of EIC is in progress
	if (hri_eic_is_syncing(EIC, EIC_SYNCBUSY_SWRST)) {
		/// * Wait until the software reset is finished
		hri_eic_wait_for_sync(EIC, EIC_SYNCBUSY_SWRST);
	}
	/// The EIC is clocked by GCLK_EIC
	hri_eic_write_CTRLA_CKSEL_bit(EIC, 0);

	/// Set edges to both edge detection and enable filter for endstop, diag, encoder_a and encoder_b
	activate_eic_channel_both_edge_detection(controller->hardware.endstop_eic_number);
	activate_eic_channel_both_edge_detection(controller->hardware.diag_eic_number);
	activate_eic_channel_both_edge_detection(controller->hardware.encoder_a.eicn);
	activate_eic_channel_both_edge_detection(controller->hardware.encoder_b.eicn);

	/// Enable interrupts for the pins
	hri_eic_set_INTEN_reg(EIC,
	1 << controller->hardware.endstop_eic_number
	| 1 << controller->hardware.diag_eic_number
	| 1 << controller->hardware.encoder_a.eicn
	| 1 << controller->hardware.encoder_b.eicn);

	// Enable event for the encoder_a pin as well
	hri_eic_set_EVCTRL_reg(EIC,	1 << controller->hardware.encoder_a.eicn);

	/// Set interrupt priority to (0 = highest, 7 = lowest)
	NVIC_SetPriority(EIC_0_IRQn + controller->hardware.encoder_a.eicn, NVIC_PRIORITY_MOTOR_ENCCODER);
	NVIC_SetPriority(EIC_0_IRQn + controller->hardware.encoder_b.eicn, NVIC_PRIORITY_MOTOR_ENCCODER);
	NVIC_SetPriority(EIC_0_IRQn + controller->hardware.endstop_eic_number, NVIC_PRIORITY_MOTOR_ENDSTOP);
	NVIC_SetPriority(EIC_0_IRQn + controller->hardware.diag_eic_number, NVIC_PRIORITY_MOTOR_DIAG);

	return 0;
}

uint8_t initialize_motor_controller_tc(dc_motor_controller_t *controller) {
	/// Initialize TC1 for speed measurement
	/// Initialize clock

	if (controller->hardware.encoder_tcn >= sizeof(tc_mclk_configs) / sizeof(TC_MCLK_Config_t)) {
		return 1;
	}

	TC_MCLK_Config_t tc_config = tc_mclk_configs[controller->hardware.encoder_tcn];
	tc_config.mclk_set_func(MCLK);

	hri_gclk_write_PCHCTRL_reg(GCLK, tc_config.tc_gclk_id, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));

	/// Check if no software reset of TC is in progress
	const Tc *tc = controller->hardware.p_encoder_tc;
	if (!hri_tc_is_syncing(tc, TC_SYNCBUSY_SWRST)) {
		/// Check if TC is already enabled
		if (hri_tc_get_CTRLA_reg(tc, TC_CTRLA_ENABLE)) {
			/// * If TC is enabled disable TC
			hri_tc_clear_CTRLA_ENABLE_bit(tc);
			/// * Wait until TC is disabled
			hri_tc_wait_for_sync(tc, TC_SYNCBUSY_ENABLE);
		}
		/// Perform a software reset of TC
		hri_tc_write_CTRLA_reg(tc, TC_CTRLA_SWRST);
	}
	/// Wait until the software reset is finished
	hri_tc_wait_for_sync(tc, TC_SYNCBUSY_SWRST);
	
	/// CTRLA register
	hri_tc_clear_CTRLA_reg(tc, ~(hri_tcc_ctrla_reg_t)0); /// Reset everything to defaults
	
	hri_tc_set_CTRLA_CAPTEN0_bit(tc); /// Enable capture channel 0
	hri_tc_set_CTRLA_CAPTEN1_bit(tc); /// Enable capture channel 1
	
	hri_tc_write_CTRLA_PRESCALER_bf(tc, TC_CTRLA_PRESCALER_DIV16_Val); /// Prescaler: Division by 256
	
	hri_tc_write_CTRLA_MODE_bf(tc, TC_CTRLA_MODE_COUNT16_Val); /// Run in 16 bit mode

	/// Clear CTRLB register
	hri_tc_clear_CTRLB_reg(tc, ~(hri_tcc_ctrlbset_reg_t)0);

	/// EVCTRL register
	hri_tc_clear_EVCTRL_reg(tc, ~(hri_tc_evctrl_reg_t)0);
	
	hri_tc_set_EVCTRL_TCEI_bit(tc); /// Enable TC event input
	hri_tc_set_EVCTRL_TCINV_bit(tc); /// Invert TC event input
	
	hri_tc_write_EVCTRL_EVACT_bf(tc, TC_EVCTRL_EVACT_PPW_Val); ///  Event Action: Pulse width capture into CC0 (0x7)

	/// Write INTEN register
	hri_tc_write_INTEN_reg(tc, 0
	| 1 << TC_INTENSET_MC0_Pos      /// Match or Capture Channel 0 Interrupt Enable: enabled
	| 1 << TC_INTENSET_OVF_Pos);    /// Overflow Interrupt enable: enabled
	
	/// Set interrupt priority of TC interrupt
	NVIC_SetPriority(TC0_IRQn + controller->hardware.encoder_tcn, NVIC_PRIORITY_MOTOR_ENCCODER);

	return 0;
}

uint8_t initialize_motor_controller_evsys(dc_motor_controller_t *controller) {
	/// Initialize EVSYS for motor speed measurement
	/// Initialize clock
	hri_mclk_set_APBBMASK_EVSYS_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, EVSYS_GCLK_ID_0, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));

	/// Configure TC event user to use channel speed_timer_evsys_channel_number
	hri_evsys_write_USER_reg(EVSYS, 44 + controller->hardware.encoder_tcn, controller->hardware.encoder_evsys_channel + 1);

	/// Configure channel speed_timer_evsys_channel_number
	hri_evsys_write_CHANNEL_reg(EVSYS, controller->hardware.encoder_evsys_channel,
	EVSYS_CHANNEL_PATH_ASYNCHRONOUS_Val << EVSYS_CHANNEL_PATH_Pos						/// * Use asynchronous path
	| (0x12 + controller->hardware.encoder_a.eicn) << EVSYS_CHANNEL_EVGEN_Pos);	/// * Use external interrupt from hall sensor A as event generator

	return 0;
}

uint8_t sam5_dc_motor_controller_enable(dc_motor_controller_t *controller) {
	/// Enable external interrupt controller
	hri_eic_set_CTRLA_ENABLE_bit(EIC);

	/// Enable PWM
	hri_tcc_set_CTRLA_ENABLE_bit(controller->hardware.p_pwm_tcc);

	/// Start velocity measurement timer
	hri_tc_set_CTRLA_ENABLE_bit(controller->hardware.p_encoder_tc);

	/// Enable TC interrupt for velocity measurement
	NVIC_DisableIRQ(TC0_IRQn + controller->hardware.encoder_tcn);
	NVIC_ClearPendingIRQ(TC0_IRQn + controller->hardware.encoder_tcn);
	NVIC_EnableIRQ(TC0_IRQn + controller->hardware.encoder_tcn);

	/// Enable EIC interrupts on input pins
	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.endstop_eic_number);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.endstop_eic_number);
	NVIC_EnableIRQ(EIC_0_IRQn + controller->hardware.endstop_eic_number);

	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.diag_eic_number);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.diag_eic_number);
	NVIC_EnableIRQ(EIC_0_IRQn + controller->hardware.diag_eic_number);

	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.encoder_a.eicn);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.encoder_a.eicn);
	NVIC_EnableIRQ(EIC_0_IRQn + controller->hardware.encoder_a.eicn);

	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.encoder_b.eicn);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.encoder_b.eicn);
	NVIC_EnableIRQ(EIC_0_IRQn + controller->hardware.encoder_b.eicn);

	//hri_evsys_set_CTRLA_E
	controller->outputs.voltage = 0;
	/// Toggle motor driver reset latch (ENABLE -> low at the beginning of the function)
	controller->outputs.enable = false;
	apply_outputs(controller);
	delay_us(10);
	controller->outputs.enable = true;
	apply_outputs(controller);
	calibrate_motor_controller_ocm(&g_motor_controller_1);
	return 0;
}

uint8_t calibrate_motor_controller_ocm(dc_motor_controller_t *controller) {
	controller->outputs.voltage = 0;
	controller->outputs.enable = true;
	apply_outputs(controller);
	int64_t rawSum = 0;
	for(uint32_t i = 0; i < NUMBER_CALIBRATION_MEASUREMENTS; ++i) {
		rawSum += receive_measurements(controller);
	}
	controller->calib_res.ocm_raw_offset -= rawSum / NUMBER_CALIBRATION_MEASUREMENTS;
	return 0;
}

uint8_t sam5_dc_motor_controller_init_shared_resources() {
	supply_voltage = 0;
	combined_state.angle[0] = 0;
	combined_state.angle[1] = 0;
	combined_state.angle[2] = 0;
	combined_state.current[0] = 0;
	combined_state.current[1] = 0;
	combined_state.current[2] = 0;
	
	uint8_t initialization_error = 0;

	/// Initialize reference voltage
	initialization_error = initialize_motor_controller_vref();
	if (initialization_error) {
		return 0b00100000 & initialization_error;

	}
	/// Initialize TC for the main loop
	initialization_error = initialize_motor_controller_loop_tc();
	if (initialization_error) {
		return 0b01100000 & initialization_error;
	}

	/// Initialize Analog to Digital converter
	initialization_error = initialize_motor_controller_adc();
	if (initialization_error) {
		return 0b10000000 & initialization_error;
	}
	/// Enable ADC
	hri_adc_set_CTRLA_ENABLE_bit(ADC0);
	hri_adc_set_CTRLA_ENABLE_bit(ADC1);

	/// Initialize Digital to Analog converter for the output of measurements
	initialization_error = initialize_motor_controller_dac();
	if (initialization_error) {
		return 0b10000000 & initialization_error;
	}
	/// Enable ADC
	hri_dac_set_CTRLA_ENABLE_bit(DAC);


	return initialization_error;
}

uint8_t initialize_motor_controller_vref() {
	uint8_t initialization_error = 0;
	hri_supc_write_VREF_reg(SUPC,
	M_SUPC_VREF_SEL << SUPC_VREF_SEL_Pos    /// * Select voltage reference
	| 1 << SUPC_VREF_VREFOE_Pos);            /// * Enable voltage reference
	return initialization_error;
}

uint8_t initialize_motor_controller_loop_tc() {
	///* Enable APB SERCOM units APB communication

	if (M_LOOP_TCn >= sizeof(tc_mclk_configs) / sizeof(TC_MCLK_Config_t)) {
		return 1;
	}

	TC_MCLK_Config_t tc_config = tc_mclk_configs[M_LOOP_TCn];

	tc_config.mclk_set_func(MCLK);
	uint8_t tc_gclk_id = tc_config.tc_gclk_id;

	hri_gclk_write_PCHCTRL_reg(GCLK, tc_gclk_id, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));

	// Check if no software reset is ongoing
	if (!hri_tc_is_syncing(TCn(M_LOOP_TCn), TC_SYNCBUSY_SWRST)) {
		// If TC7 is enable -> disable it
		if (hri_tc_get_CTRLA_ENABLE_bit(TCn(M_LOOP_TCn))) {
			hri_tc_clear_CTRLA_ENABLE_bit(TCn(M_LOOP_TCn));
		}
		// Start software reset
		hri_tc_set_CTRLA_SWRST_bit(TCn(M_LOOP_TCn));
	}
	// Wait for software to finish
	hri_tc_wait_for_sync(TCn(M_LOOP_TCn), TC_SYNCBUSY_SWRST);

	hri_tc_write_CTRLA_reg(TCn(M_LOOP_TCn),	TC_CTRLA_PRESCALER_DIV1_Val << TC_CTRLA_PRESCALER_Pos);

	// Clear CTRLB
	hri_tc_write_CTRLB_reg(TCn(M_LOOP_TCn),	0);

	// Enable Overflow interrupt
	hri_tc_write_INTEN_reg(TCn(M_LOOP_TCn), 1 << TC_INTENSET_OVF_Pos);

	/// Set wavegen to Match frequency (Overflow on match CC0)
	hri_tc_write_WAVE_reg(TCn(M_LOOP_TCn),1 << TC_WAVE_WAVEGEN_Pos);
	uint16_t buf = CONF_CPU_FREQUENCY / (3 * M_CONTROLLER_FREQ);
	hri_tccount16_set_CC_CC_bf(TCn(M_LOOP_TCn), 0, buf);

	NVIC_SetPriority(TC0_IRQn + M_LOOP_TCn, NVIC_PRIORITY_MOTOR_CTRL_LOOP);

	return 0;
}

uint8_t initialize_motor_controller_adc() {
	/// Initialize ADC
	/// Initialize clock
	hri_mclk_set_APBDMASK_ADC0_bit(MCLK);
	hri_mclk_set_APBDMASK_ADC1_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, ADC0_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_gclk_write_PCHCTRL_reg(GCLK, ADC1_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));

	/// Check if software reset of ADC is in progress
	if (!hri_adc_is_syncing(ADC0, ADC_SYNCBUSY_SWRST)) {
		/// If ADC is already enabled
		if (hri_adc_get_CTRLA_reg(ADC0, ADC_CTRLA_ENABLE)) {
			/// * Disable ADC
			hri_adc_clear_CTRLA_ENABLE_bit(ADC0);
			/// * Wait until the ADC is disabled
			hri_adc_wait_for_sync(ADC0, ADC_SYNCBUSY_ENABLE);
		}
		/// Perform software reset of ADC
		hri_adc_write_CTRLA_reg(ADC0, ADC_CTRLA_SWRST);
	}
	if (!hri_adc_is_syncing(ADC1, ADC_SYNCBUSY_SWRST)) {
		/// If ADC is already enabled
		if (hri_adc_get_CTRLA_reg(ADC1, ADC_CTRLA_ENABLE)) {
			/// * Disable ADC
			hri_adc_clear_CTRLA_ENABLE_bit(ADC1);
			/// * Wait until the ADC is disabled
			hri_adc_wait_for_sync(ADC1, ADC_SYNCBUSY_ENABLE);
		}
		/// Perform software reset of ADC
		hri_adc_write_CTRLA_reg(ADC1, ADC_CTRLA_SWRST);
	}
	/// Wait until the software reset is finished
	hri_adc_wait_for_sync(ADC0, ADC_SYNCBUSY_SWRST);
	hri_adc_wait_for_sync(ADC1, ADC_SYNCBUSY_SWRST);

	uint32_t register_value = 0;

	/// Write CTRLA register
	register_value = 1	<< ADC_CTRLA_R2R_Pos                         /// * Enable Rail-to-Rail operation for greater common mode voltage range
	| ADC_CTRLA_PRESCALER_DIV8_Val << ADC_CTRLA_PRESCALER_Pos;        /// * Divide hardware clock by 2 (lowest possible for maximum speed)
	
	hri_adc_write_CTRLA_reg(ADC0, register_value);
	hri_adc_write_CTRLA_reg(ADC1, register_value);

	/// Write CTRLB register. This
	/// Disable window monitor mode
	///	Set result resolution to 12 bit
	///	Disable digital correction logic
	///	Set ADC to single conversion mode
	///	The ADC conversion result is right-adjusted in the RESULT register
	hri_adc_write_CTRLB_reg(ADC0, 0);
	hri_adc_write_CTRLB_reg(ADC1, 0);
	
	/// Write EVCTRL register to disable all events
	hri_adc_write_EVCTRL_reg(ADC0, 0);
	hri_adc_write_EVCTRL_reg(ADC1, 0);
	
	/// Write DBGCTRL register so the ADC is halted when the CPU is halted by an external debugger.
	hri_adc_write_DBGCTRL_reg(ADC0, 0);
	hri_adc_write_DBGCTRL_reg(ADC1, 0);
	
	/// Write INPUTCTRL register
	register_value = 0 << ADC_INPUTCTRL_DSEQSTOP_Pos    /// Disable automatic stopping of DMA sequencing
	| 0x18 << ADC_INPUTCTRL_MUXNEG_Pos                  /// Set negative reference to internal ground
	| 0 << ADC_INPUTCTRL_DIFFMODE_Pos                   /// Disable differential mode
	| 0	<< ADC_INPUTCTRL_MUXPOS_Pos;                    /// Set input channel to pin 0 (Will be updated as soon, as a read out happens)
	hri_adc_write_INPUTCTRL_reg(ADC0, register_value);
	hri_adc_write_INPUTCTRL_reg(ADC1, register_value);
	
	/// Write REFCTRL register
	register_value =	1 << ADC_REFCTRL_REFCOMP_Pos    /// Reference buffer offset compensation is enabled.
	| M_OCM_ADC_REFERENCE << ADC_REFCTRL_REFSEL_Pos;    /// M_OCM_ADC_REFERENCE as reference
	hri_adc_write_REFCTRL_reg(ADC0, register_value);
	hri_adc_write_REFCTRL_reg(ADC1, register_value);
	
	/// Write AVGCTRL register
	register_value = 2 << ADC_AVGCTRL_ADJRES_Pos		/// Set division coefficient to 4 (2^2)
	| 2	<< ADC_AVGCTRL_SAMPLENUM_Pos;					/// Divide accumulated samples by Get 2 sample
	hri_adc_write_AVGCTRL_reg(ADC0, register_value);
	hri_adc_write_AVGCTRL_reg(ADC1, register_value);
	
	/// Write SMPCTRL register
	register_value = 1	<< ADC_SAMPCTRL_OFFCOMP_Pos      /// Enable comparator offset compensation (fixed 4 clock cycles sampling time, minimum due to electronic characteristics: 2.4 cycles)
	| 0	<< ADC_SAMPCTRL_SAMPLEN_Pos;                     /// Must be disabled, by being set to 0, with offset compensation enabled
	hri_adc_write_SAMPCTRL_reg(ADC0, register_value);
	hri_adc_write_SAMPCTRL_reg(ADC1, register_value);
	
	/// Write calibration values to CALIB register
	hri_adc_write_CALIB_reg(ADC0,
	ADC_CALIB_BIASREFBUF(
	(*(uint32_t *) ADC0_FUSES_BIASREFBUF_ADDR >> ADC0_FUSES_BIASREFBUF_Pos))
	| ADC_CALIB_BIASR2R((*(uint32_t *) ADC0_FUSES_BIASR2R_ADDR >> ADC0_FUSES_BIASR2R_Pos))
	| ADC_CALIB_BIASCOMP((*(uint32_t *) ADC0_FUSES_BIASCOMP_ADDR >> ADC0_FUSES_BIASCOMP_Pos)));
	hri_adc_write_CALIB_reg(ADC1,
	ADC_CALIB_BIASREFBUF(
	(*(uint32_t *) ADC1_FUSES_BIASREFBUF_ADDR >> ADC1_FUSES_BIASREFBUF_Pos))
	| ADC_CALIB_BIASR2R((*(uint32_t *) ADC1_FUSES_BIASR2R_ADDR >> ADC1_FUSES_BIASR2R_Pos))
	| ADC_CALIB_BIASCOMP((*(uint32_t *) ADC1_FUSES_BIASCOMP_ADDR >> ADC1_FUSES_BIASCOMP_Pos)));

	return 0;
}

uint8_t initialize_motor_controller_dac() {
	/// Initialize DAC
	/// Initialize clock
	hri_mclk_set_APBDMASK_DAC_bit(MCLK);;
	hri_gclk_write_PCHCTRL_reg(GCLK, DAC_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK1_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));

	/// Check if software reset of DAC is in progress
	if (!hri_dac_is_syncing(DAC, DAC_SYNCBUSY_SWRST)) {
		/// If DAC is already enabled
		if (hri_dac_get_CTRLA_reg(DAC, DAC_CTRLA_ENABLE)) {
			/// * Disable DAC
			hri_dac_clear_CTRLA_ENABLE_bit(DAC);
			/// * Wait until the DAC is disabled
			hri_dac_wait_for_sync(DAC, DAC_SYNCBUSY_ENABLE);
		}
		/// Perform software reset of DAC
		hri_dac_write_CTRLA_reg(DAC, DAC_CTRLA_SWRST);
	}
	/// Wait until the software reset is finished
	hri_dac_wait_for_sync(DAC, DAC_SYNCBUSY_SWRST);

	/// Write CTRLB register
	hri_dac_write_CTRLB_reg(DAC,
	0x3 << DAC_CTRLB_REFSEL_Pos);

	/// Write EVCTRL register
	hri_dac_write_EVCTRL_reg(DAC, 0);

	/// Write INTEN register
	hri_dac_write_INTEN_reg(DAC, 0);

	/// Write DACCTRL register
	hri_dac_write_DACCTRL_reg(DAC, 0, 0
	| 1 << DAC_DACCTRL_REFRESH_Pos
	| 2 << DAC_DACCTRL_CCTRL_Pos
	| 1 << DAC_DACCTRL_ENABLE_Pos
	| 0 << DAC_DACCTRL_LEFTADJ_Pos);
	
	// Activate PIN_PA02 for DAC output
	gpio_set_pin_direction(PIN_PA02, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(PIN_PA02, PINMUX_PA02B_DAC_VOUT0);
	
	return 0;
}

uint8_t initialize_motor_controllers(dc_motor_controller_t *p_this_motor_controller){
	{
		/// Initialize current controller
		sam5_control_settings_t * const c = &p_this_motor_controller->control.current_controller.settings_com;
		c->k_p = 4;
		c->k_i = 6333;
		c->k_d = 0;
		c->k_t = c->k_d == 0 ? c->k_i : sqrtf(c->k_i * c->k_d);
		c->n = c->k_d == 0 ? 0 : 1;
		c->hyst = 0;
	}
	{
		/// Initialize cascaded controller
		// Top Speed ~3000 rad/s -> Achieved with ~85 mA for no load but with fingers -> Tune P for 70% of this error
		// Steady state shall be reached after 0.1 s -> K_i = 10
		sam5_control_settings_t * const c = &p_this_motor_controller->control.velocity_controller.settings_com;
		c->k_p = 2 * (0.7f * 0.085f) / 3000;
		c->k_i = 50;
		c->k_d = 10;
		c->k_t = c->k_d == 0 ? c->k_i : sqrtf(c->k_i * c->k_d);
		c->n = c->k_d == 0 ? 0 : 1;
		c->hyst = 3;
	}
	{
		sam5_control_settings_t * const c = &p_this_motor_controller->control.position_controller.settings_com;
		c->k_p = 100;
		c->k_i = 0;
		c->k_d = 0;
		c->k_t = c->k_d == 0 ? c->k_i : sqrtf(c->k_i * c->k_d);
		c->n = c->k_d == 0 ? 0 : 25;
		c->hyst = 10;
		return 0;
	}
}