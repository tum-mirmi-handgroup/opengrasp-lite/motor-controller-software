#include "sam5_dc_motor_controller_impl.h"


void initial_calib_state_waiting(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_WAITING;
			controller->calib_res.flags.flip_outputs = 0;
			controller->calib_res.flags.started_calib_at_pos = 0;
			controller->calib_res.flags.pos_end_finished = 0;
			controller->calib_res.flags.neg_end_finished = 0;
			controller->calib_res.flags.endstop_at_pos = 0;
			controller->calib_res.flags.searching_endstop = 0;
			controller->calib_res.flags.endstop_trig_calibrated = 0;
			controller->calib_res.flags.endstop_untrig_calibrated = 0;
			controller->internals.calibration.movement_expected = 0;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			if(supply_voltage_buf >= 0.0f && (controller->inputs.flags.control_mode == MOTOR_CM_CALIB_PREP_POS || controller->inputs.flags.control_mode == MOTOR_CM_CALIB_PREP_NEG)){
				controller->calib_res.flags.started_calib_at_pos = controller->inputs.flags.control_mode == MOTOR_CM_CALIB_PREP_POS;
				controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
				simpleStateMachineTran_(fsm, initial_calib_state_polarity_neg, user_struct);
			}
			break;
		}
		case EXIT_SIG: {
			controller->calib_res.flags.searching_endstop = 1;
			controller->outputs.voltage = 0;
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_rip_free(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_RIP_FREE;
			controller->internals.calibration.max_current_during_rip_free = 0;
			controller->internals.calibration.movement_expected = 1;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			const uint32_t loops_passed = controller->internals.calibration.loop_iteration_count - controller->internals.calibration.start_count;
			uint8_t rip_free_velocity_reached = 0;
			if(controller->calib_res.flags.started_calib_at_pos){
				controller->outputs.voltage = -M_RIP_FREE_VOLTAGE;
				rip_free_velocity_reached = controller->state.velocity < -500;
				if(controller->internals.calibration.max_current_during_rip_free < -controller->state.current){
					controller->internals.calibration.max_current_during_rip_free = -controller->state.current;
				}
			}
			else{
				controller->outputs.voltage = M_RIP_FREE_VOLTAGE;
				rip_free_velocity_reached = controller->state.velocity > 500;
				if(controller->internals.calibration.max_current_during_rip_free < controller->state.current){
					controller->internals.calibration.max_current_during_rip_free = controller->state.current;
				}
			}
			if(loops_passed == 200 || rip_free_velocity_reached){
				if(controller->internals.calibration.max_current_during_rip_free < 0.05f){
					controller->calib_res.error_code = CALIB_ERROR_NO_CURRENT_MEASURED;
					simpleStateMachineTran_(&controller->internals.calibration.state_machine, initial_calib_state_error, controller);
				}
				else{
					simpleStateMachineTran_(fsm, initial_calib_state_polarity_neg, user_struct);
				}
			}
			break;
		}
		case EXIT_SIG: {
			controller->outputs.voltage = 0;
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_polarity_neg(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_POLARITY_NEG;
			controller->internals.calibration.movement_expected = 1;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			const uint32_t loops_passed = controller->internals.calibration.loop_iteration_count - controller->internals.calibration.start_count;
			controller->outputs.voltage = -3;
			if(loops_passed == 200){
				controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
				controller->internals.calibration.velocity_with_neg_voltage = controller->state.velocity;
				simpleStateMachineTran_(fsm, initial_calib_state_polarity_pos, user_struct);
			}
			break;
		}
		case EXIT_SIG: {
			controller->outputs.voltage = 0;
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_polarity_pos(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_POLARITY_POS;
			controller->internals.calibration.movement_expected = 1;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			const uint32_t loops_passed = controller->internals.calibration.loop_iteration_count - controller->internals.calibration.start_count;
			controller->outputs.voltage = 3;
			if(loops_passed == 200){
				controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
				controller->calib_res.flags.flip_outputs = controller->internals.calibration.velocity_with_neg_voltage > controller->state.velocity;
				if(-150 < controller->internals.calibration.velocity_with_neg_voltage && controller->internals.calibration.velocity_with_neg_voltage < 150 && -150 < controller->state.velocity && controller->state.velocity < 150){
					controller->calib_res.error_code = CALIB_ERROR_DETECTED_SPEED_TOO_SMALL;
					simpleStateMachineTran_(&controller->internals.calibration.state_machine, initial_calib_state_error, controller);
				}
				if(controller->inputs.flags.control_mode == MOTOR_CM_CALIB_PREP_POS){
					simpleStateMachineTran_(fsm, initial_calib_state_abs_end_pos, user_struct);
				}
				else{
					ASSERT(controller->inputs.flags.control_mode == MOTOR_CM_CALIB_PREP_NEG);
					simpleStateMachineTran_(fsm, initial_calib_state_abs_end_neg, user_struct);
				}
			}
			break;
		}
		case EXIT_SIG: {
			controller->outputs.voltage = 0;
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_abs_end_neg(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_ABS_END_NEG;
			controller->internals.calibration.movement_expected = 1;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			const uint32_t loops_passed = controller->internals.calibration.loop_iteration_count - controller->internals.calibration.start_count;
			controller->outputs.voltage = -3;
			if(loops_passed > 400 && controller->state.current < -M_CALIBRATION_CURRENT_THRESHOLD){
				controller->limits.angle_min = controller->state.angle;
				if(controller->state.flags_b.endstop_active){
					controller->calib_res.flags.endstop_at_pos = false;
				}
				
				controller->calib_res.flags.neg_end_finished = true;
				if(controller->calib_res.flags.pos_end_finished){
					simpleStateMachineTran_(fsm, initial_calib_state_contact_neg_to_pos_pass, user_struct);
				}
				else{
					simpleStateMachineTran_(fsm, initial_calib_state_waiting2, user_struct);
				}
			}
			break;
		}
		case EXIT_SIG: {
			controller->outputs.voltage = 0;
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_abs_end_pos(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_ABS_END_POS;
			controller->internals.calibration.movement_expected = 1;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			const uint32_t loops_passed = controller->internals.calibration.loop_iteration_count - controller->internals.calibration.start_count;
			controller->outputs.voltage = 3;
			if(loops_passed > 400 && M_CALIBRATION_CURRENT_THRESHOLD < controller->state.current){
				controller->limits.angle_max = controller->state.angle;
				if(controller->state.flags_b.endstop_active){
					controller->calib_res.flags.endstop_at_pos = true;
				}
				
				controller->calib_res.flags.pos_end_finished = true;
				if(controller->calib_res.flags.neg_end_finished){
					simpleStateMachineTran_(fsm, initial_calib_state_contact_pos_to_neg_pass, user_struct);
				}
				else{
					simpleStateMachineTran_(fsm, initial_calib_state_waiting2, user_struct);
				}
			}
			break;
		}
		case EXIT_SIG: {
			controller->outputs.voltage = 0;
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_waiting2(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_WAITING2;
			controller->calib_res.flags.searching_endstop = 0;
			controller->internals.calibration.movement_expected = 0;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			if(controller->inputs.flags.control_mode == MOTOR_CM_CALIBRATION){
				if(controller->calib_res.flags.pos_end_finished == 1){
					ASSERT(controller->calib_res.flags.neg_end_finished == 0);
					simpleStateMachineTran_(fsm, initial_calib_state_abs_end_neg, user_struct);
				}
				else {
					ASSERT(controller->calib_res.flags.neg_end_finished == 1);
					simpleStateMachineTran_(fsm, initial_calib_state_abs_end_pos, user_struct);
				}
			}
			break;
		}
		case EXIT_SIG: {
			controller->outputs.voltage = 0;
			controller->calib_res.flags.searching_endstop = 1;
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_go_back_to_start(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_GO_BACK_TO_START;
			controller->internals.calibration.movement_expected = 1;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP:{
			if(controller->calib_res.flags.started_calib_at_pos){
				controller->outputs.voltage = 3;
				if(controller->state.flags_a.max_angle_reached){
					simpleStateMachineTran_(fsm, initial_calib_state_finished, user_struct);
				}
			}
			else{
				controller->outputs.voltage = -3;
				if(controller->state.flags_a.min_angle_reached){
					simpleStateMachineTran_(fsm, initial_calib_state_finished, user_struct);
				}
			}
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_error(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_ERROR;
			controller->internals.calibration.movement_expected = 0;
			controller->outputs.voltage = 0;
			controller->outputs.enable = false;
			controller->calib_res.flags.searching_endstop = 0;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP:{
			controller->outputs.voltage = 0;
			controller->outputs.enable = false;
		}
		default:
		break;
	}
}

void initial_calib_state_finished(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;
			controller->state.flags_b.calib_stage = CALIB_STAGE_FINISHED;
			controller->internals.calibration.movement_expected = 0;
			controller->outputs.voltage = 0;
			controller->outputs.enable = false;
			controller->calib_res.flags.searching_endstop = 0;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP:{
			const uint32_t loops_passed = controller->internals.calibration.loop_iteration_count - controller->internals.calibration.start_count;
			if(loops_passed > 500){
				simpleStateMachineTran_(&controller->internals.state_machine, dc_motor_controller_state_stop, user_struct);
			}
			break;
		}
		default:
		break;
	}
}


// CALIBRATION state finds the endstop for the finger, sets the offset and then transitions to the IDLE state
void dc_motor_controller_state_calibration(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct) {
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch (event->signal_) {
		case ENTRY_SIG: {
			controller->state.flags_b.state_machine = MOTOR_FSM_CALIBRATION;
			controller->internals.calibration.loop_iteration_count = 0;
			controller->internals.calibration.start_count = controller->internals.calibration.loop_iteration_count;

			// If we are already at the end, the interrupt won't be triggered, so we read the state once
			if (!gpio_get_pin_level(controller->hardware.endstop_pin)) {
				// PIN LOW -> Endstop triggered
				controller->state.flags_b.endstop_active = 1;
			}
			else {
				// PIN LOW -> Endstop untriggered
				controller->state.flags_b.endstop_active = 0;
			}
			
			// Activate the output
			controller->outputs.enable = true;
			controller->outputs.voltage = 0;
			
			simpleStateMachineInit_(&controller->internals.calibration.state_machine, initial_calib_state_waiting, (void*) controller);
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			if(controller->internals.loop_iteration_count % M_CALIBRATION_PRSCLR == 0){
				if(controller->internals.calibration.movement_expected && -200 < controller->state.velocity && controller->state.velocity < 200){
					uint32_t counts_passed = controller->internals.calibration.loop_iteration_count - controller->internals.calibration.last_movement_count;
					if(counts_passed > 1000){
						controller->calib_res.error_code = CALIB_ERROR_NO_MOVEMENT_MEASURED;
						simpleStateMachineTran_(&controller->internals.calibration.state_machine, initial_calib_state_error, controller);
					}
				}
				else{
					controller->internals.calibration.last_movement_count = controller->internals.calibration.loop_iteration_count;
				}
				simpleStateMachineDispatch_(&controller->internals.calibration.state_machine, event, (void*) controller);
				++controller->internals.calibration.loop_iteration_count;
			}
			break;
		}
		default:
		return;
	}
}