/**
* \file sam5_dc_motor_controller.c
*
* sam5_dc_motor_controller for controlling dc motors with microcontrollers of the samd5/e5 family
* Copyright (C) 2022  Michael Ratzel

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <utils.h>
#include "sam5_dc_motor_controller_impl.h"
#include "sam5_dc_motor_controller_macros.h"
#include <arm_math.h>

#if DO_MOTOR_TEST
#include "motor_test.h"
#endif


uint8_t disable_motor_controller(dc_motor_controller_t *controller) {
	/// Disable TC interrupt for velocity measurement
	NVIC_DisableIRQ(TC0_IRQn + controller->hardware.encoder_tcn);
	NVIC_ClearPendingIRQ(TC0_IRQn + controller->hardware.encoder_tcn);

	/// Disable EIC interrupts on input pins
	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.endstop_eic_number);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.endstop_eic_number);

	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.diag_eic_number);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.diag_eic_number);

	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.encoder_a.eicn);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.encoder_a.eicn);

	NVIC_DisableIRQ(EIC_0_IRQn + controller->hardware.encoder_b.eicn);
	NVIC_ClearPendingIRQ(EIC_0_IRQn + controller->hardware.encoder_b.eicn);

	/// Stop PWM timer/counter
	hri_tcc_clear_CTRLA_ENABLE_bit(controller->hardware.p_pwm_tcc);

	/// Stop velocity measurement timer
	hri_tc_clear_CTRLA_ENABLE_bit(controller->hardware.p_encoder_tc);

	/// Set enable pin to false
	controller->outputs.enable = false;
	controller->outputs.voltage = 0;
	apply_outputs(controller);

	return 0;
}

int16_t receive_measurements(dc_motor_controller_t *controller) {
	// Start ADC current readout (this will take a while so we can do other stuff in between)
	hri_adc_write_INPUTCTRL_MUXPOS_bf(controller->hardware.p_ocm_adc, controller->hardware.ocm_adc_ainn);
	hri_adc_set_SWTRIG_START_bit(controller->hardware.p_ocm_adc);
	
	// Start readout of current encoder count
	hri_tc_set_CTRLB_CMD_bf(controller->hardware.p_encoder_tc, TC_CTRLBCLR_CMD_READSYNC_Val);
	
	// Use Encoder Readings
	controller->state.angle = controller->internals.sensors.encoder_steps * controller->hardware.position_conv_factor;
	
	// Copy here so we can make sure it will not change during the calculation
	const uint16_t encoder_velocity_ticks = controller->internals.sensors.encoder_velocity_ticks;
	const uint16_t current_encoder_velocity_ticks =  hri_tccount16_read_COUNT_reg(controller->hardware.p_encoder_tc);
	
	if (encoder_velocity_ticks == 0 || controller->internals.sensors.rotation_direction == 0){
		controller->state.velocity = 0;
	}
	else if(current_encoder_velocity_ticks > encoder_velocity_ticks){
		// If encoder speed tc has already counted higher than last measurement -> use new value as we will know the next measurement will be slower than the last and this is closer to the truth
		controller->state.velocity = controller->hardware.velocity_conv_factor / ((int32_t)controller->internals.sensors.rotation_direction * current_encoder_velocity_ticks);
	}
	else {
		controller->state.velocity = controller->hardware.velocity_conv_factor / ((int32_t)controller->internals.sensors.rotation_direction * encoder_velocity_ticks);
	}
	output_debug_data_via_dac(g_motor_controller_1.state.velocity, -2500.f, 2500.f);
	
	// Use angle factor for whole range to determine if movement range was reached
	const float32_t ang_factor = (controller->state.angle - controller->limits.angle_min) / (controller->limits.angle_max- controller->limits.angle_min);
	if(controller->inputs.flags.use_endstop_for_end){
		controller->state.flags_a.min_angle_reached = ang_factor < 0.05f || (!controller->calib_res.flags.endstop_at_pos && controller->state.flags_b.endstop_active);
		controller->state.flags_a.max_angle_reached = ang_factor > 0.95f || (controller->calib_res.flags.endstop_at_pos && controller->state.flags_b.endstop_active);
	}
	else{
		controller->state.flags_a.min_angle_reached = ang_factor < 0.05f;
		controller->state.flags_a.max_angle_reached = ang_factor > 0.95f;
	}
	
	// Check if the target angle was reached
	const float32_t angle_diff = controller->inputs.angle - controller->state.angle;
	controller->state.flags_a.target_angle_reached = (angle_diff >= 0 ? angle_diff : -angle_diff) < (5 + controller->control.position_controller.settings.hyst);
	
	// Get contact detection result which is calculated asynchronously
	controller->state.flags_a.contact_detected = (controller->internals.contact_detection.contact_detected & 0b01) != 0;
	
	// Filter applied voltage to get the sign of the current
	controller->internals.sensors.ema_filtered_voltage = controller->outputs.voltage * ((float32_t) VOLTAGE_FILTER_EMA_ALPHA) + controller->internals.sensors.ema_filtered_voltage * (1 - ((float32_t) VOLTAGE_FILTER_EMA_ALPHA));
	
	// Filter velocity and acceleration
	controller->internals.sensors.ema_filtered_velocity = controller->state.velocity * ((float32_t) VELOCITY_LOW_PASS_EMA) + controller->internals.sensors.ema_filtered_velocity * (1 - ((float32_t) VELOCITY_LOW_PASS_EMA));
	
	const float32_t vel_diff = (controller->internals.sensors.ema_filtered_velocity - controller->internals.sensors.last_filtered_velocity) * M_CONTROLLER_FREQ;
	controller->internals.sensors.ema_filtered_acceleration = vel_diff * ((float32_t) ACCELERATION_LOW_PASS_EMA) + controller->internals.sensors.ema_filtered_acceleration * (1 - ((float32_t) ACCELERATION_LOW_PASS_EMA));
	controller->internals.sensors.last_filtered_velocity = controller->internals.sensors.ema_filtered_velocity;
	
	// Wait for ADC to finish
	while (controller->hardware.p_ocm_adc->STATUS.bit.ADCBUSY){}

	// Calculate current
	const int16_t ocm_raw = hri_adc_read_RESS_reg(controller->hardware.p_ocm_adc) + controller->calib_res.ocm_raw_offset;
	controller->state.current = (controller->internals.sensors.ema_filtered_voltage >= 0) ? controller->hardware.ocm_conv_factor * ocm_raw : -controller->hardware.ocm_conv_factor * ocm_raw;
	controller->internals.sensors.ema_filtered_current = controller->state.current * ((float32_t) CURRENT_LOW_PASS_EMA) + controller->internals.sensors.ema_filtered_current * (1 - ((float32_t) CURRENT_LOW_PASS_EMA));
	
	const float32_t r_coil = MOTOR_R_COIL_AMBIENT * (1 + controller->state.temperature * MOTOR_CONDUCTOR_TEMP_COEF);
	const float32_t p_heat = controller->state.current * controller->state.current * r_coil;
	controller->state.temperature += (1.f / (M_CONTROLLER_FREQ * MOTOR_THERMAL_MASS)) * (p_heat - controller->state.temperature * MOTOR_THERMAL_REDUCTION);
	
	return ocm_raw;
}

void apply_outputs(dc_motor_controller_t *controller) {
	if (controller->outputs.enable) {
		gpio_set_pin_level(controller->hardware.en_pin, true);
	}
	else {
		gpio_set_pin_level(controller->hardware.en_pin, false);
	}
	bool forward = true;
	float32_t voltage = controller->outputs.voltage;
	if (voltage < 0) {
		forward = false;
		voltage *= -1;
	}
	if (voltage > supply_voltage_buf) {
		voltage = supply_voltage_buf;
	}
	if (voltage > controller->limits.voltage_max) {
		voltage = controller->limits.voltage_max;
	}
	controller->state.voltage = forward ? voltage : -voltage;
	
	const uint16_t ticks = (supply_voltage_buf > 0 && voltage > 0) ? M_PWM_MIN_TICKS + ((voltage / supply_voltage_buf) * (M_PWM_MAX_TICKS - M_PWM_MIN_TICKS)) : 0;
	if ((forward && !controller->calib_res.flags.flip_outputs) || (!forward && controller->calib_res.flags.flip_outputs)) {
		CRITICAL_SECTION_ENTER();
		hri_tcc_wait_for_sync(controller->hardware.p_pwm_tcc, TCC_SYNCBUSY_CC(controller->hardware.pwm_forward.ccn) | TCC_SYNCBUSY_CC(controller->hardware.pwm_reverse.ccn));
		controller->hardware.p_pwm_tcc->CCBUF[controller->hardware.pwm_forward.ccn].reg = ticks;
		controller->hardware.p_pwm_tcc->CCBUF[controller->hardware.pwm_reverse.ccn].reg = 0;
		CRITICAL_SECTION_LEAVE();
	}
	else {
		CRITICAL_SECTION_ENTER();
		hri_tcc_wait_for_sync(controller->hardware.p_pwm_tcc, TCC_SYNCBUSY_CC(controller->hardware.pwm_forward.ccn) | TCC_SYNCBUSY_CC(controller->hardware.pwm_reverse.ccn));
		controller->hardware.p_pwm_tcc->CCBUF[controller->hardware.pwm_forward.ccn].reg = 0;
		controller->hardware.p_pwm_tcc->CCBUF[controller->hardware.pwm_reverse.ccn].reg = ticks;
		CRITICAL_SECTION_LEAVE();
	}
}

void detect_contact(dc_motor_controller_t *controller){
	dc_motor_controller_contact_detection_t* detection = &controller->internals.contact_detection;
	float32_t voltage;
	float32_t current;
	float32_t acceleration;
	float32_t velocity_abs;
	int8_t sign;
	CRITICAL_SECTION_ENTER();
	sign = controller->internals.sensors.ema_filtered_voltage >= 0 ? 1 : -1;
	voltage = sign >= 0 ? controller->internals.sensors.ema_filtered_voltage : -controller->internals.sensors.ema_filtered_voltage;
	current = sign >= 0 ? controller->internals.sensors.ema_filtered_current : -controller->internals.sensors.ema_filtered_current;
	acceleration = sign >= 0 ? controller->internals.sensors.ema_filtered_acceleration : -controller->internals.sensors.ema_filtered_acceleration;
	velocity_abs = controller->internals.sensors.ema_filtered_velocity >= 0 ? controller->internals.sensors.ema_filtered_velocity : -controller->internals.sensors.ema_filtered_velocity;
	CRITICAL_SECTION_LEAVE();
	if(acceleration > -2500){
		detection->contact_detected = 0;
		return;
	}
	
	const dc_motor_friction_model_t* friction_model = sign >= 0 ? &detection->friction_pos : &detection->friction_neg;
	const float32_t ang_factor = (controller->state.angle - detection->angle_min) * detection->angle_factor;
	
	float32_t friction_factor;
	if(ang_factor <= 0){
		friction_factor = friction_model->friction_factor[0];
	}
	else if(ang_factor >= CONTACT_CALIB_NUM_ANGLE_RANGES - 1){
		friction_factor = friction_model->friction_factor[CONTACT_CALIB_NUM_ANGLE_RANGES - 1];
	}
	else{
		const int8_t ang_pos_lower = (int8_t) ang_factor;
		const int8_t ang_pos_upper = (int8_t) ang_factor + 1;
		const float32_t ang_ratio = ang_factor - ang_pos_lower;
		friction_factor = friction_model->friction_factor[ang_pos_lower] * (1-ang_ratio) + friction_model->friction_factor[ang_pos_upper] * ang_ratio;
	}
	
	float32_t expected_cur = (friction_model->a0 + friction_model->a1 * voltage) * friction_factor;
	if(expected_cur < 0){
		expected_cur = 0;
	}
	if(velocity_abs > 0 && velocity_abs < 750){
		expected_cur *= 1 + ((750 - velocity_abs) / 750);
	}
	if(controller->inputs.flags.use_aggressive_contact_detection){	
		detection->contact_detected = current > (expected_cur * 1.5f + 0.04f);
	}
	else{
		detection->contact_detected = current > (expected_cur * 1.25f + 0.02f);
	}
}

uint8_t sam5_dc_motor_controller_start_loop() {
	NVIC_DisableIRQ(TC0_IRQn + M_LOOP_TCn);
	NVIC_ClearPendingIRQ(TC0_IRQn + M_LOOP_TCn);
	NVIC_EnableIRQ(TC0_IRQn + M_LOOP_TCn);

	hri_tc_set_CTRLA_ENABLE_bit(TCn(M_LOOP_TCn));
	return 0;
}

void output_debug_data_via_dac(float data, float min_value, float max_value) {
	float factor = (data - min_value) / (max_value - min_value);
	uint16_t data_12bit;
	if (factor < 0) {
		data_12bit = 0;
	}
	else if (factor >= 1) {
		data_12bit = 4095;
	}
	else {
		data_12bit = 4095 * factor;
	}
	hri_dac_write_DATA_reg(DAC, 0, data_12bit);
}

void sam5_dc_motor_controller_low_prio_loop(){
	#ifdef M1_PRESENT
	detect_contact(&g_motor_controller_1);
	g_motor_controller_1.calib_res.endstop_angle_trigger = g_motor_controller_1.internals.sensors.endstop_encoder_steps_trig * g_motor_controller_1.hardware.position_conv_factor;
	g_motor_controller_1.calib_res.endstop_angle_untrigger = g_motor_controller_1.internals.sensors.endstop_encoder_steps_untrig * g_motor_controller_1.hardware.position_conv_factor;
	#endif
	#ifdef M2_PRESENT
	detect_contact(&g_motor_controller_2);
	g_motor_controller_2.calib_res.endstop_angle_trigger = g_motor_controller_2.internals.sensors.endstop_encoder_steps_trig * g_motor_controller_2.hardware.position_conv_factor;
	g_motor_controller_2.calib_res.endstop_angle_untrigger = g_motor_controller_2.internals.sensors.endstop_encoder_steps_untrig * g_motor_controller_2.hardware.position_conv_factor;
	#endif
	#ifdef M3_PRESENT
	detect_contact(&g_motor_controller_3);
	g_motor_controller_3.calib_res.endstop_angle_trigger = g_motor_controller_3.internals.sensors.endstop_encoder_steps_trig * g_motor_controller_3.hardware.position_conv_factor;
	g_motor_controller_3.calib_res.endstop_angle_untrigger = g_motor_controller_3.internals.sensors.endstop_encoder_steps_untrig * g_motor_controller_3.hardware.position_conv_factor;
	#endif
}

static uint8_t current_controller = 0;

void TCn_Handler(M_LOOP_TCn)(void) {
	current_controller = (current_controller + 1) % 3;
	switch(current_controller){
		case 0:
		#ifdef M1_PRESENT
		++g_motor_controller_1.internals.loop_iteration_count;
		run_state_machine(&g_motor_controller_1);
		#if DO_MOTOR_TEST
		motor_test_record_measurement(&g_motor_controller_1);
		#endif
		#endif
		current_controller = 1;
		break;
		case 1:
		#ifdef M2_PRESENT
		++g_motor_controller_2.internals.loop_iteration_count;
		#if !DO_MOTOR_TEST
		run_state_machine(&g_motor_controller_2);
		#endif
		#endif
		current_controller = 2;
		break;
		case 2:
		#ifdef M3_PRESENT
		++g_motor_controller_3.internals.loop_iteration_count;
		#if !DO_MOTOR_TEST
		run_state_machine(&g_motor_controller_3);
		#endif
		#endif
		current_controller = 0;
		break;
		default:
		current_controller = 0;
		break;
	}
	hri_tc_clear_INTFLAG_OVF_bit(TCn(M_LOOP_TCn));
}
