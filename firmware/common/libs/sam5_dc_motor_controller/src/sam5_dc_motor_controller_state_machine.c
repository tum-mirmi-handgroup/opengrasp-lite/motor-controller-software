/**
* \file sam5_dc_motor_controller_state_machine.c
*
* sam5_dc_motor_controller for controlling DC motors with microcontrollers of the samd5/e5 family
* Copyright (C) 2022  Marie Fieweger

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <utils.h>
#include <stdio.h>
#include <math.h>
#include "sam5_dc_motor_controller_impl.h"
#include "sam5_dc_motor_controller_macros.h"
#include "control_flags.h"

static uint8_t need_to_stop_movement(dc_motor_controller_t *controller);

// INIT state initializes motor controllers and peripherals, then transitions to the CALIBRATION state
void dc_motor_controller_state_init(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct) {
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch (event->signal_) {
		case ENTRY_SIG: {
			controller->state.flags_b.state_machine = MOTOR_FSM_INIT;
			initialize_motor_controller_peripherals(controller);
			initialize_motor_controllers(controller);
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_HARDWARE_ON: {
			simpleStateMachineTran_(fsm, dc_motor_controller_state_stop, user_struct);
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP:
		case EXIT_SIG:
		default: {
			return;
		}
	}
}

// the IDLE state will prepare the motor to run or switch control mode
void dc_motor_controller_state_stop(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct)
{
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			controller->state.flags_b.state_machine = MOTOR_FSM_STOP;
			controller->outputs.enable = false;
			controller->outputs.voltage = 0;
			break;
		}
		case EXIT_SIG:
		{
			controller->outputs.enable = true;
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP:
		{
			if (controller->inputs.flags.control_mode != MOTOR_CM_STOPPED)
			{
				switch(controller->inputs.flags.control_mode)
				{
					case MOTOR_CM_VOLTAGE:
					{
						simpleStateMachineTran_(fsm, dc_motor_controller_state_open_loop_control, user_struct);
						break;
					}
					case MOTOR_CM_POSITION:
					case MOTOR_CM_VELOCITY:
					case MOTOR_CM_CURRENT:
					{
						simpleStateMachineTran_(fsm, dc_motor_controller_state_closed_loop_control, user_struct);
						break;
					}
					case MOTOR_CM_CALIB_PREP_POS:
					case MOTOR_CM_CALIB_PREP_NEG:
					{
						simpleStateMachineTran_(fsm, dc_motor_controller_state_calibration, user_struct);
					}
					default:
					{
						break;
					}
				}
			}
			break;
		}
		default:
		{
			break;
		}
	}
	return;
}

void dc_motor_controller_state_open_loop_control(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct)
{
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			controller->state.flags_b.state_machine = MOTOR_FSM_OPEN_LOOP_CONTROL;
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP:
		{
			if (controller->inputs.flags.control_mode != MOTOR_CM_VOLTAGE)
			{
				simpleStateMachineTran_(fsm, dc_motor_controller_state_stop, user_struct);
				break;
			}
			controller->outputs.voltage = controller->inputs.voltage;
			break;
		}
		default:
		{
			break;
		}
	}
	return;
}

void dc_motor_controller_state_closed_loop_control(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct)
{
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			controller->state.flags_b.state_machine = MOTOR_FSM_CLOSE_LOOP_CONTROL;
			controller->internals.control_mode_from_last_input = MOTOR_CM_STOPPED;
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP:
		{
			if (controller->inputs.flags.control_mode != MOTOR_CM_POSITION && controller->inputs.flags.control_mode != MOTOR_CM_VELOCITY && controller->inputs.flags.control_mode != MOTOR_CM_CURRENT)
			{
				simpleStateMachineTran_(fsm, dc_motor_controller_state_stop, user_struct);
				break;
			}
			else if (controller->inputs.flags.control_mode != controller->internals.control_mode_from_last_input){
				controller->internals.control_mode_from_last_input = controller->inputs.flags.control_mode;
				sam5_control_reset_controller(&controller->control.current_controller);
				sam5_control_reset_controller(&controller->control.velocity_controller);
				sam5_control_reset_controller(&controller->control.position_controller);
			}
			if(controller->internals.loop_iteration_count % M_POS_VEL_CTRL_PRSCLR == 0){
				if((controller->inputs.flags.control_mode & 0b110) == 0b110){
					// Run position and velocity
					sam5_control_pid(&controller->control.position_controller, ((float32_t) M_POS_VEL_CTRL_PRSCLR) / M_CONTROLLER_FREQ);
					sam5_control_pid(&controller->control.velocity_controller, ((float32_t) M_POS_VEL_CTRL_PRSCLR) / M_CONTROLLER_FREQ);
				}
				else if((controller->inputs.flags.control_mode & 0b010) == 0b010){
					// Run only velocity
					if(controller->inputs.velocity >= controller->limits.velocity_max){
						controller->control.velocity_setpoint = controller->limits.velocity_max;
					}
					else if(controller->inputs.velocity <= -controller->limits.velocity_max){
						controller->control.velocity_setpoint = -controller->limits.velocity_max;
					}
					else{
						controller->control.velocity_setpoint = controller->inputs.velocity;
					}
					sam5_control_pid(&controller->control.velocity_controller, ((float32_t) M_POS_VEL_CTRL_PRSCLR) / M_CONTROLLER_FREQ);
				}
				else {
					if(controller->inputs.current >= controller->limits.current_max){
						controller->control.current_setpoint = controller->limits.current_max;
					}
					else if(controller->inputs.current <= -controller->limits.current_max){
						controller->control.current_setpoint = -controller->limits.current_max;
					}
					else{
						controller->control.current_setpoint = controller->inputs.current;
					}
				}
			}
			sam5_control_pi(&controller->control.current_controller, ((float32_t) 1) / M_CONTROLLER_FREQ);
			break;
		}
		default:
		{
			break;
		}
	}
	return;
}

uint8_t sam5_dc_motor_controller_init(dc_motor_controller_t *controller)
{
	if(!controller) {
		return 1;
	}
	simpleStateMachineInit_(&controller->internals.state_machine, dc_motor_controller_state_init, controller);
	return 0;
}

void run_state_machine(dc_motor_controller_t *controller)
{
	const uint8_t id = controller->id - 1;
	CRITICAL_SECTION_ENTER();
	memcpy((void*)&controller->inputs, (const void*)&controller->inputs_com, sizeof(dc_motor_controller_inputs_t));
	supply_voltage_buf = supply_voltage;
	if(control_flags.run_motor_controllers){
		const uint8_t need_to_stop = need_to_stop_movement(controller);
		if(need_to_stop != 0){
			// Override even the buffered inputs, so we need to have an updated command to get out of the stop again
			controller->inputs_com.flags.control_mode = MOTOR_CM_STOPPED;
			controller->inputs.flags.control_mode = MOTOR_CM_STOPPED;
			controller->state.flags_a.motor_stopped = 1;
		}
		else{
			controller->state.flags_a.motor_stopped = 0;
		}
	}
	else{
		controller->state.flags_a.motor_stopped = 1;
	}
	CRITICAL_SECTION_LEAVE();
	receive_measurements(controller);
	if(control_flags.run_motor_controllers){
		const simple_state_machine_event_t loop_event =
		{
			.signal_ = DC_MOTOR_CONTROLLER_EVT_RUN_LOOP
		};
		simpleStateMachineDispatch_(&controller->internals.state_machine, &loop_event, controller);
	}
	else{
		controller->outputs.enable = false;
	}
	apply_outputs(controller);
	CRITICAL_SECTION_ENTER();
	combined_state_flags.flags_a[id] = controller->state.flags_a;
	combined_state_flags.flags_b[id] = controller->state.flags_b;
	combined_state.angle[id] = controller->state.angle;
	combined_state.current[id] = controller->state.current;
	CRITICAL_SECTION_LEAVE();
}

static uint8_t need_to_stop_movement(dc_motor_controller_t *controller){
	if(controller->inputs.flags.control_mode == MOTOR_CM_STOPPED){
		return 1;
	}
	if(controller->inputs.flags.stop_at_contact == 1 && controller->state.flags_a.contact_detected){
		controller->state.flags_a.stopped_due_to_contact = 1;
		return 2;
	}
	controller->state.flags_a.stopped_due_to_contact = 0;
	if(controller->inputs.flags.continue_at_end == 0){
		switch(controller->inputs.flags.control_mode){
			case MOTOR_CM_VOLTAGE:{
				if(controller->inputs.voltage >= 0){
					if(controller->state.flags_a.max_angle_reached){
						return 3;
					}
				}
				else{
					if(controller->state.flags_a.min_angle_reached){
						return 4;
					}
				}
				break;
			}
			case MOTOR_CM_CURRENT:{
				if(controller->inputs.current >= 0){
					if(controller->state.flags_a.max_angle_reached){
						return 5;
					}
				}
				else{
					if(controller->state.flags_a.min_angle_reached){
						return 6;
					}
				}
				break;
			}
			case MOTOR_CM_VELOCITY:{
				if(controller->inputs.velocity >= 0){
					if(controller->state.flags_a.max_angle_reached){
						return 7;
					}
				}
				else{
					if(controller->state.flags_a.min_angle_reached){
						return 8;
					}
				}
				break;
			}
			case MOTOR_CM_POSITION:{
				if(controller->inputs.angle > controller->state.angle){
					if(controller->state.flags_a.max_angle_reached){
						return 9;
					}
				}
				else{
					if(controller->state.flags_a.min_angle_reached){
						return 10;
					}
				}
				break;
			}
		}
	}
	return 0;
}