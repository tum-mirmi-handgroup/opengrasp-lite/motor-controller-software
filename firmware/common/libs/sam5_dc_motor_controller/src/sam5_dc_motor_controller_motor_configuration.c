/**
* \file motor_configuration.c
*
* This file contains the configuration structs for all motors.
*/

#include "sam5_dc_motor_controller_impl.h"
#include "hardware_defines.h"

volatile dc_motor_controller_combined_state_t combined_state;
volatile dc_motor_controller_combined_state_flags_t combined_state_flags;
volatile float32_t supply_voltage = 0;
float32_t supply_voltage_buf;

#if M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV1_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 1
#elif M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV2_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 2
#elif M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV4_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 4
#elif M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV8_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 8
#elif M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV16_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 16
#elif M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV64_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 64
#elif M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV256_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 256
#elif M_ENCODER_TC_PRESCALER == TC_CTRLA_PRESCALER_DIV1024_Val
#define M_ENCODER_TC_PRESCALER_FACTOR 1024
#else
#error "Unknown value for M_ENCODER_TC_PRESCALER"
#endif

#ifdef M1_PRESENT
/**
* \brief Global struct for motor controller 1
*
* This struct is only generated if M1_PRESENT is defined.
* It is populated with the values from the header template sam5_dc_motor_controller_hardware_definitions.h.
*/
dc_motor_controller_t g_motor_controller_1 =
{
	.id = 1,
	.hardware =
	{
		.en_pin = M1_EN_PIN,

		.pwm_tccn = M1_PWM_TCCn,
		.pwm_otmx = M1_PWM_OTMX,
		.p_pwm_tcc = TCCn(M1_PWM_TCCn),
		.pwm_forward = {
			.pin = M1_PWM_FORWARD_PIN,
			.funct = M1_PWM_FORWARD_FUNCTION,
			.ccn = M1_PWM_FORWARD_CCn,
		},
		.pwm_reverse = {
			.pin = M1_PWM_REVERSE_PIN,
			.funct = M1_PWM_REVERSE_FUNCTION,
			.ccn = M1_PWM_REVERSE_CCn,
		},

		.endstop_pin = M1_ENDSTOP_PIN,
		.endstop_eic_number = M1_ENDSTOP_EICn,

		.ocm_pin = M1_OCM_PIN,
		.ocm_adcn = M1_OCM_ADCn,
		.ocm_adc_ainn = M1_OCM_ADC_AINn,
		.p_ocm_adc = ADCn(M1_OCM_ADCn),
		.ocm_conv_factor =  (429.18f * M_SUPC_VREF_V) / (4095.f * M1_OCM_R_SENSE),

		.diag_pin = M1_DIAG_PIN,
		.diag_eic_number = M1_DIAG_EICn,

		.encoder_a = {
			.pin = M1_ENCODER_A_PIN,
			.eicn = M1_ENCODER_A_EICn
		},
		.encoder_b = {
			.pin = M1_ENCODER_B_PIN,
			.eicn = M1_ENCODER_B_EICn
		},
		.encoder_evsys_channel = M1_ENCODER_EVSYS_CHANNEL,
		.encoder_tcn = 1,
		.p_encoder_tc = TCn(1),
		// For a 12 step quadrature encoder
		// Per motor rotation: 3 Encoder A lowToHigh and 3 Encoder A hightToLow edges.
		// The TC measures the time between an Encoder A lowToHigh and higthToLow edge, so 1/6 of a rotation
		// Expressed with the ENCODER_COUNTS_PER_REV constant:
		// angle = 2 * PI / 6 = 4 * PI / ENCODER_COUNTS_PER_REV
		// Velocity is angle / time
		// Time is (TC_ticks * prescaler) / clock_freq
		// speed is: (angle * (clock_freq / prescaler)) / TC_ticks
		.velocity_conv_factor = (CONF_CPU_FREQUENCY / M_ENCODER_TC_PRESCALER_FACTOR) * (4.f * PI / ((float32_t) M_ENCODER_COUNTS_PER_REV)),
		// For a 12 step quadrature encoder
		// 12 signal edges per motor revolution (3 Encoder A lowToHigh, 3 Encoder B lowToHigh, and the same for highToLow)
		// Angle = ((2 * PI) / 12) * total_steps
		.position_conv_factor = (2.f * PI / ((float32_t) M_ENCODER_COUNTS_PER_REV)),
	},
	.inputs_com = {
		.flags = {
			.control_mode = MOTOR_CM_STOPPED,
			.stop_at_contact = 0,
			.continue_at_end = 0,
			.use_endstop_for_end = 0,
			.use_endstop_for_refine = 0,
		},
		.angle = 0,
		.velocity = 0,
		.current = 0,
		.voltage = 0,
	},
	.outputs = {
		.enable = 0,
		.voltage = 0,
	},
	.state = {
		.angle = 0,
		.velocity = 0,
		.current = 0,
		.voltage = 0,
		.flags_a = {
			.hardware_state = 0,
			.contact_detected = 0,
			.min_angle_reached = 0,
			.max_angle_reached = 0,
			.target_angle_reached = 0,
			.stopped_due_to_contact = 0,
			.motor_stopped = 0,
		},
		.flags_b = {
			.state_machine = 0,
			.calib_stage = 0,
			.endstop_active = 0,
		},
	},
	.limits = {
		.angle_min = 0,
		.angle_max = 0,
		.voltage_max = M_LIMIT_VOLTAGE,
		.current_max = M_LIMIT_CURRENT,
		.velocity_max = M_LIMIT_VELOCITY,
	},
	.calib_res = {
		.endstop_angle_trigger = 0,
		.endstop_angle_untrigger = 0,
		.ocm_raw_offset = 0,
		.flags = {
			.endstop_at_pos = 0,
			.flip_outputs = 0,
			.started_calib_at_pos = 0,
			.pos_end_finished = 0,
			.neg_end_finished = 0,
			.searching_endstop = 0,
		}
	},
	.control = {
		.last_position_input = 0,
		.velocity_setpoint = 0,
		.current_setpoint = 0,
		.position_controller =
		{
			.p_setpoint = &g_motor_controller_1.inputs.angle,
			.p_process_value = &g_motor_controller_1.state.angle,
			.p_out = &g_motor_controller_1.control.velocity_setpoint,
			.p_out_max_1 = &g_motor_controller_1.inputs.velocity,
			.p_out_max_2 = &g_motor_controller_1.limits.velocity_max,
		},
		.velocity_controller =
		{
			.p_setpoint = &g_motor_controller_1.control.velocity_setpoint,
			.p_process_value = &g_motor_controller_1.state.velocity,
			.p_out = &g_motor_controller_1.control.current_setpoint,
			.p_out_max_1 = &g_motor_controller_1.inputs.current,
			.p_out_max_2 = &g_motor_controller_1.limits.current_max,
		},
		.current_controller =
		{
			.p_setpoint = &g_motor_controller_1.control.current_setpoint,
			.p_process_value = &g_motor_controller_1.state.current,
			.p_out = &g_motor_controller_1.outputs.voltage,
			.p_out_max_1 = &g_motor_controller_1.limits.voltage_max,
			.p_out_max_2 = NULL,
		},
	},
	.internals =
	{
		.loop_iteration_count = 9, // Offset this a to avoid all 3 motors having the computation intense cascaded control loop in the same iteration
		
		.sensors = {
			.encoder_steps = 0,
			.encoder_velocity_ticks = UINT16_MAX,
			.ema_filtered_voltage = 0,
			.ema_filtered_current = 0,
			.ema_filtered_velocity = 0,
			.last_filtered_velocity = 0,
			.ema_filtered_acceleration = 0,
			.rotation_direction = 0,
			.rotation_direction_changed = 1,
		},
		.contact_detection = {
			.contact_detected = 0,
		},
	},
};

#endif // M1_PRESENT

#ifdef M2_PRESENT
/**
* \brief Global struct for motor controller 2
*
* This struct is only generated if M2_PRESENT is defined.
* It is populated with the values from the header template sam5_dc_motor_controller_hardware_definitions.h.
*/
dc_motor_controller_t g_motor_controller_2 =
{
	.id = 2,
	.hardware =
	{
		.en_pin = M2_EN_PIN,

		.pwm_tccn = M2_PWM_TCCn,
		.pwm_otmx = M2_PWM_OTMX,
		.p_pwm_tcc = TCCn(M2_PWM_TCCn),
		.pwm_forward = {
			.pin = M2_PWM_FORWARD_PIN,
			.funct = M2_PWM_FORWARD_FUNCTION,
			.ccn = M2_PWM_FORWARD_CCn,
		},
		.pwm_reverse = {
			.pin = M2_PWM_REVERSE_PIN,
			.funct = M2_PWM_REVERSE_FUNCTION,
			.ccn = M2_PWM_REVERSE_CCn,
		},

		.endstop_pin = M2_ENDSTOP_PIN,
		.endstop_eic_number = M2_ENDSTOP_EICn,

		.ocm_pin = M2_OCM_PIN,
		.ocm_adcn = M2_OCM_ADCn,
		.ocm_adc_ainn = M2_OCM_ADC_AINn,
		.p_ocm_adc = ADCn(M2_OCM_ADCn),
		.ocm_conv_factor =  (429.1845493562231759656652360515f * M_SUPC_VREF_V) /
		(4095.f * M2_OCM_R_SENSE),

		.diag_pin = M2_DIAG_PIN,
		.diag_eic_number = M2_DIAG_EICn,

		.encoder_a = {
			.pin = M2_ENCODER_A_PIN,
			.eicn = M2_ENCODER_A_EICn
		},
		.encoder_b = {
			.pin = M2_ENCODER_B_PIN,
			.eicn = M2_ENCODER_B_EICn
		},
		.encoder_evsys_channel = M2_ENCODER_EVSYS_CHANNEL,
		.encoder_tcn = 2,
		.p_encoder_tc = TCn(2),
		// For a 12 step quadrature encoder
		// Per motor rotation: 3 Encoder A lowToHigh and 3 Encoder A hightToLow edges.
		// The TC measures the time between an Encoder A lowToHigh and higthToLow edge, so 1/6 of a rotation
		// Expressed with the ENCODER_COUNTS_PER_REV constant:
		// angle = 2 * PI / 6 = 4 * PI / ENCODER_COUNTS_PER_REV
		// Velocity is angle / time
		// Time is (TC_ticks * prescaler) / clock_freq
		// speed is: (angle * (clock_freq / prescaler)) / TC_ticks
		.velocity_conv_factor = (CONF_CPU_FREQUENCY / M_ENCODER_TC_PRESCALER_FACTOR) * (4.f * PI / ((float32_t) M_ENCODER_COUNTS_PER_REV)),
		// For a 12 step quadrature encoder
		// 12 signal edges per motor revolution (3 Encoder A lowToHigh, 3 Encoder B lowToHigh, and the same for highToLow)
		// Angle = ((2 * PI) / 12) * total_steps
		.position_conv_factor = (2.f * PI / ((float32_t) M_ENCODER_COUNTS_PER_REV)),
	},
	.inputs_com = {
		.flags = {
			.control_mode = MOTOR_CM_STOPPED,
			.stop_at_contact = 0,
			.continue_at_end = 0,
			.use_endstop_for_end = 0,
			.use_endstop_for_refine = 0,
		},
		.angle = 0,
		.velocity = 0,
		.current = 0,
		.voltage = 0,
	},
	.outputs = {
		.enable = 0,
		.voltage = 0,
	},
	.state = {
		.angle = 0,
		.velocity = 0,
		.current = 0,
		.voltage = 0,
		.flags_a = {
			.hardware_state = 0,
			.contact_detected = 0,
			.min_angle_reached = 0,
			.max_angle_reached = 0,
			.target_angle_reached = 0,
			.stopped_due_to_contact = 0,
			.motor_stopped = 0,
		},
		.flags_b = {
			.state_machine = 0,
			.calib_stage = 0,
			.endstop_active = 0,
		},
	},
	.limits = {
		.angle_min = 0,
		.angle_max = 0,
		.voltage_max = M_LIMIT_VOLTAGE,
		.current_max = M_LIMIT_CURRENT,
		.velocity_max = M_LIMIT_VELOCITY,
	},
	.calib_res = {
		.endstop_angle_trigger = 0,
		.endstop_angle_untrigger = 0,
		.ocm_raw_offset = 0,
		.flags = {
			.endstop_at_pos = 0,
			.flip_outputs = 0,
			.started_calib_at_pos = 0,
			.pos_end_finished = 0,
			.neg_end_finished = 0,
			.searching_endstop = 0,
		}
	},
	.control = {
		.last_position_input = 0,
		.velocity_setpoint = 0,
		.current_setpoint = 0,
		.position_controller =
		{
			.p_setpoint = &g_motor_controller_2.inputs.angle,
			.p_process_value = &g_motor_controller_2.state.angle,
			.p_out = &g_motor_controller_2.control.velocity_setpoint,
			.p_out_max_1 = &g_motor_controller_2.inputs.velocity,
			.p_out_max_2 = &g_motor_controller_2.limits.velocity_max,
		},
		.velocity_controller =
		{
			.p_setpoint = &g_motor_controller_2.control.velocity_setpoint,
			.p_process_value = &g_motor_controller_2.state.velocity,
			.p_out = &g_motor_controller_2.control.current_setpoint,
			.p_out_max_1 = &g_motor_controller_2.inputs.current,
			.p_out_max_2 = &g_motor_controller_2.limits.current_max,
		},
		.current_controller =
		{
			.p_setpoint = &g_motor_controller_2.control.current_setpoint,
			.p_process_value = &g_motor_controller_2.state.current,
			.p_out = &g_motor_controller_2.outputs.voltage,
			.p_out_max_1 = &g_motor_controller_2.limits.voltage_max,
			.p_out_max_2 = NULL,
		},
	},
	.internals =
	{
		.loop_iteration_count = 9, // Offset this a to avoid all 3 motors having the computation intense cascaded control loop in the same iteration
		
		.sensors = {
			.encoder_steps = 0,
			.encoder_velocity_ticks = UINT16_MAX,
			.ema_filtered_voltage = 0,
			.ema_filtered_current = 0,
			.ema_filtered_velocity = 0,
			.last_filtered_velocity = 0,
			.ema_filtered_acceleration = 0,
			.rotation_direction = 0,
			.rotation_direction_changed = 1,
		},
		.contact_detection = {
			.contact_detected = 0,
		},
	},
};
#endif // M2_PRESENT

#ifdef M3_PRESENT
/**
* \brief Global struct for motor controller 3
*
* This struct is only generated if M3_PRESENT is defined.
* It is populated with the values from the header template sam5_dc_motor_controller_hardware_definitions.h.
*/
dc_motor_controller_t g_motor_controller_3 =
{
	.id = 3,
	.hardware =
	{
		.en_pin = M3_EN_PIN,

		.pwm_tccn = M3_PWM_TCCn,
		.pwm_otmx = M3_PWM_OTMX,
		.p_pwm_tcc = TCCn(M3_PWM_TCCn),
		.pwm_forward = {
			.pin = M3_PWM_FORWARD_PIN,
			.funct = M3_PWM_FORWARD_FUNCTION,
			.ccn = M3_PWM_FORWARD_CCn,
		},
		.pwm_reverse = {
			.pin = M3_PWM_REVERSE_PIN,
			.funct = M3_PWM_REVERSE_FUNCTION,
			.ccn = M3_PWM_REVERSE_CCn,
		},

		.endstop_pin = M3_ENDSTOP_PIN,
		.endstop_eic_number = M3_ENDSTOP_EICn,

		.ocm_pin = M3_OCM_PIN,
		.ocm_adcn = M3_OCM_ADCn,
		.ocm_adc_ainn = M3_OCM_ADC_AINn,
		.p_ocm_adc = ADCn(M3_OCM_ADCn),
		.ocm_conv_factor =  (429.1845493562231759656652360515f * M_SUPC_VREF_V) /
		(4095.f * M3_OCM_R_SENSE),

		.diag_pin = M3_DIAG_PIN,
		.diag_eic_number = M3_DIAG_EICn,

		.encoder_a = {
			.pin = M3_ENCODER_A_PIN,
			.eicn = M3_ENCODER_A_EICn
		},
		.encoder_b = {
			.pin = M3_ENCODER_B_PIN,
			.eicn = M3_ENCODER_B_EICn
		},
		.encoder_evsys_channel = M3_ENCODER_EVSYS_CHANNEL,
		.encoder_tcn = 3,
		.p_encoder_tc = TCn(3),
		// For a 12 step quadrature encoder
		// Per motor rotation: 3 Encoder A lowToHigh and 3 Encoder A hightToLow edges.
		// The TC measures the time between an Encoder A lowToHigh and higthToLow edge, so 1/6 of a rotation
		// Expressed with the ENCODER_COUNTS_PER_REV constant:
		// angle = 2 * PI / 6 = 4 * PI / ENCODER_COUNTS_PER_REV
		// Velocity is angle / time
		// Time is (TC_ticks * prescaler) / clock_freq
		// speed is: (angle * (clock_freq / prescaler)) / TC_ticks
		.velocity_conv_factor = (CONF_CPU_FREQUENCY / M_ENCODER_TC_PRESCALER_FACTOR) * (4.f * PI / ((float32_t) M_ENCODER_COUNTS_PER_REV)),
		// For a 12 step quadrature encoder
		// 12 signal edges per motor revolution (3 Encoder A lowToHigh, 3 Encoder B lowToHigh, and the same for highToLow)
		// Angle = ((2 * PI) / 12) * total_steps
		.position_conv_factor = (2.f * PI / ((float32_t) M_ENCODER_COUNTS_PER_REV)),
	},
	.inputs_com = {
		.flags = {
			.control_mode = MOTOR_CM_STOPPED,
			.stop_at_contact = 0,
			.continue_at_end = 0,
			.use_endstop_for_end = 0,
			.use_endstop_for_refine = 0,
		},
		.angle = 0,
		.velocity = 0,
		.current = 0,
		.voltage = 0,
	},
	.outputs = {
		.enable = 0,
		.voltage = 0,
	},
	.state = {
		.angle = 0,
		.velocity = 0,
		.current = 0,
		.voltage = 0,
		.flags_a = {
			.hardware_state = 0,
			.contact_detected = 0,
			.min_angle_reached = 0,
			.max_angle_reached = 0,
			.target_angle_reached = 0,
			.stopped_due_to_contact = 0,
			.motor_stopped = 0,
		},
		.flags_b = {
			.state_machine = 0,
			.calib_stage = 0,
			.endstop_active = 0,
		},
	},
	.limits = {
		.angle_min = 0,
		.angle_max = 0,
		.voltage_max = M_LIMIT_VOLTAGE,
		.current_max = M_LIMIT_CURRENT,
		.velocity_max = M_LIMIT_VELOCITY,
	},
	.calib_res = {
		.endstop_angle_trigger = 0,
		.endstop_angle_untrigger = 0,
		.ocm_raw_offset = 0,
		.flags = {
			.endstop_at_pos = 0,
			.flip_outputs = 0,
			.started_calib_at_pos = 0,
			.pos_end_finished = 0,
			.neg_end_finished = 0,
			.searching_endstop = 0,
		}
	},
	.control = {
		.last_position_input = 0,
		.velocity_setpoint = 0,
		.current_setpoint = 0,
		.position_controller =
		{
			.p_setpoint = &g_motor_controller_3.inputs.angle,
			.p_process_value = &g_motor_controller_3.state.angle,
			.p_out = &g_motor_controller_3.control.velocity_setpoint,
			.p_out_max_1 = &g_motor_controller_3.inputs.velocity,
			.p_out_max_2 = &g_motor_controller_3.limits.velocity_max,
		},
		.velocity_controller =
		{
			.p_setpoint = &g_motor_controller_3.control.velocity_setpoint,
			.p_process_value = &g_motor_controller_3.state.velocity,
			.p_out = &g_motor_controller_3.control.current_setpoint,
			.p_out_max_1 = &g_motor_controller_3.inputs.current,
			.p_out_max_2 = &g_motor_controller_3.limits.current_max,
		},
		.current_controller =
		{
			.p_setpoint = &g_motor_controller_3.control.current_setpoint,
			.p_process_value = &g_motor_controller_3.state.current,
			.p_out = &g_motor_controller_3.outputs.voltage,
			.p_out_max_1 = &g_motor_controller_3.limits.voltage_max,
			.p_out_max_2 = NULL,
		},
	},
	.internals =
	{
		.loop_iteration_count = 9, // Offset this a to avoid all 3 motors having the computation intense cascaded control loop in the same iteration
		
		.sensors = {
			.encoder_steps = 0,
			.encoder_velocity_ticks = UINT16_MAX,
			.ema_filtered_voltage = 0,
			.ema_filtered_current = 0,
			.ema_filtered_velocity = 0,
			.last_filtered_velocity = 0,
			.ema_filtered_acceleration = 0,
			.rotation_direction = 0,
			.rotation_direction_changed = 1,
		},
		.contact_detection = {
			.contact_detected = 0,
		},
	},
};
#endif // M3_PRESENT
