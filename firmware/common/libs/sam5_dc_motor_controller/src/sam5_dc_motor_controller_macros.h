#ifndef _SAM5_DC_MOTORCONTROLLER_MACROS_H_
#define _SAM5_DC_MOTORCONTROLLER_MACROS_H_

#define M_ENCODER_COUNTS_PER_REV	12
#define M_ENCODER_TC_PRESCALER		TC_CTRLA_PRESCALER_DIV16_Val

#define M_WORM_GEAR_RATIO			20
#define M_FINGER_MAX_RANGE			0.10f


#define M_SUPC_VREF_SEL		SUPC_VREF_SEL_1V0_Val			///< Value for the reference voltage supply
#define M_OCM_ADC_REFERENCE	ADC_REFCTRL_REFSEL_INTREF_Val	///< ADC voltage reference for CURRENT MEASURING

#define M_CONTROLLER_FREQ		10000						///< [Hz] The frequency at which the controllers are run Minimum 1000
#define M_CALIBRATION_PRSCLR	10							///< Calibration prescaler, calibration is hardcoded for 1kHz update frequency
#define M_POS_VEL_CTRL_PRSCLR	10							///< Position/Velocity Control prescaler

#define M_LOOP_TCn			0								///< Timer/Counter used for the control loop (TCn 1, 2, 3, ... are used by Motor 1, 2, 3, ...)

#define M_MAX_BREAK_ACCEL	25								///< [rad/(s*s)] Must be positive

#define M_RIP_FREE_VOLTAGE	4.5f							///< Voltage to rip motor free

#define M_LIMIT_VELOCITY		3000.f
#define M_LIMIT_VOLTAGE			6.f
#define M_LIMIT_CURRENT			1.f

#define M_CALIBRATION_CURRENT_THRESHOLD 0.1f				///< [A] if more current is measured than this we assume the end of the movement range

#if M_CONTROLLER_FREQ != 10000
#error "Please recalculate the VOLTAGE_FILTER_EMA_ALPHA and fix this error afterwards"
#endif

//###############################################################################
// Exponential moving average alphas:
// Follow this wikipedia article to calculate the alphas from tau and delta T
// https://en.wikipedia.org/wiki/Exponential_smoothing#Time_constant

// L = 0.6 mH  (from literature, measured 0.6 mH)
// R = 3.2 Ohm (from literature, measured 3.8 Ohm including cables)
// tau = L/R
// delta T = 1 / M_CONTROLLER_FREQ
#define VOLTAGE_FILTER_EMA_ALPHA	0.4133537	

#define CURRENT_LOW_PASS_EMA 0.010480742
#define VELOCITY_LOW_PASS_EMA 0.010480742
#define ACCELERATION_LOW_PASS_EMA 0.010480742



#if M_MAX_BREAK_ACCEL <= 0
#error "M_MAX_BREAK_ACCEL below or equal zero"!
#endif

#define MOTOR_THERMAL_MASS			(385 * 0.001f)					///< [Joule/Kelvin] We assume 1g of copper
#define MOTOR_THERMAL_REDUCTION		0.001f							///< [Watt/Kelvin] How many watt of heat will dissipate per kelvin of temperature difference
#define MOTOR_CONDUCTOR_TEMP_COEF	0.004f							///< [1/Kelvin] How much the resistance changes depending on the motor temp

#define MOTOR_R_COIL_AMBIENT		3.2f

#define M_PWM_FREQ			20000									///< The frequency the TCC unit runs at to send the PWM signal
#define M_PWM_MAX_TICKS		(CONF_CPU_FREQUENCY / M_PWM_FREQ)
#define M_PWM_MIN_TICKS		(M_PWM_MAX_TICKS / 20)					///< The motor driver does not forward less than 5% duty cycle

#endif // _SAM5_DC_MOTORCONTROLLER_MACROS_H_