/**
* \file sam5_dc_motor_controller.h
*
* sam5_dc_motor_controller for controlling brushed DC motors with microcontrollers of the samd5/e5 family
* Copyright (C) 2022  Michael Ratzel

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SAM5_DC_MOTOR_CONTROLLER_IMPL_H_
#define SAM5_DC_MOTOR_CONTROLLER_IMPL_H_

#include "sam5_dc_motor_controller.h"
#include "sam5_dc_motor_controller_macros.h"

/**
* \brief Function to initialize the peripherals for a given motor controller
*
* \param controller pointer to the motor for which the peripherals are initialized
*/
uint8_t initialize_motor_controller_peripherals(dc_motor_controller_t *controller);

/**
* \brief Function to initialize the pins for a given motor controller
*
* \param controller pointer to the given motor controller
*/
uint8_t initialize_motor_controller_pins(dc_motor_controller_t *controller);

/**
* \brief Function to initialize the timer/counter for a given motor controller
*
* \param controller pointer to the given motor controller
*/
uint8_t initialize_motor_controller_pwm(dc_motor_controller_t *controller);

/**
* \brief Function to initialize the external interrupt controller for a given motor controller
*
* \param controller pointer to the motor for which the external interrupt controller is initialized
*/
uint8_t initialize_motor_controller_eic(dc_motor_controller_t *controller);

/**
* \brief Function to initialize the velocity measurement timer for a given motor controller
*
* \param controller pointer to the motor for which the velocity measurement timer is initialized
*/
uint8_t initialize_motor_controller_tc(dc_motor_controller_t *controller);

/**
* \brief Function to initialize the event system for a given motor controller
*
* \param controller pointer to the motor for which the event system is initialized
*/
uint8_t initialize_motor_controller_evsys(dc_motor_controller_t *controller);


/**
* \brief Function to calibrate a given motor controller
*
* \param controller pointer to the given motor controller
*/
uint8_t calibrate_motor_controller_ocm(dc_motor_controller_t *controller);

/**
* \brief Function to disable a given motor controller
*
* \param controller pointer to the given motor controller
*/
uint8_t disable_motor_controller(dc_motor_controller_t *controller);

int16_t receive_measurements(dc_motor_controller_t *controller);
void apply_outputs(dc_motor_controller_t *controller);
void detect_contact(dc_motor_controller_t *controller);

/**
* \brief Function to initialize the PI controller parameters of the different controllers
*
* \param controller pointer to the given motor controller
*/
uint8_t initialize_motor_controllers(dc_motor_controller_t *controller);

/**
* \brief Function to initialize the voltage reference system
*/
uint8_t initialize_motor_controller_vref();

/**
* \brief Function to initialize the looping tc
*/
uint8_t initialize_motor_controller_loop_tc();

/**
* \brief Function to initialize both adc instances
*/
uint8_t initialize_motor_controller_adc();

/**
* \brief Function to initialize both dac instances
*/
uint8_t initialize_motor_controller_dac();

void output_debug_data_via_dac(float data, float min_value, float max_value);

/**
* \ingroup user
* \brief Function that executes the given motor controller
*
* This function executes the state machine of the given motor controller. It has to be called repeatedly in the main while loop at least with the frequency of the fastest controller.
*
* \param controller Pointer to the given motor controller instance
*/
void run_state_machine(dc_motor_controller_t *controller);

/**
* \ingroup developer
* \brief Enum defining the events for the state machine
*/
enum
{
	DC_MOTOR_CONTROLLER_EVT_RUN_LOOP = EVENT_BASE,
	DC_MOTOR_CONTROLLER_EVT_HARDWARE_ON,
	DC_MOTOR_CONTROLLER_EVT_HARDWARE_OFF,
	DC_MOTOR_CONTROLLER_EVT_HARDWARE_ERROR,
};

void dc_motor_controller_state_init(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void dc_motor_controller_state_calibration(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void dc_motor_controller_state_stop(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void dc_motor_controller_state_open_loop_control(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void dc_motor_controller_state_current_control(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void dc_motor_controller_state_velocity_control(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void dc_motor_controller_state_closed_loop_control(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);

void initial_calib_state_waiting(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_rip_free(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_polarity_neg(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_polarity_pos(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_abs_end_neg(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_waiting2(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_abs_end_pos(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_contact_neg_to_pos_pass(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_contact_pos_to_neg_pass(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_go_back_to_start(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_error(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);
void initial_calib_state_finished(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);

void contact_calib_init(dc_motor_controller_t* controller);
void contact_calib_get_expected_values(const dc_motor_controller_calibration_t* calib, const dc_motor_controller_combined_state_t* state, float32_t* expected_velocity, float32_t* expected_current);

// Create macro containing reference voltage
#if (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_1V0_Val)
#	define M_SUPC_VREF_V 1.0f
#elif (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_1V1_Val)
#	define M_SUPC_VREF_V 1.1f
#elif (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_1V2_Val)
#	define M_SUPC_VREF_V 1.2f
#elif (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_1V25_Val)
#	define M_SUPC_VREF_V 1.25f
#elif (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_2V0_Val)
#	define M_SUPC_VREF_V 2.0f
#elif (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_2V2_Val)
#	define M_SUPC_VREF_V 2.2f
#elif (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_2V4_Val)
#	define M_SUPC_VREF_V 2.4f
#elif (M_SUPC_VREF_SEL) == (SUPC_VREF_SEL_2V5_Val)
#	define M_SUPC_VREF_V 2.5f
#else
#error "Unexpected value for M_SUPC_VREF_SEL"
#endif


// Macros to generate EIC_Handler symbols from the number
#define _EICn_Handler(num) EIC_##num##_Handler
#define EICn_Handler(num) _EICn_Handler(num)

// Macros to generate TC symbols from the number
#define _TCn(num) TC##num
#define TCn(num) _TCn(num)

// Macros to generate TCC symbols from the number
#define _TCCn(num) TCC##num
#define TCCn(num) _TCCn(num)

// Macros to generate TC_IRQn symbols from the number
#define _TCn_Handler(num) TC##num##_Handler
#define TCn_Handler(num) _TCn_Handler(num)

// Macros to generate ADCn symbols from the number
#define _ADCn(num) ADC##num
#define ADCn(num) _ADCn(num)

#endif // SAM5_DC_MOTOR_CONTROLLER_IMPL_H_