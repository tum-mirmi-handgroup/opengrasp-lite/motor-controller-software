#include "sam5_dc_motor_controller_impl.h"
#include <string.h>

static int8_t get_current_angle_range(const dc_motor_controller_state_t* state, const dc_motor_controller_contact_detection_t* detection);
static void trigger_next_pass(dc_motor_controller_t* controller);

void contact_calib_init(dc_motor_controller_t* controller){
	dc_motor_controller_contact_detection_t* detection = &controller->internals.contact_detection;
	memset(detection->data_cur_pos, 0, sizeof(detection->data_cur_pos));
	memset(detection->data_cur_neg, 0, sizeof(detection->data_cur_neg));
	
	detection->flags.started_with_pos = controller->state.flags_b.calib_stage == CALIB_STAGE_NEG_TO_POS_PASS;
	detection->current_voltage_pass = 0;
	detection->angle_min = controller->limits.angle_min;
	detection->angle_max = controller->limits.angle_max;
	detection->last_angle_range = get_current_angle_range(&controller->state, &controller->internals.contact_detection);
	detection->sum_cur = 0;
	detection->count_measurements = 0;
	
	if(detection->angle_max - detection->angle_min < 25.f){
		controller->calib_res.error_code = CALIB_ERROR_ANGLE_RANGE_TOO_SMALL;
		simpleStateMachineTran_(&controller->internals.calibration.state_machine, initial_calib_state_error, controller);
	}
	detection->flags.initialized = 1;
}


void initial_calib_state_contact_neg_to_pos_pass(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->state.flags_b.calib_stage = CALIB_STAGE_NEG_TO_POS_PASS;
			controller->internals.calibration.movement_expected = 1;
			if(!controller->internals.contact_detection.flags.initialized){
				contact_calib_init(controller);
			}
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			dc_motor_controller_contact_detection_t* detection = &controller->internals.contact_detection;
			controller->outputs.voltage = detection->current_voltage_pass == 0 ? CONTACT_CALIB_VOLTAGE_1 : CONTACT_CALIB_VOLTAGE_2;
			int8_t angle_range = get_current_angle_range(&controller->state, &controller->internals.contact_detection);
			if(detection->last_angle_range != angle_range){
				if(0 <= detection->last_angle_range && detection->last_angle_range < CONTACT_CALIB_NUM_ANGLE_RANGES){
					detection->data_cur_pos[detection->current_voltage_pass][detection->last_angle_range] = detection->sum_cur / detection->count_measurements;
				}
				detection->sum_cur = 0;
				detection->count_measurements = 0;
				detection->last_angle_range = angle_range;
			}
			detection->sum_cur += controller->state.current;
			++detection->count_measurements;
			
			if(angle_range == CONTACT_CALIB_NUM_ANGLE_RANGES + 1){
				trigger_next_pass(controller);
			}
			break;
		}
		case EXIT_SIG: {
			break;
		}
		default:
		break;
	}
}

void initial_calib_state_contact_pos_to_neg_pass(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct){
	dc_motor_controller_t * const controller = (dc_motor_controller_t *) user_struct;
	switch(event->signal_) {
		case ENTRY_SIG: {
			controller->state.flags_b.calib_stage = CALIB_STAGE_POS_TO_NEG_PASS;
			controller->internals.calibration.movement_expected = 1;
			if(!controller->internals.contact_detection.flags.initialized){
				contact_calib_init(controller);
			}
			break;
		}
		case DC_MOTOR_CONTROLLER_EVT_RUN_LOOP: {
			dc_motor_controller_contact_detection_t* detection = &controller->internals.contact_detection;
			controller->outputs.voltage = detection->current_voltage_pass == 0 ? -CONTACT_CALIB_VOLTAGE_1 : -CONTACT_CALIB_VOLTAGE_2;
			int8_t angle_range = get_current_angle_range(&controller->state, &controller->internals.contact_detection);
			if(detection->last_angle_range != angle_range){
				if(0 <= detection->last_angle_range && detection->last_angle_range < CONTACT_CALIB_NUM_ANGLE_RANGES){
					detection->data_cur_neg[detection->current_voltage_pass][detection->last_angle_range] = detection->sum_cur / detection->count_measurements;
				}
				detection->sum_cur = 0;
				detection->count_measurements = 0;
				detection->last_angle_range = angle_range;
			}
			detection->sum_cur -= controller->state.current;
			++detection->count_measurements;
			
			if(angle_range == -2){
				trigger_next_pass(controller);
			}
			break;
		}
		case EXIT_SIG: {
			break;
		}
		default:
		break;
	}
}

static int8_t get_current_angle_range(const dc_motor_controller_state_t* state, const dc_motor_controller_contact_detection_t* detection){
	const float32_t angle_range = detection->angle_max - detection->angle_min;
	const float32_t end_angle = 0.01f;
	if(state->angle < detection->angle_min + end_angle * angle_range){
		return -2;
	}
	if(state->angle < detection->angle_min + 2 * end_angle * angle_range){
		return -1;
	}
	for(uint8_t i = 0; i < CONTACT_CALIB_NUM_ANGLE_RANGES; ++i){
		if(state->angle < detection->angle_min + (2 * end_angle + (i + 1.0f) * (1 - 4 * end_angle) / CONTACT_CALIB_NUM_ANGLE_RANGES) * angle_range){
			return i;
		}
	}
	if(state->angle < detection->angle_min + (1 - end_angle) * angle_range){
		return CONTACT_CALIB_NUM_ANGLE_RANGES;
	}
	return CONTACT_CALIB_NUM_ANGLE_RANGES + 1;
}

static void trigger_next_pass(dc_motor_controller_t* controller){
	dc_motor_controller_contact_detection_t* detection = &controller->internals.contact_detection;
	const bool voltage_pass_finished = (detection->flags.started_with_pos == 0 && controller->state.flags_b.calib_stage == CALIB_STAGE_NEG_TO_POS_PASS) || (detection->flags.started_with_pos == 1 && controller->state.flags_b.calib_stage == CALIB_STAGE_POS_TO_NEG_PASS);
	controller->outputs.voltage = 0;
	if(voltage_pass_finished){
		if(detection->current_voltage_pass + 1 == CONTACT_CALIB_NUM_VOLTAGE_PASSES){
			// We are finished -> shift angle limits to the ones used during calibration as those limits will be used during interpolation to determine the angle position
			float32_t angle_range = detection->angle_max - detection->angle_min;
			detection->angle_max = detection->angle_min + 0.9f * angle_range;
			detection->angle_min = detection->angle_min + 0.1f * angle_range;
			detection->angle_factor = CONTACT_CALIB_NUM_ANGLE_RANGES / (detection->angle_max - detection->angle_min);
			detection->flags.initialized = 0;
			
			ASSERT(CONTACT_CALIB_NUM_ANGLE_RANGES >= 20);
			
			float32_t avg_pos[CONTACT_CALIB_NUM_VOLTAGE_PASSES] = {0};
			float32_t avg_neg[CONTACT_CALIB_NUM_VOLTAGE_PASSES] = {0};
			for(uint8_t j = 0; j < CONTACT_CALIB_NUM_VOLTAGE_PASSES; ++j){
				for(uint8_t i = 0; i < CONTACT_CALIB_NUM_ANGLE_RANGES; ++i){
					avg_pos[j] += detection->data_cur_pos[j][i];
					avg_neg[j] += detection->data_cur_neg[j][i];
				}
			}
			for(uint8_t j = 0; j < CONTACT_CALIB_NUM_VOLTAGE_PASSES; ++j){
				avg_pos[j] /= (CONTACT_CALIB_NUM_ANGLE_RANGES);
				avg_neg[j] /= (CONTACT_CALIB_NUM_ANGLE_RANGES);
			}
			
			detection->friction_pos.a1 = (avg_pos[0] - avg_pos[1]) / (CONTACT_CALIB_VOLTAGE_1 - CONTACT_CALIB_VOLTAGE_2);
			detection->friction_pos.a0 = (avg_pos[0] - detection->friction_pos.a1 * CONTACT_CALIB_VOLTAGE_1);
			
			detection->friction_neg.a1 = (avg_neg[0] - avg_neg[1]) / (CONTACT_CALIB_VOLTAGE_1 - CONTACT_CALIB_VOLTAGE_2);
			detection->friction_neg.a0 = (avg_neg[0] - detection->friction_neg.a1 * CONTACT_CALIB_VOLTAGE_1);
			
			for (uint8_t i = 0; i < CONTACT_CALIB_NUM_ANGLE_RANGES; ++i){
				detection->friction_pos.friction_factor[i] = 0;
				detection->friction_neg.friction_factor[i] = 0;
				for(uint8_t j = 0; j < CONTACT_CALIB_NUM_VOLTAGE_PASSES; ++j){
					detection->friction_pos.friction_factor[i] += detection->data_cur_pos[j][i] / avg_pos[j];
					detection->friction_neg.friction_factor[i] += detection->data_cur_neg[j][i] / avg_neg[j];
				}
				detection->friction_pos.friction_factor[i] /= CONTACT_CALIB_NUM_VOLTAGE_PASSES;
				detection->friction_neg.friction_factor[i] /= CONTACT_CALIB_NUM_VOLTAGE_PASSES;
			}
			simpleStateMachineTran_(&controller->internals.calibration.state_machine, initial_calib_state_go_back_to_start, (void*) controller);
			return;
		}
		else{
			++detection->current_voltage_pass;
		}
	}
	if(controller->state.flags_b.calib_stage == CALIB_STAGE_NEG_TO_POS_PASS){
		simpleStateMachineTran_(&controller->internals.calibration.state_machine, initial_calib_state_contact_pos_to_neg_pass, (void*) controller);
	}
	else{
		simpleStateMachineTran_(&controller->internals.calibration.state_machine, initial_calib_state_contact_neg_to_pos_pass, (void*) controller);
	}
}