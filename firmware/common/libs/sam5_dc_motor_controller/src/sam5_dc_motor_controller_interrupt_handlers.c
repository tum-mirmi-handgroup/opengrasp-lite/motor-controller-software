#include "sam5_dc_motor_controller_impl.h"

static inline void motor_endstop_handler(dc_motor_controller_t *motor_controller) {
	if (!gpio_get_pin_level(motor_controller->hardware.endstop_pin)) {
		if(motor_controller->state.flags_b.endstop_active == 0){
			// Ensure we are on a rising edge
			if(motor_controller->calib_res.flags.searching_endstop){
				// Store endstop ticks for later
				if(motor_controller->calib_res.flags.endstop_trig_calibrated == 0){
					motor_controller->internals.sensors.endstop_encoder_steps_trig = motor_controller->internals.sensors.encoder_steps;
					motor_controller->calib_res.flags.endstop_trig_calibrated = 1;
				}
				else{
					motor_controller->internals.sensors.endstop_encoder_steps_trig = (motor_controller->internals.sensors.encoder_steps + motor_controller->internals.sensors.endstop_encoder_steps_trig) / 2;
				}
			}
			if(motor_controller->calib_res.flags.endstop_trig_calibrated && motor_controller->inputs_com.flags.use_endstop_for_refine){
				// Slightly adjust current encoder steps to counteract drift
				motor_controller->internals.sensors.encoder_steps = 0.9 * motor_controller->internals.sensors.encoder_steps + 0.1 * motor_controller->internals.sensors.endstop_encoder_steps_trig;
			}
		}
		// PIN LOW -> Endstop triggered
		motor_controller->state.flags_b.endstop_active = 1;
		} else {
		if(motor_controller->state.flags_b.endstop_active == 1){
			// Ensure we are on a falling edge
			if(motor_controller->calib_res.flags.searching_endstop){
				// Store endstop ticks for later
				if(motor_controller->calib_res.flags.endstop_untrig_calibrated == 0){
					motor_controller->internals.sensors.endstop_encoder_steps_untrig = motor_controller->internals.sensors.encoder_steps;
					motor_controller->calib_res.flags.endstop_untrig_calibrated = 1;
				}
				else{
					motor_controller->internals.sensors.endstop_encoder_steps_untrig = (motor_controller->internals.sensors.encoder_steps + motor_controller->internals.sensors.endstop_encoder_steps_untrig) / 2;
				}
			}
			if(motor_controller->calib_res.flags.endstop_untrig_calibrated && motor_controller->inputs_com.flags.use_endstop_for_refine){
				// Slightly adjust current encoder steps to counteract drift
				motor_controller->internals.sensors.encoder_steps = 0.9f * motor_controller->internals.sensors.encoder_steps + 0.1f * motor_controller->internals.sensors.endstop_encoder_steps_untrig;
			}
		}
		// PIN LOW -> Endstop untriggered
		motor_controller->state.flags_b.endstop_active = 0;
	}
	apply_outputs(motor_controller);
	hri_eic_clear_INTFLAG_reg(EIC, 1 << motor_controller->hardware.endstop_eic_number);
}


static inline void motor_velocity_interrupt_handler(dc_motor_controller_t *motor_controller) {
	volatile dc_motor_controller_raw_sensor_data_t *sensors = &(motor_controller->internals.sensors);
	const Tc* p_encoder_tc = motor_controller->hardware.p_encoder_tc;

	if (hri_tc_get_INTFLAG_MC0_bit(p_encoder_tc)) {
		
		// Compute current velocity
		if (sensors->rotation_direction_changed > 0) {
			sensors->rotation_direction_changed = 0;
			sensors->encoder_velocity_ticks = 0;
			hri_tc_clear_INTFLAG_MC0_bit(p_encoder_tc);
		}
		else {
			sensors->encoder_velocity_ticks = hri_tccount16_read_CC_reg(p_encoder_tc, 0);
		}
	}
	if (hri_tc_get_INTFLAG_OVF_bit(p_encoder_tc)) {
		sensors->encoder_velocity_ticks = 0;
		sensors->rotation_direction_changed = 1;
		hri_tc_clear_INTFLAG_OVF_bit(p_encoder_tc);
	}
}

static inline void motor_diag_interrupt_handler(dc_motor_controller_t *motor_controller) {
	static simple_state_machine_event_t event;
	if (gpio_get_pin_level(motor_controller->hardware.en_pin)) {
		if (gpio_get_pin_level(motor_controller->hardware.diag_pin)) {
			event.signal_ = DC_MOTOR_CONTROLLER_EVT_HARDWARE_ON;
			motor_controller->state.flags_a.hardware_state = 0x1;
			} else {
			event.signal_ = DC_MOTOR_CONTROLLER_EVT_HARDWARE_ERROR;
			motor_controller->state.flags_a.hardware_state = 0x2;
		}
		} else {
		if (gpio_get_pin_level(motor_controller->hardware.diag_pin)) {
			event.signal_ = DC_MOTOR_CONTROLLER_EVT_HARDWARE_ERROR;
			motor_controller->state.flags_a.hardware_state = 0x3;
			} else {
			event.signal_ = DC_MOTOR_CONTROLLER_EVT_HARDWARE_OFF;
			motor_controller->state.flags_a.hardware_state = 0x0;
		}
	}
	simpleStateMachineDispatch_(&(motor_controller->internals.state_machine), &event, motor_controller);
	apply_outputs(motor_controller);
	hri_eic_clear_INTFLAG_reg(EIC, 1 << motor_controller->hardware.diag_eic_number);
}

static inline void motor_encoder_interrupt_handler_channel_A(dc_motor_controller_t *motor_controller) {
	const bool pin_a = (((Port *) PORT)->Group[GPIO_PORT(motor_controller->hardware.encoder_a.pin)].IN.reg >> GPIO_PIN(motor_controller->hardware.encoder_a.pin)) & 0x1;
	const bool pin_b = (((Port *) PORT)->Group[GPIO_PORT(motor_controller->hardware.encoder_b.pin)].IN.reg >> GPIO_PIN(motor_controller->hardware.encoder_b.pin)) & 0x1;
	const int8_t dir_increment = (pin_a == pin_b) ? -1 : 1;
	
	if(motor_controller->internals.sensors.rotation_direction != dir_increment){
		motor_controller->internals.sensors.rotation_direction_changed = 1;
	}
	motor_controller->internals.sensors.rotation_direction = dir_increment;
	motor_controller->internals.sensors.encoder_steps += dir_increment;
	hri_eic_clear_INTFLAG_reg(EIC, 1 << motor_controller->hardware.encoder_a.eicn);
}

static inline void motor_encoder_interrupt_handler_channel_B(dc_motor_controller_t *motor_controller) {
	const bool pin_a = (((Port *) PORT)->Group[GPIO_PORT(motor_controller->hardware.encoder_a.pin)].IN.reg >> GPIO_PIN(motor_controller->hardware.encoder_a.pin)) & 0x1;
	const bool pin_b = (((Port *) PORT)->Group[GPIO_PORT(motor_controller->hardware.encoder_b.pin)].IN.reg >> GPIO_PIN(motor_controller->hardware.encoder_b.pin)) & 0x1;
	const int8_t dir_increment = (pin_a == pin_b) ? 1 : -1;

	if(motor_controller->internals.sensors.rotation_direction != dir_increment){
		motor_controller->internals.sensors.rotation_direction_changed = 1;
	}
	motor_controller->internals.sensors.rotation_direction = dir_increment;
	motor_controller->internals.sensors.encoder_steps += dir_increment;
	hri_eic_clear_INTFLAG_reg(EIC, 1 << motor_controller->hardware.encoder_b.eicn);
}

#ifdef M1_PRESENT
/**
* \brief Interrupt service routine executed on M1 velocity measurement TC capture or overflow
*/
void TCn_Handler(1)(void) {
	motor_velocity_interrupt_handler(&g_motor_controller_1);
}

void EICn_Handler(M1_ENDSTOP_EICn)(void) {
	motor_endstop_handler(&g_motor_controller_1);
}

void EICn_Handler(M1_DIAG_EICn)(void) {
	motor_diag_interrupt_handler(&g_motor_controller_1);
}

void EICn_Handler(M1_ENCODER_A_EICn)(void) {
	motor_encoder_interrupt_handler_channel_A(&g_motor_controller_1);
}

void EICn_Handler(M1_ENCODER_B_EICn)(void) {
	motor_encoder_interrupt_handler_channel_B(&g_motor_controller_1);
}

#endif // M1_PRESENT

#ifdef M2_PRESENT
/**
* \brief Interrupt service routine executed on M2 velocity measurement TC capture or overflow
*/
void TCn_Handler(2)(void) {
	motor_velocity_interrupt_handler(&g_motor_controller_2);
}

void EICn_Handler(M2_ENDSTOP_EICn)(void) {
	motor_endstop_handler(&g_motor_controller_2);
}

void EICn_Handler(M2_DIAG_EICn)(void) {
	motor_diag_interrupt_handler(&g_motor_controller_2);
}

void EICn_Handler(M2_ENCODER_A_EICn)(void) {
	motor_encoder_interrupt_handler_channel_A(&g_motor_controller_2);
}

void EICn_Handler(M2_ENCODER_B_EICn)(void) {
	motor_encoder_interrupt_handler_channel_B(&g_motor_controller_2);
}

#endif // M2_PRESENT

#ifdef M3_PRESENT
/**
* \brief Interrupt service routine executed on M3 velocity measurement TC capture or overflow
*/
void TCn_Handler(3)(void) {
	motor_velocity_interrupt_handler(&g_motor_controller_3);
}

void EICn_Handler(M3_ENDSTOP_EICn)(void) {
	motor_endstop_handler(&g_motor_controller_3);
}

void EICn_Handler(M3_DIAG_EICn)(void) {
	motor_diag_interrupt_handler(&g_motor_controller_3);
}

void EICn_Handler(M3_ENCODER_A_EICn)(void) {
	motor_encoder_interrupt_handler_channel_A(&g_motor_controller_3);
}

void EICn_Handler(M3_ENCODER_B_EICn)(void) {
	motor_encoder_interrupt_handler_channel_B(&g_motor_controller_3);
}

#endif // M3_PRESENT

