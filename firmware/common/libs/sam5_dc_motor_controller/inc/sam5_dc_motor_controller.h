/**
* \file sam5_dc_motor_controller.h
*
* sam5_dc_motor_controller for controlling brushed DC motors with microcontrollers of the samd5/e5 family
* Copyright (C) 2022  Michael Ratzel

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
* \mainpage SAMD51 BLDC Motor Controller
* The documentation of this library is split into two parts. The first part contains only the data structures relevant to users of this library.
* The second part is aimed at developers and contains the documentation of all the internal functions.
* A developer is required to also read the user documentation.
* - \subpage user_page "Using the Library"
* - \subpage developer_page "Working on the Library"
*/

/**
* \defgroup user
* This module contains all the data structures relevant to a user.
*/

/**
* \page user_page Using the Library
* This library is meant for controlling BLDC motors with a microcontroller in the SAMD5 or SAME5 family from Microchip.
* In order to use the library in a project, the user has to set up the clock configuration so that the generic clock generator 0 runs at 100 MHz.
* The user also has to modify the template header file sam5_dc_motor_controller_hardware_definitions.h according to his electronics setup and include his modified file in his project.
* Lastly, the user must not use the peripherals used by the library.
* These peripherals are the ones specified by the user in the template header file and the following ones:
* - If M1_PRESENT is defined
*   - TCC0
*   - TC1
*   - DMAC channel 0 and 1
*   - Event system channel 12 and 13
* - If M2_PRESENT is defined
*   - TCC1
*   - TC2
*   - DMAC channel 2 and 3
*   - Event system channel 14 and 15
*
* The following link leads to the module containing everything that is relevant for using the library:
* \ref user
*/

/**
* \defgroup developer
* This module contains all the data structures not relevant to the user.
*/

/**
* \page developer_page Working on the Library
* The following link leads to the module containing everything that is internal to the library:
* \ref developer
*/

#ifndef SAM5_DC_MOTOR_CONTROLLER_H_
#define SAM5_DC_MOTOR_CONTROLLER_H_

#include <atmel_start.h>
#include <peripheral_clk_config.h>
#include "arm_math.h"
#include "arm_neon.h"

#include "simple_state_machine.h"
#include "hardware_defines.h"
#include "sam5_control.h"


void output_debug_data_via_dac(float data, float min_value, float max_value);

#define CONTACT_CALIB_VOLTAGE_1				3.f
#define CONTACT_CALIB_VOLTAGE_2				6.f
#define CONTACT_CALIB_NUM_VOLTAGE_PASSES	2
#define CONTACT_CALIB_NUM_ANGLE_RANGES		50

typedef enum {
	MOTOR_CM_STOPPED = 0,										///< No voltage is applied to the motor
	MOTOR_CM_CALIB_PREP_POS = 2,								///< Prepare calibration by moving to positive end of the movement range
	MOTOR_CM_CALIB_PREP_NEG = 4,								///< Prepare calibration by moving to negative end of the movement range
	MOTOR_CM_CALIBRATION = 5,									///< Run finger calibration
	MOTOR_CM_VOLTAGE = 6,										///< The voltage setpoint is applied to the motor
	// The following three CM must have exactly those 3 numbers DO NOT CHANGE THIS
	MOTOR_CM_CURRENT =  0b001, // 1								///< The current setpoint is applied to the motor by the current controller
	MOTOR_CM_VELOCITY = 0b011, // 3								///< The velocity setpoint is applied to the motor while keeping the current limits
	MOTOR_CM_POSITION = 0b111, // 7								///< The position setpoint is applied to the motor while keeping the velocity and current limits
}control_mode_e;

typedef enum {
	MOTOR_FSM_INIT = 0,
	MOTOR_FSM_CALIBRATION,
	MOTOR_FSM_STOP,
	MOTOR_FSM_OPEN_LOOP_CONTROL,
	MOTOR_FSM_CLOSE_LOOP_CONTROL,
} motor_state_machine_state_e;

typedef enum {
	CALIB_STAGE_WAITING = 0,
	CALIB_STAGE_WAITING2,
	CALIB_STAGE_RIP_FREE,
	CALIB_STAGE_POLARITY_NEG,
	CALIB_STAGE_POLARITY_POS,
	CALIB_STAGE_ABS_END_NEG,
	CALIB_STAGE_ABS_END_POS,
	CALIB_STAGE_NEG_TO_POS_PASS,
	CALIB_STAGE_POS_TO_NEG_PASS,
	CALIB_STAGE_GO_BACK_TO_START,
	CALIB_STAGE_ERROR,
	CALIB_STAGE_FINISHED,
} calibration_stage_e;

typedef struct {
	const uint8_t pin;
	const uint8_t funct;
	const uint8_t ccn;
} dc_motor_controller_pwm_t;

typedef struct {
	const uint8_t pin;
	const uint8_t eicn;
} dc_motor_controller_encoder_t;

typedef struct {
	const uint8_t en_pin;
	
	const uint8_t pwm_tccn;
	const uint8_t pwm_otmx;
	Tcc * const p_pwm_tcc;
	const dc_motor_controller_pwm_t pwm_forward;
	const dc_motor_controller_pwm_t pwm_reverse;
	
	const uint8_t endstop_pin;
	const uint8_t endstop_eic_number;
	
	const uint8_t ocm_pin;										///< Pin of the current measurement
	const uint8_t ocm_adcn;										///< Number of the ADC instance used to measure the current of this motor
	const uint8_t ocm_adc_ainn;									///< Number of the Analog INput channel within the ADC instance
	Adc * const p_ocm_adc;										///< Pointer to the ADC instance used to measure the phase currents of this motor.
	const float32_t ocm_conv_factor;							///< Factor to convert raw ADC output to ocm amperage
	
	const uint8_t diag_pin;
	const uint8_t diag_eic_number;
	
	const dc_motor_controller_encoder_t encoder_a;
	const dc_motor_controller_encoder_t encoder_b;
	const uint8_t encoder_tcn;
	Tc * const p_encoder_tc;
	const uint8_t encoder_evsys_channel;
	const float32_t velocity_conv_factor;						///< Conversion factor from ticks to velocity after the gear
	const float32_t position_conv_factor;						///< Conversion factor from total ticks to position after the gear

} dc_controller_hardware_param_t;

typedef struct {
	uint8_t control_mode : 3;									///< Control mode of the motor controller
	uint8_t stop_at_contact : 1;								///< If this bit is 1 the motor will be stopped at contact
	uint8_t continue_at_end : 1;								///< If this bit is 1 the motor will NOT stop at the movement range
	uint8_t use_endstop_for_end : 1;							///< If this bit is 1 the motor will use the endstop to detect the end of the movement range
	uint8_t use_endstop_for_refine : 1;							///< If this bit is 1 the motor will use the endstop to refine the current position with when the endstop is triggered
	uint8_t use_aggressive_contact_detection: 1;				///< If this bit is 1 the motor will not stop so easily on a contact and be a bit more aggressive
} dc_motor_controller_input_flags_t;

typedef struct {
	float32_t angle;											///< Angle of the motor [rad]
	float32_t velocity;											///< Velocity of the motor [rad/s]
	float32_t current;											///< Motor current [A]
	float32_t voltage;											///< Motor voltage [V]
	dc_motor_controller_input_flags_t flags;
} dc_motor_controller_inputs_t;

typedef struct {
	bool enable;												///< If the enable pin should be set
	float32_t voltage;											///< Motor voltage [V]
} dc_motor_controller_outputs_t;

typedef struct {
	uint8_t hardware_state : 2;
	uint8_t contact_detected : 1;
	uint8_t min_angle_reached : 1;
	uint8_t max_angle_reached : 1;
	uint8_t target_angle_reached : 1;
	uint8_t stopped_due_to_contact : 1;
	uint8_t motor_stopped : 1;
} dc_motor_controller_state_flags_a_t;

typedef struct {
	uint8_t state_machine : 3;
	uint8_t calib_stage : 4;
	uint8_t endstop_active : 1;
} dc_motor_controller_state_flags_b_t;

typedef struct {
	float32_t angle;											///< Angle of the motor after gearbox [rad]
	float32_t velocity;											///< Velocity of the motor after gearbox [rad/s]
	float32_t current;											///< Motor current [A]
	float32_t voltage;											///< Motor voltage [V]
	float32_t temperature;										///< Temp above ambient [K]
	dc_motor_controller_state_flags_a_t flags_a;				///< Flags that describe different states
	dc_motor_controller_state_flags_b_t flags_b;				///< Flags that describe different states
} dc_motor_controller_state_t;

typedef struct {
	float32_t angle_min;										///< [rad] Lowest positive value of the angle of the motor after gearbox
	float32_t angle_max;										///< [rad] Highest value of the angle of the motor after gearbox
	float32_t velocity_max;										///< [rad/s] Highest absolute value of the velocity after gearbox applied to the motor
	float32_t current_max;										///< [A] Highest absolute value of the current applied to the motor
	float32_t voltage_max;										///< [V] Highest ABSOLUTE value of the voltage applied to the motor
}  dc_motor_controller_limits_t;

typedef struct {
	uint8_t flip_outputs : 1;
	uint8_t started_calib_at_pos : 1;
	uint8_t pos_end_finished : 1;
	uint8_t neg_end_finished : 1;
	uint8_t endstop_at_pos : 1;
	uint8_t searching_endstop : 1;
	uint8_t endstop_trig_calibrated : 1;
	uint8_t endstop_untrig_calibrated : 1;
} dc_motor_controller_calibration_flags_t;

typedef enum {
	CALIB_ERROR_NONE,
	CALIB_ERROR_NO_MOVEMENT_MEASURED,
	CALIB_ERROR_NO_CURRENT_MEASURED,
	CALIB_ERROR_DETECTED_SPEED_TOO_SMALL,
	CALIB_ERROR_ANGLE_RANGE_TOO_SMALL,
} dc_motor_controller_calibration_error_code_e;

typedef struct {
	float32_t endstop_angle_trigger;
	float32_t endstop_angle_untrigger;
	int16_t ocm_raw_offset;										///< Raw offset of the current sensors [LSB]
	uint8_t error_code;
	dc_motor_controller_calibration_flags_t flags;
} dc_motor_controller_calibration_result_t;

typedef struct {
	sam5_controller_t current_controller;						///< PI controller for current control
	sam5_controller_t velocity_controller;						///< PI controller for velocity control
	sam5_controller_t position_controller;						///< PI controller for position control
	float32_t last_position_input;
	float32_t velocity_setpoint;
	float32_t current_setpoint;
} dc_motor_controller_control_t;

/************************************************************************************************************************************************/
/* INTERNAL DATA                                                                                                                                */
/************************************************************************************************************************************************/
typedef struct {
	
	volatile uint16_t encoder_velocity_ticks;					///< Ticks of the TC since the last encoder interrupt
	volatile int32_t encoder_steps;								///< Steps of the encoder from the 0 position
	volatile int32_t endstop_encoder_steps_trig;
	volatile int32_t endstop_encoder_steps_untrig;
	
	float32_t ema_filtered_voltage;								///< Output voltage after an EMA filter
	float32_t ema_filtered_current;								///< Measured current after an EMA filter
	float32_t ema_filtered_velocity;							///< Measured velocity after an EMA filter
	float32_t last_filtered_velocity;
	float32_t ema_filtered_acceleration;						///< Measured velocity after differentiation and two EMA filter
	
	volatile int8_t rotation_direction;
	volatile int8_t rotation_direction_changed;
} dc_motor_controller_raw_sensor_data_t;


typedef struct {
	float32_t friction_factor[CONTACT_CALIB_NUM_ANGLE_RANGES];
	float32_t a0;
	float32_t a1;
} dc_motor_friction_model_t;

typedef struct {
	float32_t data_cur_pos[CONTACT_CALIB_NUM_VOLTAGE_PASSES][CONTACT_CALIB_NUM_ANGLE_RANGES];
	float32_t data_cur_neg[CONTACT_CALIB_NUM_VOLTAGE_PASSES][CONTACT_CALIB_NUM_ANGLE_RANGES];
	
	dc_motor_friction_model_t friction_pos;
	dc_motor_friction_model_t friction_neg;	
	
	float32_t angle_min;
	float32_t angle_max;
	float32_t angle_factor;
	
	struct {
		uint8_t initialized : 1;
		uint8_t started_with_pos : 1;
	} flags;
	uint8_t current_voltage_pass;
	int8_t last_angle_range;
	float32_t sum_cur;
	uint32_t count_measurements;
	uint8_t contact_detected;
} dc_motor_controller_contact_detection_t;

typedef struct {
	simple_state_machine_t state_machine;						///< State machine used for the initial calibration
	
	uint32_t loop_iteration_count;
	uint32_t start_count;
	uint32_t last_movement_count;
	float32_t max_current_during_rip_free;
	float32_t velocity_with_neg_voltage;
	
	uint8_t movement_expected;
} dc_motor_controller_calibration_t;

typedef struct {
	uint32_t loop_iteration_count;								///< Counter to track the nubmer of loop iterations
	simple_state_machine_t state_machine;						///< State machine used to control the motor
	uint8_t control_mode_from_last_input;
	
	dc_motor_controller_raw_sensor_data_t sensors;
	dc_motor_controller_contact_detection_t contact_detection;
	dc_motor_controller_calibration_t calibration;
} dc_motor_controller_internals_t;

typedef struct {
	const dc_controller_hardware_param_t hardware;				///< Struct containing the pins and peripherals to which this motor instance is connected
	
	// Inputs are double buffered, to allow for a permanent i2c communication
	// inputs_com will be copied atomically once prior to a controller run
	dc_motor_controller_inputs_t inputs_com;					///< Struct containing the inputs of the motor controller. Double buffered to allow for permanent i2c updates
	dc_motor_controller_inputs_t inputs;						///< Struct containing the inputs of the motor controller. Double buffered to allow for permanent i2c updates
	
	// Double buffering of state is not neceassry as we usually only use the combined_state and combined_state_flags variables which are basically the double buffer
	dc_motor_controller_state_t state;						///< Struct containing the information about the current motor state
	dc_motor_controller_outputs_t outputs;						///< Struct containing the outputs of the motor controller
	
	dc_motor_controller_limits_t limits;						///< Struct containing process parameters
	dc_motor_controller_calibration_result_t calib_res;			///< Result of the calibration
	
	dc_motor_controller_control_t control;
	
	dc_motor_controller_internals_t internals;					///< Struct containing internal values needed by the motor controller
	uint8_t id;
} dc_motor_controller_t;

typedef struct {
	float32_t angle[3];
	float32_t current[3];
} dc_motor_controller_combined_state_t;

typedef struct {
	dc_motor_controller_state_flags_a_t flags_a[3];
	dc_motor_controller_state_flags_b_t flags_b[3];
} dc_motor_controller_combined_state_flags_t;



uint8_t sam5_dc_motor_controller_init_shared_resources();
uint8_t sam5_dc_motor_controller_init(dc_motor_controller_t *controller);
uint8_t sam5_dc_motor_controller_enable(dc_motor_controller_t *controller);

/**
* \ingroup user
* \brief Function that starts the TC controlled loop, that calls sam5_dc_motor_controller_run periodically
*/
uint8_t sam5_dc_motor_controller_start_loop();
void sam5_dc_motor_controller_low_prio_loop();


#ifdef M1_PRESENT
extern dc_motor_controller_t g_motor_controller_1;
#endif // M1_PRESENT

#ifdef M2_PRESENT
extern dc_motor_controller_t g_motor_controller_2;
#endif // M2_PRESENT

#ifdef M3_PRESENT
extern dc_motor_controller_t g_motor_controller_3;
#endif // M3_PRESENT

extern volatile dc_motor_controller_combined_state_t combined_state;
extern volatile dc_motor_controller_combined_state_flags_t combined_state_flags;

extern volatile float32_t supply_voltage;
extern float32_t supply_voltage_buf;

#endif /* SAM5_DC_MOTOR_CONTROLLER_H_ */