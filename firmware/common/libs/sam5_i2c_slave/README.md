# SAM5 I²C Slave

This project provides an interrupt based I²C slave that can be used with a SAM5 microprocesser from Microchip. This provides an interface that can be used by the Main Board to control this slave.

Have a look at the [wiki entry](https://gitlab.lrz.de/groups/cybertum_arm_electronics/-/wikis/How-Tos/Using-SAM5-I%C2%B2C-Slave) for a more detailed description.
