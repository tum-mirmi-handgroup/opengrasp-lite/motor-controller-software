#ifndef SAM5_I2C_SLAVE_H_
#define SAM5_I2C_SLAVE_H_

/********************************************************************************************************/
/*                                                                                                      */
/* If you want to use this I2C slave in your project please have a look at:                             */
/* https://gitlab.lrz.de/groups/cybertum_arm_electronics/-/wikis/How-Tos/How-To-Use-SAM5-I%C2%B2C-Slave */
/*                                                                                                      */
/********************************************************************************************************/

#include <atmel_start.h>
#include "simple_state_machine.h"
#include "hardware_defines.h"
#include "sam5_sw_crc8.h"

#define I2C_SLAVE_PROTOCOL_TYPE_EXAMPLE	1
#define I2C_SLAVE_PROTOCOL_TYPE_MOTOR	2
#define I2C_SLAVE_PROTOCOL_TYPE_EMG		3

#ifndef I2C_SLAVE_TYPE
#error "I2C Slave type not specified"
#endif

#if I2C_SLAVE_TYPE == I2C_SLAVE_PROTOCOL_TYPE_EXAMPLE
#define I2C_SLAVE_ADDRESS 8
#elif I2C_SLAVE_TYPE == I2C_SLAVE_PROTOCOL_TYPE_MOTOR
#define I2C_SLAVE_ADDRESS 49
#elif I2C_SLAVE_TYPE == I2C_SLAVE_PROTOCOL_TYPE_EMG
#define I2C_SLAVE_ADDRESS 50
#else
#error "I2C_SLAVE_TYPE unknown"
#endif


#ifndef I2C_SLAVE_SERCOM_UNIT
#error "Did not define: Selected SERCOM unit"
#endif

#ifndef I2C_SLAVE_SCL_PIN
#error "DID not define: The GPIO pin to which the SCL signal is connected"
#endif
#ifndef I2C_SLAVE_SCL_PIN_FUNCTION
#error "Did not define: Function value that connects the I2C_SLAVE_SCL_PIN to PAD1 of the selected SERCOM unit"
#endif
#ifndef I2C_SLAVE_SDA_PIN
#error "Did not define: GPIO pin to which the SDA signal is connected"
#endif
#ifndef I2C_SLAVE_SDA_PIN_FUNCTION
#error "Did not define: Function value that connects the I2C_SLAVE_SDA_PIN to PAD0 of the selected SERCOM unit"
#endif

#ifndef I2C_SLAVE_GLCK_CORE_GEN
#error "Did not define: The main clock that drives the I2C slave with I2C_SLAVE_GLCK_CORE_GEN"
#endif
#ifndef I2C_SLAVE_GLCK_SLOW_GEN
#error "Did not define: The 32.768 kHz slow clock that drives the I2C slave with I2C_SLAVE_GLCK_SLOW_GEN"
#endif

/**
* \brief Definition of the events that can be dispatched to the I2C slave state machine
*/
enum{
	I2C_SLAVE_EVT_SIG_START_READ = EVENT_BASE,      ///< The I2C slave received a start followed by a read request
	I2C_SLAVE_EVT_SIG_START_WRITE,                  ///< The I2C slave received a start followed by a write request
	I2C_SLAVE_EVT_SIG_REPEATED_START_READ,          ///< The I2C slave received a repeated start followed by a read request
	I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE,         ///< The I2C slave received a repeated start followed by a write request
	I2C_SLAVE_EVT_SIG_DATA_READY,                   ///< The I2C slave either received a data byte or has to send a data byte
	I2C_SLAVE_EVT_SIG_STOP,                         ///< The I2C slave received a stop
	I2C_SLAVE_EVT_SIG_ERROR                         ///< The I2C slave detected an error
};

enum {
	I2C_SLAVE_FIELD_WRITTEN_BY_MASTER = 0,
	I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE = 1,
};
typedef uint8_t i2c_slave_field_written_by_t;

/**
* \brief Structure describing an I2C command
*/
typedef struct
{
	volatile void *address;						///< Address in memory to which this command refers
	size_t size_in_byte;						///< Size in byte of the value
	i2c_slave_field_written_by_t written_by;	///< Indicating whether the slave or master write that value
} i2c_slave_command_t;

typedef struct
{
	const i2c_slave_command_t* slave_commands;
	uint8_t num_slave_commands;
	uint8_t type;
	uint8_t major_version_number;
	uint8_t minor_version_number;
} i2c_slave_protocol_t;

extern const i2c_slave_protocol_t g_i2c_slave_protocol;

/**
* \brief Initializes the I2C slave peripherals and state machine
*/
void sam5_i2c_slave_init();

#define I2C_SLAVE_PROTOCOL_ENTRY(DIRECTION, OBJECT)	\
{													\
	.address = &(OBJECT),							\
	.size_in_byte = sizeof(OBJECT),					\
	.written_by = DIRECTION							\
}

#endif /* SAM5_I2C_SLAVE_H_ */