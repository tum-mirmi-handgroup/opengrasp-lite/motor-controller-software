#include "sam5_i2c_slave_impl.h"
#include "sam5_sw_crc8.h"
#include "simple_state_machine.h"
#include <string.h>

volatile i2c_slave_t g_i2c_slave;

static const i2c_slave_command_t protocol_identification = {
	.address = NULL, // Can be NULL as this is never used with the normal sending state, only with the special command send
	.written_by = I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE,
	.size_in_byte = 3,
};

static const i2c_slave_command_t echo_receive = {
	.address = (void*)&g_i2c_slave.echo_buffer,
	.written_by = I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE,
	.size_in_byte = sizeof(g_i2c_slave.echo_buffer),
};

static const i2c_slave_command_t echo_send = {
	.address = (void*)&g_i2c_slave.echo_buffer,
	.written_by = I2C_SLAVE_FIELD_WRITTEN_BY_MASTER,
	.size_in_byte = sizeof(g_i2c_slave.echo_buffer),
};

static const i2c_slave_command_t num_commands = {
	.address = (void*)&g_i2c_slave_protocol.num_slave_commands,
	.written_by = I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE,
	.size_in_byte = sizeof(g_i2c_slave_protocol.num_slave_commands),
};

static uint8_t i2c_slave_initialize_peripherals(void)
{
	uint8_t initialization_error = 0;
	
	for(uint8_t i = 0; i < g_i2c_slave_protocol.num_slave_commands; ++i){
		ASSERT(g_i2c_slave_protocol.slave_commands[i].size_in_byte <= SAM5_CRC8_MAX_DATA_LENGTH);
	}
	
	///* Initialize pins
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the SCL pin
	gpio_set_pin_pull_mode(I2C_SLAVE_SCL_PIN, GPIO_PULL_OFF);
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the SDA pin
	gpio_set_pin_pull_mode(I2C_SLAVE_SDA_PIN, GPIO_PULL_OFF);
	///* Set function of I2C_SLAVE_SCL_PIN to I2C_SLAVE_SCL_PIN_FUNCTION
	gpio_set_pin_function(I2C_SLAVE_SCL_PIN, I2C_SLAVE_SCL_PIN_FUNCTION);
	///* Set function of I2C_SLAVE_SDA_PIN to I2C_SLAVE_SDA_PIN_FUNCTION
	gpio_set_pin_function(I2C_SLAVE_SDA_PIN, I2C_SLAVE_SDA_PIN_FUNCTION);
	
	I2C_SLAVE_APB_MCLK_MASK_SETTER(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, I2C_SLAVE_SERCOM_GCLK_ID_CORE, I2C_SLAVE_GLCK_CORE_GEN | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_gclk_write_PCHCTRL_reg(GCLK, I2C_SLAVE_SERCOM_GCLK_ID_SLOW, I2C_SLAVE_GLCK_SLOW_GEN | (1 << GCLK_PCHCTRL_CHEN_Pos));
	///* Check if no software reset of I2C_SLAVE_SERCOM is in progress
	if (!hri_sercomi2cs_is_syncing(I2C_SLAVE_SERCOM, SERCOM_I2CS_SYNCBUSY_SWRST))
	{
		///* Perform a software reset of I2C_SLAVE_SERCOM
		hri_sercomi2cs_write_CTRLA_reg(I2C_SLAVE_SERCOM, SERCOM_I2CS_CTRLA_SWRST);
	}
	///* Wait until the software reset is finished
	hri_sercomi2cs_wait_for_sync(I2C_SLAVE_SERCOM, SERCOM_I2CS_SYNCBUSY_SWRST);
	
	hri_sercomi2cs_write_CTRLA_reg(I2C_SLAVE_SERCOM,
	0 << SERCOM_I2CS_CTRLA_LOWTOUTEN_Pos    /* Time-out disabled */
	| 0 << SERCOM_I2CS_CTRLA_SCLSM_Pos      /* SCL stretch before ACK bit for software interaction */
	| 0x0 << SERCOM_I2CS_CTRLA_SPEED_Pos    /* Fast-mode (Fm) up to 400 kHz */
	| 0 << SERCOM_I2CS_CTRLA_SEXTTOEN_Pos   /* Cumulative time-out disabled */
	| 0x0 << SERCOM_I2CS_CTRLA_SDAHOLD_Pos  /* SDA hold time disabled */
	| 0 << SERCOM_I2CS_CTRLA_PINOUT_Pos     /* 4-wire operation disabled */
	| 0 << SERCOM_I2CS_CTRLA_RUNSTDBY_Pos   /* Run in standby disabled, all reception is dropped */
	| 0x04 << SERCOM_I2CS_CTRLA_MODE_Pos    /* Set SERCOM mode to I2C slave */
	| 0 );
	hri_sercomi2cs_write_CTRLB_reg(I2C_SLAVE_SERCOM,
	0x0 << SERCOM_I2CS_CTRLB_AMODE_Pos      /* The slave responds to the address written in ADDR.ADDR masked by the value in ADDR.ADDRMASK. */
	| 0 << SERCOM_I2CS_CTRLB_AACKEN_Pos     /* Address is not automatically acknowledged if there is an address match */
	| 0 << SERCOM_I2CS_CTRLB_GCMD_Pos       /* PMBus group command is disabled */
	| 0 << SERCOM_I2CS_CTRLB_SMEN_Pos       /* Smart mode is disabled, data is not acknowledged automatically when DATA.DATA is read */
	| 0 );
	hri_sercomi2cs_write_CTRLC_reg(I2C_SLAVE_SERCOM,
	0 << SERCOM_I2CS_CTRLC_DATA32B_Pos      /* Data transaction to/from DATA are 8-bit in size */
	| 0 );
	hri_sercomi2cs_write_INTEN_reg(I2C_SLAVE_SERCOM,
	1 << SERCOM_I2CS_INTENSET_ERROR_Pos     /* Error interrupt is enabled. */
	| 1 << SERCOM_I2CS_INTENSET_DRDY_Pos    /* The Data Ready interrupt is enabled. */
	| 1 << SERCOM_I2CS_INTENSET_AMATCH_Pos  /* The Address Match interrupt is enabled. */
	| 1 << SERCOM_I2CS_INTENSET_PREC_Pos    /* The Stop Received interrupt is enabled. */
	| 0 );
	
	#ifdef NVIC_PRIORITY_I2C
	NVIC_SetPriority(I2C_SLAVE_PREC_IRQn, NVIC_PRIORITY_I2C);
	NVIC_SetPriority(I2C_SLAVE_AMATCH_IRQn, NVIC_PRIORITY_I2C);
	NVIC_SetPriority(I2C_SLAVE_DRDY_IRQn, NVIC_PRIORITY_I2C);
	NVIC_SetPriority(I2C_SLAVE_ERROR_IRQn, NVIC_PRIORITY_I2C);
	#else
	NVIC_SetPriority(I2C_SLAVE_PREC_IRQn, 0);
	NVIC_SetPriority(I2C_SLAVE_AMATCH_IRQn, 0);
	NVIC_SetPriority(I2C_SLAVE_DRDY_IRQn, 0);
	NVIC_SetPriority(I2C_SLAVE_ERROR_IRQn, 0);
	#endif
	
	return initialization_error;
}

static void i2c_slave_set_address(const uint8_t address)
{
	hri_sercomi2cs_write_ADDR_ADDRMASK_bf(I2C_SLAVE_SERCOM, 0x00);
	hri_sercomi2cs_write_ADDR_ADDR_bf(I2C_SLAVE_SERCOM, address);
}

static void i2c_slave_enable(void)
{
	///* Enable PREC interrupt
	NVIC_DisableIRQ(I2C_SLAVE_PREC_IRQn);
	NVIC_ClearPendingIRQ(I2C_SLAVE_PREC_IRQn);
	NVIC_EnableIRQ(I2C_SLAVE_PREC_IRQn);
	///* Enable AMATCH interrupt
	NVIC_DisableIRQ(I2C_SLAVE_AMATCH_IRQn);
	NVIC_ClearPendingIRQ(I2C_SLAVE_AMATCH_IRQn);
	NVIC_EnableIRQ(I2C_SLAVE_AMATCH_IRQn);
	///* Enable DRDY interrupt
	NVIC_DisableIRQ(I2C_SLAVE_DRDY_IRQn);
	NVIC_ClearPendingIRQ(I2C_SLAVE_DRDY_IRQn);
	NVIC_EnableIRQ(I2C_SLAVE_DRDY_IRQn);
	///* Enable ERROR interrupt
	NVIC_DisableIRQ(I2C_SLAVE_ERROR_IRQn);
	NVIC_ClearPendingIRQ(I2C_SLAVE_ERROR_IRQn);
	NVIC_EnableIRQ(I2C_SLAVE_ERROR_IRQn);
	
	hri_sercomi2cs_set_CTRLA_ENABLE_bit(I2C_SLAVE_SERCOM);
}

static const i2c_slave_command_t * i2c_slave_parse_command_code(uint8_t command_code)
{
	const i2c_slave_command_t * command = NULL;
	if (command_code < g_i2c_slave_protocol.num_slave_commands)
	{
		command = &g_i2c_slave_protocol.slave_commands[command_code];
	}
	return command;
}

void I2C_SLAVE_PREC_HANDLER(void)
{
	simple_state_machine_event_t this_event =
	{
		.signal_ = I2C_SLAVE_EVT_SIG_STOP
	};
	simpleStateMachineDispatch_(&g_i2c_slave.state_machine, &this_event, NULL);
	i2c_slave_execute_finish_action(SERCOM_I2CS_INTFLAG_PREC);
}

void I2C_SLAVE_AMATCH_HANDLER(void)
{
	simple_state_machine_event_t this_event;
	
	const uint16_t errors_from_status =hri_sercomi2cs_get_STATUS_reg(I2C_SLAVE_SERCOM,
	SERCOM_I2CS_STATUS_LENERR
	| SERCOM_I2CS_STATUS_SEXTTOUT
	| SERCOM_I2CS_STATUS_LOWTOUT
	| SERCOM_I2CS_STATUS_BUSERR
	| SERCOM_I2CS_STATUS_COLL
	);
	if(errors_from_status == 0){
		if(hri_sercomi2cs_get_STATUS_SR_bit(I2C_SLAVE_SERCOM))
		{
			if(hri_sercomi2cs_get_STATUS_DIR_bit(I2C_SLAVE_SERCOM))
			{
				this_event.signal_ = I2C_SLAVE_EVT_SIG_REPEATED_START_READ;
			}
			else
			{
				this_event.signal_ = I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE;
			}
		}
		else
		{
			if(hri_sercomi2cs_get_STATUS_DIR_bit(I2C_SLAVE_SERCOM))
			{
				this_event.signal_ = I2C_SLAVE_EVT_SIG_START_READ;
			}
			else
			{
				this_event.signal_ = I2C_SLAVE_EVT_SIG_START_WRITE;
			}
		}
	}
	else{
		hri_sercomi2cs_clear_STATUS_reg(I2C_SLAVE_SERCOM, errors_from_status);
		return;
	}
	simpleStateMachineDispatch_(&g_i2c_slave.state_machine, &this_event, NULL);
	i2c_slave_execute_finish_action(SERCOM_I2CS_INTFLAG_AMATCH);
}

void I2C_SLAVE_DRDY_HANDLER(void)
{
	simple_state_machine_event_t this_event =
	{
		.signal_ = I2C_SLAVE_EVT_SIG_DATA_READY
	};
	simpleStateMachineDispatch_(&g_i2c_slave.state_machine, &this_event, NULL);
	i2c_slave_execute_finish_action(SERCOM_I2CS_INTFLAG_DRDY);
}

void I2C_SLAVE_ERROR_HANDLER(void)
{
	simple_state_machine_event_t this_event =
	{
		.signal_ = I2C_SLAVE_EVT_SIG_ERROR
	};
	simpleStateMachineDispatch_(&g_i2c_slave.state_machine, &this_event, NULL);
	i2c_slave_execute_finish_action(SERCOM_I2CS_INTFLAG_ERROR);
}

static void i2c_slave_finish_action_command(uint8_t command){
	hri_sercomi2cs_write_CTRLB_CMD_bf(I2C_SLAVE_SERCOM, command);
}

static void i2c_slave_finish_action_command_ack(uint8_t command){
	hri_sercomi2cs_clear_CTRLB_ACKACT_bit(I2C_SLAVE_SERCOM);
	hri_sercomi2cs_write_CTRLB_CMD_bf(I2C_SLAVE_SERCOM, command);
}

static void i2c_slave_finish_action_command_nack(uint8_t command){
	hri_sercomi2cs_set_CTRLB_ACKACT_bit(I2C_SLAVE_SERCOM);
	hri_sercomi2cs_write_CTRLB_CMD_bf(I2C_SLAVE_SERCOM, command);
}

static void i2c_slave_finish_action_data(uint8_t data){
	hri_sercomi2cs_write_DATA_DATA_bf(I2C_SLAVE_SERCOM, data);
}

static void i2c_slave_finish_action_change_address(uint8_t new_address){
	hri_sercomi2cs_clear_CTRLB_ACKACT_bit(I2C_SLAVE_SERCOM);
	hri_sercomi2cs_write_CTRLB_CMD_bf(I2C_SLAVE_SERCOM, 0x2);
	delay_us(1); // Make sure the ACK bit has been sent
	hri_sercomi2cs_clear_CTRLA_ENABLE_bit(I2C_SLAVE_SERCOM);
	hri_sercomi2cs_wait_for_sync(I2C_SLAVE_SERCOM, SERCOM_I2CS_SYNCBUSY_ENABLE);
	i2c_slave_set_address(new_address);
	hri_sercomi2cs_set_CTRLA_ENABLE_bit(I2C_SLAVE_SERCOM);
	hri_sercomi2cs_wait_for_sync(I2C_SLAVE_SERCOM, SERCOM_I2CS_SYNCBUSY_ENABLE);
}

static void i2c_slave_register_finish_action(void(*volatile finish_action)(uint8_t data), uint8_t data){
	g_i2c_slave.finish_action = finish_action;
	g_i2c_slave.finish_data = data;
}

static void i2c_slave_execute_finish_action(uint8_t default_flag){
	if(g_i2c_slave.finish_action == 0){
		((Sercom *)I2C_SLAVE_SERCOM)->I2CS.INTFLAG.reg = default_flag;
	}
	else{
		(*g_i2c_slave.finish_action)(g_i2c_slave.finish_data);
		g_i2c_slave.finish_action = 0;
		g_i2c_slave.finish_data = 0;
	}
}

static void i2c_slave_state_init(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			i2c_slave_initialize_peripherals();
			i2c_slave_set_address(I2C_SLAVE_ADDRESS);
			i2c_slave_enable();
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		default:
		{
			break;
		}
	}
}

static void i2c_slave_state_idle(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			g_i2c_slave.p_command = NULL;
			g_i2c_slave.command = 0;
			g_i2c_slave.processed_bytes = 0;
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_SLAVE_EVT_SIG_START_WRITE:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
			simpleStateMachineTran_(fsm, i2c_slave_state_addressed, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x3);
			break;
		}
		case I2C_SLAVE_EVT_SIG_DATA_READY:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
			break;
		}
		case I2C_SLAVE_EVT_SIG_STOP:
		case I2C_SLAVE_EVT_SIG_ERROR:
		default:
		break;
	}
}

static void i2c_slave_state_addressed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_SLAVE_EVT_SIG_DATA_READY:
		{
			const uint8_t command = hri_sercomi2cs_read_DATA_DATA_bf(I2C_SLAVE_SERCOM);
			if(command > 240){
				g_i2c_slave.command = command;
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x2);
				// Use the void pointer as a by value parameter to forward the command number
				simpleStateMachineTran_(fsm, i2c_slave_state_special_commands, NULL);
				break;
			}
			const i2c_slave_command_t *this_command = i2c_slave_parse_command_code(command);
			if(this_command)
			{
				g_i2c_slave.p_command = this_command;
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x2);
				simpleStateMachineTran_(fsm, i2c_slave_state_command_parsed, NULL);
				break;
			}
			else
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_START_READ:
		case I2C_SLAVE_EVT_SIG_START_WRITE:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_STOP:
		case I2C_SLAVE_EVT_SIG_ERROR:
		default:
		{
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
	}
}

static void i2c_slave_state_command_parsed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_SLAVE_EVT_SIG_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_READ:
		{
			if(g_i2c_slave.p_command->written_by == I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE)
			{
				CRITICAL_SECTION_ENTER();
				memcpy((void *)g_i2c_slave.transfer_buffer, (const void *)g_i2c_slave.p_command->address, g_i2c_slave.p_command->size_in_byte);
				CRITICAL_SECTION_LEAVE();
				g_i2c_slave.transfer_buffer[g_i2c_slave.p_command->size_in_byte] = sam5_sw_crc8_CRC(g_i2c_slave.transfer_buffer, g_i2c_slave.p_command->size_in_byte);
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_slave_state_sending, NULL);
				break;
			}
			else
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_START_WRITE:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE:
		{
			if(g_i2c_slave.p_command->written_by == I2C_SLAVE_FIELD_WRITTEN_BY_MASTER)
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_slave_state_receiving, NULL);
				break;
			}
			else
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_DATA_READY:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_STOP:
		case I2C_SLAVE_EVT_SIG_ERROR:
		default:
		{
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
	}
}

static void i2c_slave_state_sending(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_SLAVE_EVT_SIG_DATA_READY:
		{
			if(hri_sercomi2cs_get_STATUS_RXNACK_bit(I2C_SLAVE_SERCOM) || g_i2c_slave.processed_bytes > g_i2c_slave.p_command->size_in_byte)
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command, 0x2);
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
			else
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_data, g_i2c_slave.transfer_buffer[g_i2c_slave.processed_bytes++]);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_START_WRITE:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
			simpleStateMachineTran_(fsm, i2c_slave_state_addressed, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_READ:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_STOP:
		case I2C_SLAVE_EVT_SIG_ERROR:
		default:
		{
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
	}
}

static void i2c_slave_state_receiving(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_SLAVE_EVT_SIG_DATA_READY:
		{
			if(g_i2c_slave.processed_bytes < g_i2c_slave.p_command->size_in_byte)
			{
				g_i2c_slave.transfer_buffer[g_i2c_slave.processed_bytes++] = hri_sercomi2cs_read_DATA_DATA_bf(I2C_SLAVE_SERCOM);
				// Received part of the message -> Send ACK and wait for next byte
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				break;
			}
			else
			{
				uint8_t crc = hri_sercomi2cs_read_DATA_DATA_bf(I2C_SLAVE_SERCOM);
				crc = sam5_sw_crc8_Applied(g_i2c_slave.transfer_buffer, g_i2c_slave.p_command->size_in_byte, crc);
				if(crc != 0){
					// CRC was right
					CRITICAL_SECTION_ENTER();
					memcpy((void *)g_i2c_slave.p_command->address, (const void *)g_i2c_slave.transfer_buffer, g_i2c_slave.p_command->size_in_byte);
					CRITICAL_SECTION_LEAVE();
					i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x2);
				}
				else{
					// CRC was wrong
					i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
				}
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_START_WRITE:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
			simpleStateMachineTran_(fsm, i2c_slave_state_addressed, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_READ:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_STOP:
		case I2C_SLAVE_EVT_SIG_ERROR:
		default:
		{
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
	}
}

static void i2c_slave_state_special_commands(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	const uint8_t command = g_i2c_slave.command;
	switch(event->signal_)
	{
		
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_SLAVE_EVT_SIG_START_WRITE:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE:
		{
			if (command == 252)
			{
				g_i2c_slave.p_command = &echo_receive;
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_slave_state_receiving, NULL);
				break;
			}
			else if(command == 255)
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_slave_state_change_address, NULL);
				break;
			}
			else
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_READ:
		{
			if (command == 251)
			{
				g_i2c_slave.p_command = &protocol_identification;
				g_i2c_slave.transfer_buffer[0] = g_i2c_slave_protocol.type;
				g_i2c_slave.transfer_buffer[1] = g_i2c_slave_protocol.major_version_number;
				g_i2c_slave.transfer_buffer[2] = g_i2c_slave_protocol.minor_version_number;
				g_i2c_slave.transfer_buffer[g_i2c_slave.p_command->size_in_byte] = sam5_sw_crc8_CRC(g_i2c_slave.transfer_buffer, g_i2c_slave.p_command->size_in_byte);
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_slave_state_sending, NULL);
				break;
			}
			else if (command == 253)
			{
				g_i2c_slave.p_command = &echo_send;
				memcpy((void *)g_i2c_slave.transfer_buffer, (const void *)g_i2c_slave.p_command->address, g_i2c_slave.p_command->size_in_byte);
				g_i2c_slave.transfer_buffer[g_i2c_slave.p_command->size_in_byte] = sam5_sw_crc8_CRC(g_i2c_slave.transfer_buffer, g_i2c_slave.p_command->size_in_byte);
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_slave_state_sending, NULL);
				break;
			}
			else if(command == 254)
			{
				g_i2c_slave.p_command = &num_commands;
				memcpy((void *)g_i2c_slave.transfer_buffer, (const void *)g_i2c_slave.p_command->address, g_i2c_slave.p_command->size_in_byte);
				g_i2c_slave.transfer_buffer[g_i2c_slave.p_command->size_in_byte] = sam5_sw_crc8_CRC(g_i2c_slave.transfer_buffer, g_i2c_slave.p_command->size_in_byte);
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_slave_state_sending, NULL);
				break;
			}
			else
			{
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_DATA_READY:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_STOP:
		case I2C_SLAVE_EVT_SIG_ERROR:
		default:
		{
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
	}
}

static void i2c_slave_state_change_address(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_SLAVE_EVT_SIG_DATA_READY:
		{
			if(g_i2c_slave.processed_bytes == 0)
			{
				g_i2c_slave.transfer_buffer[g_i2c_slave.processed_bytes++] = hri_sercomi2cs_read_DATA_DATA_bf(I2C_SLAVE_SERCOM);
				// Received address -> Send ACK and wait for next byte
				i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
				break;
			}
			else
			{
				uint8_t crc = hri_sercomi2cs_read_DATA_DATA_bf(I2C_SLAVE_SERCOM);
				crc = sam5_sw_crc8_Applied(g_i2c_slave.transfer_buffer, 1, crc);
				if(crc != 0){
					// CRC was right
					i2c_slave_register_finish_action(&i2c_slave_finish_action_change_address, g_i2c_slave.transfer_buffer[0]);
				}
				else{
					// CRC was wrong
					i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
				}
				simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
				break;
			}
		}
		case I2C_SLAVE_EVT_SIG_START_WRITE:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_WRITE:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_ack, 0x3);
			simpleStateMachineTran_(fsm, i2c_slave_state_addressed, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_START_READ:
		case I2C_SLAVE_EVT_SIG_REPEATED_START_READ:
		{
			i2c_slave_register_finish_action(&i2c_slave_finish_action_command_nack, 0x2);
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
		case I2C_SLAVE_EVT_SIG_STOP:
		case I2C_SLAVE_EVT_SIG_ERROR:
		default:
		{
			simpleStateMachineTran_(fsm, i2c_slave_state_idle, NULL);
			break;
		}
	}
}
void sam5_i2c_slave_init()
{
	simpleStateMachineInit_(&g_i2c_slave.state_machine, i2c_slave_state_init, NULL);
}
