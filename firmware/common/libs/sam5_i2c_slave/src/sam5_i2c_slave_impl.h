#ifndef SAM5_I2C_SLAVE_IMPL_H_
#define SAM5_I2C_SLAVE_IMPL_H_


#include "sam5_i2c_slave.h"
#include "sam5_sw_crc8.h"

#define I2C_SLAVE_INSERT_SERCOM_UNIT_(pre, unit, post)	pre ## unit ## post									///< This is necessary for C preprocessor macro expansion
#define I2C_SLAVE_INSERT_SERCOM_UNIT(pre, unit, post)	I2C_SLAVE_INSERT_SERCOM_UNIT_(pre, unit, post)		///< This is necessary for C preprocessor macro expansion

/**
* Some macros to generate "#define I2C_SLAVE_SERCOM SERCOMn" out of "#define I2C_SLAVE_SERCOM_UNIT n"
*/
#define I2C_SLAVE_SERCOM I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT,)

/**
* Some macros to generate "#define I2C_SLAVE_SERCOM hri_mclk_set_APBXMASK_SERCOMn_bit" out of "#define I2C_SLAVE_SERCOM_UNIT n"
*/
#if I2C_SLAVE_SERCOM_UNIT == 0 || I2C_SLAVE_SERCOM_UNIT == 1
#define I2C_SLAVE_APB_MCLK_MASK_SETTER I2C_SLAVE_INSERT_SERCOM_UNIT(hri_mclk_set_APBAMASK_SERCOM, I2C_SLAVE_SERCOM_UNIT, _bit)		///< SERCOM Unit on APBA Bus
#elif I2C_SLAVE_SERCOM_UNIT == 2 || I2C_SLAVE_SERCOM_UNIT == 3
#define I2C_SLAVE_APB_MCLK_MASK_SETTER I2C_SLAVE_INSERT_SERCOM_UNIT(hri_mclk_set_APBBMASK_SERCOM, I2C_SLAVE_SERCOM_UNIT, _bit)		///< SERCOM Unit on APBB Bus
#elif I2C_SLAVE_SERCOM_UNIT == 4 || I2C_SLAVE_SERCOM_UNIT == 5 || I2C_SLAVE_SERCOM_UNIT == 6 || I2C_SLAVE_SERCOM_UNIT == 7
#define I2C_SLAVE_APB_MCLK_MASK_SETTER I2C_SLAVE_INSERT_SERCOM_UNIT(hri_mclk_set_APBDMASK_SERCOM, I2C_SLAVE_SERCOM_UNIT, _bit)		///< SERCOM Unit on APBD Bus
#else
#error "Unknown I2C_SLAVE_SERCOM_UNIT, it is not within [0..7]"
#endif

/**
* Some macros to generate more information out of "#define I2C_SLAVE_SERCOM_UNIT n"
*/
#define I2C_SLAVE_SERCOM_GCLK_ID_CORE I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT,_GCLK_ID_CORE)
#define I2C_SLAVE_SERCOM_GCLK_ID_SLOW I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT,_GCLK_ID_SLOW)

/**
* Some macros to generate more information out of "#define I2C_SLAVE_SERCOM_UNIT n"
*/
#define I2C_SLAVE_SERCOM_GCLK_ID_CORE I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT,_GCLK_ID_CORE)

#define I2C_SLAVE_PREC_HANDLER			I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _0_Handler)
#define I2C_SLAVE_PREC_IRQn				I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _0_IRQn)
#define I2C_SLAVE_AMATCH_HANDLER		I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _1_Handler)
#define I2C_SLAVE_AMATCH_IRQn			I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _1_IRQn)
#define I2C_SLAVE_DRDY_HANDLER			I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _2_Handler)
#define I2C_SLAVE_DRDY_IRQn				I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _2_IRQn)
#define I2C_SLAVE_ERROR_HANDLER			I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _3_Handler)
#define I2C_SLAVE_ERROR_IRQn			I2C_SLAVE_INSERT_SERCOM_UNIT(SERCOM, I2C_SLAVE_SERCOM_UNIT, _3_IRQn)

/**
* \brief Initializes the peripherals needed by the i2c slave
*/
static uint8_t i2c_slave_initialize_peripherals(void);

/**
* \brief Sets the address the i2c slave responds to
*/
static void i2c_slave_set_address(uint8_t address);

/**
* \brief Checks if the command code is valid
*
* \param[in] command_code The code that is parsed
* \return  Pointer to the corresponding command or NULL if the command code is invalid
*/
static const i2c_slave_command_t * i2c_slave_parse_command_code(uint8_t command_code);

/**
* \brief Init state of the I2C slave state machine
*/
static void i2c_slave_state_init(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Idle state of the I2C slave state machine
*/
static void i2c_slave_state_idle(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Addressed state of the I2C slave state machine
*/
static void i2c_slave_state_addressed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Addressed state of the I2C slave state machine
*/
static void i2c_slave_state_command_parsed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Sending state of the I2C slave state machine
*/
static void i2c_slave_state_sending(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Receiving state of the I2C slave state machine
*/
static void i2c_slave_state_receiving(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Similar to the command parsed state, but this will handle special commands like changing the address
*/
static void i2c_slave_state_special_commands(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Similar to the receiving state, but after reception of 2 bytes (new address + crc) the I2C Slave will restart and change it's address
*/
static void i2c_slave_state_change_address(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);


static void i2c_slave_finish_action_command(uint8_t command);

static void i2c_slave_finish_action_command_ack(uint8_t command);

static void i2c_slave_finish_action_command_nack(uint8_t command);

static void i2c_slave_finish_action_data(uint8_t data);

static void i2c_slave_register_finish_action(void(*volatile finish_action)(uint8_t data), uint8_t data);

static void i2c_slave_execute_finish_action(uint8_t default_flag);

/**
* \brief Structure describing an I2C slave
*/
typedef struct
{
	volatile simple_state_machine_t state_machine;						///< State machine for controlling the slave
	volatile const i2c_slave_command_t *p_command;						///< Pointer to the current command that is processed
	volatile uint8_t command;											///< Number of the current command that is processed
	volatile uint8_t transfer_buffer[SAM5_CRC8_MAX_DATA_LENGTH + 1];	///< Transfer buffer of the slave (Max message size + 1 byte CRC)
	volatile uint8_t processed_bytes;									///< Number of bytes that are processed
	void(*volatile finish_action)(uint8_t data);						///< The action that will be called to exit the interrupt
	volatile uint8_t finish_data;
	volatile uint8_t echo_buffer;										///< Buffer to store a single byte to allow echoing those for connection testing
} i2c_slave_t;

extern volatile i2c_slave_t g_i2c_slave;
#endif