/**
* \brief Array containing the accessible values
*
* The index of each entry is the command itself. E.g. the command for the control mode of motor 1 is 0.
* This array can be extended or reduced as needed.
*/

#include "sam5_i2c_slave.h"

uint32_t buffer;
static const i2c_slave_command_t example_protocol_slave_commands[] =
{
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, buffer),	// 00 - buffer written by master
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, buffer),		// 01 - buffer written by slave
};

const i2c_slave_protocol_t g_i2c_slave_protocol = {
	.slave_commands = example_protocol_slave_commands,
	.num_slave_commands = sizeof(example_protocol_slave_commands) / sizeof(i2c_slave_command_t),
	.type = I2C_SLAVE_PROTOCOL_TYPE_EXAMPLE,
	.major_version_number = 1,
	.minor_version_number = 0,
};
