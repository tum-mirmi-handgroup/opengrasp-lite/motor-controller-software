/**
* \brief Array containing the accessible values
*
* The index of each entry is the command itself. E.g. the command for the control mode of motor 1 is 0.
* This array can be extended or reduced as needed.
*/

#include "sam5_i2c_slave.h"
#include "sam5_dc_motor_controller.h"
#include "control_flags.h"

static uint8_t spare = 42;

#ifndef M1_PRESENT
dc_motor_controller_t g_motor_controller_1;
#endif // !M1_PRESENT

#ifndef M2_PRESENT
dc_motor_controller_t g_motor_controller_2;
#endif // !M2_PRESENT

#ifndef M3_PRESENT
dc_motor_controller_t g_motor_controller_3;
#endif // !M3_PRESENT

static const i2c_slave_command_t motor_protocol_slave_commands[] =
{
	//##################################################################################
	// General Commands ----------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, control_flags),														// 00 - Control Flags
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, combined_state),														// 01 - Read shortened state of all 3 motors
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, combined_state_flags),												// 02 - Read state flags_a of all 3 motors
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, supply_voltage),													// 03 - Write Supply Voltage
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 04 - Spare
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 05 - Spare
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 06 - Spare
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 07 - Spare
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 08 - Spare
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 09 - Spare
	
	//##################################################################################
	// 1 - Inputs ----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.inputs_com.flags),								// 10 - Write input flags
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.inputs_com.angle),								// 11 - Write angle set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.inputs_com.velocity),							// 12 - Write velocity set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.inputs_com.current),							// 13 - Write current set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.inputs_com.voltage),							// 14 - Write voltage set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.inputs_com),									// 15 - Write whole Input
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.inputs_com),									// 16 - Read whole input
	// 1 - State -----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.state),											// 17 - Read whole State
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.state.flags_a),									// 18 - Read status flags A
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.state.flags_b),									// 19 - Read status flags B
	// 1 - Motor information  ----------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.limits),										// 20 - Write limits
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.limits),										// 21 - Read Limits
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.calib_res),										// 22 - Read Calibration results
	// 1 - Controller Settings ---------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.control.current_controller.settings_com),		// 23 - Write current controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.control.velocity_controller.settings_com),		// 24 - Write velocity controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_1.control.position_controller.settings_com),		// 25 - Write position controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.control.current_controller.settings_com),		// 26 - Read current controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.control.velocity_controller.settings_com),		// 27 - Read velocity controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_1.control.position_controller.settings_com),		// 28 - Read position controller
	// 1 - Spare -----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 29 - Spare
	
	//##################################################################################
	// 2 - Inputs ----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.inputs_com.flags),								// 30 - Write input flagse
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.inputs_com.angle),								// 31 - Write angle set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.inputs_com.velocity),							// 32 - Write velocity set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.inputs_com.current),							// 33 - Write current set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.inputs_com.voltage),							// 34 - Write voltage set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.inputs_com),									// 35 - Write whole Input
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.inputs_com),									// 36 - Read whole input
	// 2 - State -----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.state),											// 37 - Read whole State
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.state.flags_a),									// 38 - Read status flags A
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.state.flags_b),									// 39 - Read status flags B
	// 2 - Motor information  ----------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.limits),										// 40 - Write limits
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.limits),										// 41 - Read Limits
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.calib_res),										// 42 - Read Calibration results
	// 2 - Controller Settings ---------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.control.current_controller.settings_com),		// 43 - Write current controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.control.velocity_controller.settings_com),		// 44 - Write velocity controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_2.control.position_controller.settings_com),		// 45 - Write position controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.control.current_controller.settings_com),		// 46 - Read current controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.control.velocity_controller.settings_com),		// 47 - Read velocity controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_2.control.position_controller.settings_com),		// 48 - Read position controller
	// 2 - Spare -----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 49 - Spare
	
	//##################################################################################
	// 3 - Inputs ----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.inputs_com.flags),								// 50 - Write input flagse
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.inputs_com.angle),								// 51 - Write angle set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.inputs_com.velocity),							// 52 - Write velocity set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.inputs_com.current),							// 53 - Write current set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.inputs_com.voltage),							// 54 - Write voltage set point
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.inputs_com),									// 55 - Write whole Input
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.inputs_com),									// 56 - Read whole input
	// 3 - State -----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.state),											// 57 - Read whole State
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.state.flags_a),									// 58 - Read status flags A
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.state.flags_b),									// 59 - Read status flags B
	// 3 - Motor information  ----------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.limits),										// 60 - Write limits
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.limits),										// 61 - Read Limits
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.calib_res),										// 62 - Read Calibration results
	// 3 - Controller Settings ---------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.control.current_controller.settings_com),		// 63 - Write current controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.control.velocity_controller.settings_com),		// 64 - Write velocity controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, g_motor_controller_3.control.position_controller.settings_com),		// 65 - Write position controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.control.current_controller.settings_com),		// 66 - Read current controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.control.velocity_controller.settings_com),		// 67 - Read velocity controller
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, g_motor_controller_3.control.position_controller.settings_com),		// 68 - Read position controller
	// 3 - Spare -----------------------------------------------------------------------
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, spare),																// 69 - Spare
};

const i2c_slave_protocol_t g_i2c_slave_protocol = {
	.slave_commands = motor_protocol_slave_commands,
	.num_slave_commands = sizeof(motor_protocol_slave_commands) / sizeof(i2c_slave_command_t),
	.type = I2C_SLAVE_PROTOCOL_TYPE_MOTOR,
	.major_version_number = 3,
	.minor_version_number = 2,
};

_Static_assert(!((sizeof(motor_protocol_slave_commands) / sizeof(i2c_slave_command_t)) > 70), "Unexpected amount of commands in the motorcontroller protocol - Check for duplicates - If everything is correct, adjust this assert");
_Static_assert(!((sizeof(motor_protocol_slave_commands) / sizeof(i2c_slave_command_t)) < 70), "Unexpected amount of commands in the motorcontroller protocol - Check for missing entries - If everything is correct, adjust this assert");

_Static_assert(sizeof(control_flags) < SAM5_CRC8_MAX_DATA_LENGTH, "Control flags are too long");
_Static_assert(sizeof(combined_state) < SAM5_CRC8_MAX_DATA_LENGTH, "Combined state is too long");
_Static_assert(sizeof(combined_state_flags) < SAM5_CRC8_MAX_DATA_LENGTH, "Combined flags is too long");
_Static_assert(sizeof(supply_voltage) < SAM5_CRC8_MAX_DATA_LENGTH, "Supply voltage is too long");
_Static_assert(sizeof(dc_motor_controller_inputs_t) < SAM5_CRC8_MAX_DATA_LENGTH, "Inputs are too long");
_Static_assert(sizeof(dc_motor_controller_state_t) < SAM5_CRC8_MAX_DATA_LENGTH, "State is too long");
_Static_assert(sizeof(dc_motor_controller_limits_t) < SAM5_CRC8_MAX_DATA_LENGTH, "Limits are too long");
_Static_assert(sizeof(dc_motor_controller_calibration_result_t) < SAM5_CRC8_MAX_DATA_LENGTH, "Calibration results are too long");
_Static_assert(sizeof(sam5_control_settings_t) < SAM5_CRC8_MAX_DATA_LENGTH, "Control settings are too long");