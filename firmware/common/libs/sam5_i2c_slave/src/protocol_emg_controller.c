/*
* protocol_emg_controller.c
*
* Created: 08.12.2022 11:10:32
*  Author: Benjamin Ernhofer
*/
/**
* \brief Array containing the accessible values
*
* The index of each entry is the command itself. E.g. the command for the control mode of motor 1 is 0.
* This array can be extended or reduced as needed.
*/
#include "sam5_i2c_slave.h"


static uint32_t testData = 0; // not needed atm

//i2c output for main controller
extern uint8_t tx_slave_buf[2];

/* EMG Protocol 09.12.2022
* tx_slave_buf[0] bit0		= locked ? 1 if locked											data length: 1 bit
* tx_slave_buf[0] bit1:255	= states (grasp+direction), currently only [1,8] used			data length: 7 bit
*
* tx_slave_buf[1] bit0:2	= 8 levels of muscle contraction								data length: 3 bit
*/

static const i2c_slave_command_t emg_protocol_slave_commands[] =
{
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_MASTER, testData),		// 00
	I2C_SLAVE_PROTOCOL_ENTRY(I2C_SLAVE_FIELD_WRITTEN_BY_SLAVE, tx_slave_buf),	// 01
};

const i2c_slave_protocol_t g_i2c_slave_protocol = {
	.slave_commands = emg_protocol_slave_commands,
	.num_slave_commands = sizeof(emg_protocol_slave_commands) / sizeof(i2c_slave_command_t),
	.type = I2C_SLAVE_PROTOCOL_TYPE_EMG,
	.major_version_number = 1,
	.minor_version_number = 0,
};
