/**
* \file main.c
*
* Copyright (C) 2022  Michael Ratzel
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <atmel_start.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "sam5_dc_motor_controller.h"
#include "sam5_i2c_slave.h"
#include "control_flags.h"

#if DO_MOTOR_TEST
#include "motor_test.h"
#endif

#include "hardware_defines.h"



#ifdef __SAME54P20A__
struct io_descriptor *usart_io;
#endif

volatile control_flags_t control_flags = {
	.heartbeat = 0,
	.run_motor_controllers = 0,
};

static void status_led(uint8_t led, uint8_t level);

static uint8_t calibration_finished();

static volatile dc_motor_controller_input_flags_t flags;

int main(void)
{
	/* Set floating point coprosessor access mode. */
	SCB->CPACR |= ((3UL << 10*2) | /* set CP10 Full Access */
	(3UL << 11*2) ); /* set CP11 Full Access */
	
	// Setup output of the GCLK 0 to PB14
	gpio_set_pin_direction(PIN_PB14, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(PIN_PB14, PINMUX_PB14M_GCLK_IO0);
	
	// Setup output of the GCLK 1 (frequency of 1/100th of GCLK1) to PB15
	gpio_set_pin_direction(PIN_PB15, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(PIN_PB15, PINMUX_PB15M_GCLK_IO1);
	
	/// Initialize MCU, drivers and middleware
	atmel_start_init();
	
	// Activate system timer: 24bit wide with 100 MHz -> ~ 167 ms maximum time span measureable
	SysTick->LOAD = 0x00FFFFFF;
	SysTick->VAL = 0;
	SysTick->CTRL = (1 << 2) | (0<<1) | (1 << 0); // CLKSOURCE -> Processor Clock | TICKINT -> No interrupt at overflow | ENABLE
	
	#ifdef __SAME54P20A__
	usart_sync_get_io_descriptor(&USART_PC_DEBUG, &usart_io); // USART IO for demo
	usart_sync_enable(&USART_PC_DEBUG);

	io_write(usart_io, (const uint8_t *) "Starting\r\n", 10);
	#endif
	
	sam5_dc_motor_controller_init_shared_resources();
	#ifdef M1_PRESENT
	sam5_dc_motor_controller_init(&g_motor_controller_1);
	#endif
	#ifdef M2_PRESENT
	sam5_dc_motor_controller_init(&g_motor_controller_2);
	#endif
	#ifdef M3_PRESENT
	sam5_dc_motor_controller_init(&g_motor_controller_3);
	#endif

	#ifdef M1_PRESENT
	sam5_dc_motor_controller_enable(&g_motor_controller_1);
	#endif
	#ifdef M2_PRESENT
	sam5_dc_motor_controller_enable(&g_motor_controller_2);
	#endif
	#ifdef M3_PRESENT
	sam5_dc_motor_controller_enable(&g_motor_controller_3);
	#endif
	
	#if DO_MOTOR_TEST
	motor_test_init(&g_motor_controller_1);
	#else
	/// Initialize I2C client driver
	sam5_i2c_slave_init();
	#endif
	
	uint8_t initialized = 0;
	status_led(1, 1);
	while(1){
		if(control_flags.run_motor_controllers){
			if(!initialized){
				initialized = 1;
				/// Start motor controller loops
				sam5_dc_motor_controller_start_loop();
				status_led(1, 0);
			}
		}
		#if DO_MOTOR_TEST
		status_led(0, g_motor_controller_1.internals.contact_detection.contact_detected & 0b01); // Current (Capacitor+connector side)
		status_led(1, g_motor_controller_1.internals.contact_detection.contact_detected & 0b10); // Velocity (Microcontroller side)
		motor_test_run(&g_motor_controller_1);
		sam5_dc_motor_controller_low_prio_loop();
		#else
		if(calibration_finished()){
			status_led(1, true);
		}
		status_led(0, control_flags.heartbeat);
		sam5_dc_motor_controller_low_prio_loop();
		#endif
	}
}

static void status_led(uint8_t led, uint8_t level){
	switch(led){
		#ifdef STATUS_LED_PIN_0
		case 0: {
			gpio_set_pin_direction(STATUS_LED_PIN_0, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_0, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_0, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_1
		case 1: {
			gpio_set_pin_direction(STATUS_LED_PIN_1, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_1, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_1, level);
		} break;
		#endif
	}
}

static uint8_t calibration_finished(){
	return 1
	#ifdef M1_PRESENT
	&& g_motor_controller_1.state.flags_b.calib_stage == CALIB_STAGE_FINISHED
	#endif
	#ifdef M2_PRESENT
	&& g_motor_controller_2.state.flags_b.calib_stage == CALIB_STAGE_FINISHED
	#endif
	#ifdef M3_PRESENT
	&& g_motor_controller_3.state.flags_b.calib_stage == CALIB_STAGE_FINISHED
	#endif
	;
}

void debugHardfault(uint32_t *sp)
{
	uint32_t r0  = sp[0];
	uint32_t r1  = sp[1];
	uint32_t r2  = sp[2];
	uint32_t r3  = sp[3];
	uint32_t r12 = sp[4];
	uint32_t lr  = sp[5];
	uint32_t pc  = sp[6];
	uint32_t psr = sp[7];

	printf("HardFault Handler...\r\n");
	printf("System Control Block (SCB)\r\n:");
	printf("\tCFSR   0x%08lx\r\n", SCB->CFSR);
	printf("\tHFSR   0x%08lx\r\n", SCB->HFSR);
	printf("\tMMFAR  0x%08lx\r\n", SCB->MMFAR);
	printf("\tBFAR   0x%08lx\r\n", SCB->BFAR);
	printf("Stack before error: 0x%08lx\r\n", (uint32_t)sp);
	printf("\tR0     0x%08lx\r\n", r0);
	printf("\tR1     0x%08lx\r\n", r1);
	printf("\tR2     0x%08lx\r\n", r2);
	printf("\tR3     0x%08lx\r\n", r3);
	printf("\tR12    0x%08lx\r\n", r12);
	printf("\tLR     0x%08lx\r\n", lr);
	printf("\tPC     0x%08lx\r\n", pc);
	printf("\tPSR    0x%08lx\r\n", psr);
	
	#if defined(__SAMD51J19A__) || defined(__SAMD51J18A__)
	gpio_set_pin_level(PIN_PA12, 1);
	#endif
	
	while(1);
}

__attribute__( (naked) )
void HardFault_Handler(void)
{
	__asm volatile
	(
	"tst lr, #4                                    \n"
	"ite eq                                        \n"
	"mrseq r0, msp                                 \n"
	"mrsne r0, psp                                 \n"
	"ldr r1, debugHardfault_address                \n"
	"bx r1                                         \n"
	"debugHardfault_address: .word debugHardfault  \n"
	);
}

// _write implementation to make printf work with debug usart
_ssize_t _write(int fd, const void *buf, size_t count)
{
	if (fd == 1 || fd == 2)
	{
		#ifdef __SAME54P20A__
		io_write(usart_io, (uint8_t *)buf, count);
		#endif
		return (_ssize_t)count;
	}
	errno = EBADF;

	return -1;
}

// _read implementation to make printf work with debug usart
_ssize_t _read(int fd, void *buf, size_t count)
{
	if (fd == 0)
	{
		return 0;
	}
	errno = EBADF;
	return -1;
}
